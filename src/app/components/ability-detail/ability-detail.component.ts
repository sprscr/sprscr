import { Component, Input, Output, OnChanges, OnInit, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { Observable } from 'rxjs/observable';

@Component({
  selector: 'app-ability-detail',
  templateUrl: './ability-detail.component.html',
  styleUrls: ['./ability-detail.component.scss']
})
export class AbilityDetailComponent implements OnChanges, OnInit {
  @Input() ability:any = {};
  @Output() statusEffectIDPresent = new EventEmitter<string>();
  @Output() attackIDPresent = new EventEmitter<string>();
  abilityForm: FormGroup;
  statusEffects: Object[];

  constructor(private fb: FormBuilder, private gameDataService: GameDataService) {
    this.createForm();
  }

  ngOnInit() {
    this.gameDataService.getStatusEffects().subscribe(statusEffects => this.statusEffects = statusEffects);
  }

  ngOnChanges() {
    this.rebuildForm();
  }

  createForm() {
    this.abilityForm = this.fb.group({
      ID: '',
      DebugName: '',
      DisplayName: 0,
      Description: 0,
      UpgradedFromID: '',
      Icon: '',
      UsageType: '',
      UsageValue: 0,
      AbilityClass: '',
      AbilityLevel: 0,
      IsPassive: false,
      TriggerOnHit: false,
      IsModal: false,
      ModalGroupId: '',
      IsCombatOnly: false,
      IsNonCombatOnly: false,
      HideFromUI: false,
      HideFromCombatLog: false,
      UniqueSet: '',
      NoiseLevelID: '',
      DurationOverride: 0,
      ClearsOnMovement: false,
      CannotActivateWhileInStealth: false,
      CannotActivateWhileInvisible: false,
      PowerLevelScaling: this.fb.group({
        BaseLevel: 0,
        LevelIncrement: 0,
        MaxLevel: 0,
        DamageAdjustment: 0,
        DurationAdjustment: 0,
        BounceCountAdjustment: 0,
        ProjectileCountAdjustment: 0,
        AccuracyAdjustment: 0,
        PenetrationAdjustment: 0
      }),
      StatusEffectsIDs: this.fb.array([]),
      AttackID: ''
    });
  }

  rebuildForm() {
    if (!this.ability) {
      return;
    }
    let componentObjectIndex = -1; 
    if (this.ability.Components[0].$type === "Game.GameData.GenericAbilityComponent, Assembly-CSharp") { componentObjectIndex = 0 } 
    else if (this.ability.Components[1].$type === "Game.GameData.GenericAbilityComponent, Assembly-CSharp") { componentObjectIndex = 1 }
    this.abilityForm.reset({
      ID: this.ability.ID,
      DebugName: this.ability.DebugName,
      DisplayName: this.ability.Components[componentObjectIndex].DisplayName,
      Description: this.ability.Components[componentObjectIndex].Description,
      UpgradedFromID: this.ability.Components[componentObjectIndex].UpgradedFromID,
      Icon: this.ability.Components[componentObjectIndex].Icon,
      UsageValue: this.ability.Components[componentObjectIndex].UsageValue,
      AbilityClass: this.ability.Components[componentObjectIndex].AbilityClass,
      AbilityLevel: this.ability.Components[componentObjectIndex].AbilityLevel,
      IsPassive: this.ability.Components[componentObjectIndex].IsPassive,
      TriggerOnHit: this.ability.Components[componentObjectIndex].TriggerOnHit,
      IsModal: this.ability.Components[componentObjectIndex].IsModal,
      ModalGroupId: this.ability.Components[componentObjectIndex].ModalGroupId,
      IsCombatOnly: this.ability.Components[componentObjectIndex].IsCombatOnly,
      IsNonCombatOnly: this.ability.Components[componentObjectIndex].IsNonCombatOnly,
      HideFromUI: this.ability.Components[componentObjectIndex].HideFromUI,
      HideFromCombatLog: this.ability.Components[componentObjectIndex].HideFromCombatLog,
      UniqueSet: this.ability.Components[componentObjectIndex].UniqueSet,
      NoiseLevelID: this.ability.Components[componentObjectIndex].NoiseLevelID,
      DurationOverride: this.ability.Components[componentObjectIndex].DurationOverride,
      ClearsOnMovement: this.ability.Components[componentObjectIndex].ClearsOnMovement,
      CannotActivateWhileInStealth: this.ability.Components[componentObjectIndex].CannotActivateWhileInStealth,
      CannotActivateWhileInvisible: this.ability.Components[componentObjectIndex].CannotActivateWhileInvisible,
      PowerLevelScaling: {
        BaseLevel: this.ability.Components[componentObjectIndex].PowerLevelScaling.BaseLevel,
        LevelIncrement: this.ability.Components[componentObjectIndex].PowerLevelScaling.LevelIncrement,
        MaxLevel: this.ability.Components[componentObjectIndex].PowerLevelScaling.MaxLevel,
        DamageAdjustment: this.ability.Components[componentObjectIndex].PowerLevelScaling.DamageAdjustment,
        DurationAdjustment: this.ability.Components[componentObjectIndex].PowerLevelScaling.DurationAdjustment,
        BounceCountAdjustment: this.ability.Components[componentObjectIndex].PowerLevelScaling.BounceCountAdjustment,
        ProjectileCountAdjustment: this.ability.Components[componentObjectIndex].PowerLevelScaling.ProjectileCountAdjustment,
        AccuracyAdjustment: this.ability.Components[componentObjectIndex].PowerLevelScaling.AccuracyAdjustment,
        PenetrationAdjustment: this.ability.Components[componentObjectIndex].PowerLevelScaling.PenetrationAdjustment
      },
      AttackID: this.ability.Components[componentObjectIndex].AttackID
    });
    this.setStatusEffectsIDs(this.ability.Components[componentObjectIndex].StatusEffectsIDs);
  }

  get StatusEffectsIDs() {
    return this.abilityForm.get('StatusEffectsIDs') as FormArray;
  }

  setStatusEffectsIDs(StatusEffectsIDs) {
    const statusEffectsFormControls = StatusEffectsIDs.map(StatusEffectsID => {
      return this.fb.group({
        statusEffectDebugName: this.lookUpName('statuseffects', StatusEffectsID),
        statusEffectIDValue: StatusEffectsID
      });
    });
    const statusEffectsIDsFormArray = this.fb.array(statusEffectsFormControls);
    this.abilityForm.setControl('StatusEffectsIDs', statusEffectsIDsFormArray);
  }

  lookUpName(gamedatabundle: string, ID: string): string {
    return this.gameDataService.lookUpName(gamedatabundle, ID);
  }

  isIDFieldValid(value) {
    if ((value.length < 36) || (value === "00000000-0000-0000-0000-000000000000")) {
      return false;
    }
      return true;
  }

  goToListItemObject(event: Event) {
    event.stopPropagation();
    this.attackIDPresent.emit(this.abilityForm.value.AttackID);
  }

  loadStatusEffect(statusEffectID) {
    this.statusEffectIDPresent.emit(statusEffectID);
  }

  prepareSaveAbility() {
    const formModel = this.abilityForm.value;

    const statusEffectsList: string[] = formModel.StatusEffectsIDs.map(
      (statuseffect: any) => statuseffect.statusEffectIDValue);

    const saveAbility = {
      DebugName: this.ability.DebugName,
      ID: this.ability.ID,
      Components: [{
        DisplayName: parseInt(formModel.DisplayName, 10),
        Description: parseInt(formModel.Description, 10),
        UpgradedFromID: formModel.UpgradedFromID,
        Icon: formModel.Icon,
        UsageValue: parseInt(formModel.UsageValue, 10),
        AbilityClass: formModel.AbilityClass,
        AbilityLevel: parseInt(formModel.AbilityLevel, 10),
        IsPassive: formModel.IsPassive,
        TriggerOnHit: formModel.TriggerOnHit,
        IsModal: formModel.IsModal,
        ModalGroupId: formModel.ModalGroupId,
        IsCombatOnly: formModel.IsCombatOnly,
        IsNonCombatOnly: formModel.IsNonCombatOnly,
        HideFromUI: formModel.HideFromUI,
        HideFromCombatLog: formModel.HideFromCombatLog,
        UniqueSet: formModel.UniqueSet,
        NoiseLevelID: formModel.NoiseLevelID,
        DurationOverride: parseInt(formModel.DurationOverride, 10),
        ClearsOnMovement: formModel.ClearsOnMovement,
        CannotActivateWhileInStealth: formModel.CannotActivateWhileInStealth,
        CannotActivateWhileInvisible: formModel.CannotActivateWhileInvisible,
        PowerLevelScaling: {
          BaseLevel: parseInt(formModel.PowerLevelScaling.BaseLevel, 10),
          LevelIncrement: parseInt(formModel.PowerLevelScaling.LevelIncrement, 10),
          MaxLevel: parseInt(formModel.PowerLevelScaling.MaxLevel, 10),
          DamageAdjustment: parseInt(formModel.PowerLevelScaling.DamageAdjustment, 10),
          DurationAdjustment: parseInt(formModel.PowerLevelScaling.DurationAdjustment, 10),
          BounceCountAdjustment: parseInt(formModel.PowerLevelScaling.BounceCountAdjustment, 10),
          ProjectileCountAdjustment: parseInt(formModel.PowerLevelScaling.ProjectileCountAdjustment, 10),
          AccuracyAdjustment: parseInt(formModel.PowerLevelScaling.AccuracyAdjustment, 10),
          PenetrationAdjustment: parseInt(formModel.PowerLevelScaling.PenetrationAdjustment, 10)
        },
        AttackID: formModel.AttackID,
        StatusEffectsIDs: statusEffectsList
      }]
    };
    return saveAbility;
  }

  onStatusEffectIDChange(value, iterator) {
    this.abilityForm.value.StatusEffectsIDs[iterator].statusEffectIDValue = value;
    this.abilityForm.value.StatusEffectsIDs[iterator].statusEffectDebugName = value;
    this.onObjectListAltered();
  }

  onObjectListAltered() {
    this.abilityForm.markAsDirty();
  }

  onSubmit() {
    this.ability = this.prepareSaveAbility();
    this.gameDataService.recordChangedGameDataObject(this.ability, 'abilities', ['StatusEffectsIDs']);
    this.abilityForm.markAsPristine();
  }
}
