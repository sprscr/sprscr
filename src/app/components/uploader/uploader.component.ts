import { Component } from '@angular/core';
import { UploaderService } from '../../services/uploader/uploader.service';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
  providers: [ UploaderService ]
})
export class UploaderComponent  {
  message: string;
  placeholderMessage: string;
  fileContents: any;

  constructor(private uploaderService: UploaderService) { }

  fileInputChange(input: HTMLInputElement, uploadProgressBar: HTMLDivElement) {
    const file = input.files[0];
    if (file) {
      this.uploaderService.upload(file, uploadProgressBar);
    }
  }
}
