import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressionTableDetailComponent } from './progression-table-detail.component';

describe('ProgressionTableDetailComponent', () => {
  let component: ProgressionTableDetailComponent;
  let fixture: ComponentFixture<ProgressionTableDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressionTableDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressionTableDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
