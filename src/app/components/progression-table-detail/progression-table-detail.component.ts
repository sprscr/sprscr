import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { GameDataObjectTypes } from '../../data-model/game-data-object-types';

import { ProgressionTable, ProgressionTableGameDataComponent, AbilityList, Prerequisites } from '../../gamedata-templates';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';
@Component({
  selector: 'app-progression-table-detail',
  templateUrl: './progression-table-detail.component.html',
  styleUrls: ['./progression-table-detail.component.scss']
})
export class ProgressionTableDetailComponent implements OnChanges {
  @Input() progressionTable: ProgressionTable;
  ptForm: FormGroup;
  nameChangeLog: String[] = [];
  selectedAbility: any = {};
  selectedAttack: any = {};
  selectedStatusEffect: any = {};

  messageEmitterName: string = 'Progression Table';
  constructor(private messageService: MessageService, private fb: FormBuilder, private gameDataService: GameDataService, private gameDataObjectTypes: GameDataObjectTypes) { 
    this.createForm();
  }

  createForm() {
    this.ptForm = this.fb.group({
      ID: '',
      DebugName: '',
      AbilityUnlocks: this.fb.array([])
    });
  }

  ngOnChanges() {
    this.rebuildForm();
    this.resetSubAbilitySelections();
  }

  ngOnInit() {
    this.checkAbilities();
    this.checkStatusEffects();
  }

  rebuildForm() {
    if (!this.progressionTable) {
      return;
    }
    this.ptForm.reset({
      ID: this.progressionTable.ID,
      DebugName: this.progressionTable.DebugName
    });
    let componentObjectIndex = -1; 
    if (this.progressionTable.Components[0].$type === "Game.GameData.BaseProgressionTableComponent, Assembly-CSharp") { componentObjectIndex = 0 } 
    else if (this.progressionTable.Components[1].$type === "Game.GameData.BaseProgressionTableComponent, Assembly-CSharp") { componentObjectIndex = 1 }
    this.setAbilities(this.progressionTable.Components[componentObjectIndex].AbilityUnlocks);
  }

  setAbilities(abilities: AbilityList[]) {
    const abilityFGs = abilities.map(ability => {
      return this.fb.group({
        Note: ability.Note ? ability.Note : '<No Name Specified in Data>',
        Category: ability.Category,
        UnlockStyle: ability.UnlockStyle,
        ActivationObject: ability.ActivationObject,
        AddAbilityID: ability.AddAbilityID,
        RemoveAbilityID: ability.RemoveAbilityID,
        Prerequisites: this.fb.group({
          MinimumCharacterLevel: ability.Prerequisites.MinimumCharacterLevel,
          PowerLevelRequirement: this.fb.group({
            Class: ability.Prerequisites.PowerLevelRequirement.Class,
            MinimumPowerLevel: ability.Prerequisites.PowerLevelRequirement.MinimumPowerLevel
          }),
          RequiresAbilityID: ability.Prerequisites.RequiresAbilityID
        })
      })
    });
    const abilityFormArray = this.fb.array(abilityFGs);
    this.ptForm.setControl('AbilityUnlocks', abilityFormArray);
  }

  get AbilityUnlocks(): FormArray {
    return this.ptForm.get('AbilityUnlocks') as FormArray;
  }

  onSubmit() {
    this.progressionTable = this.prepareSaveProgressionTable();
    this.gameDataService.recordChangedGameDataObject(this.progressionTable, 'progressiontables', []);
    this.rebuildForm();
  }

  checkAbilities(): void {
    this.gameDataService.checkGameDataObjectArray('abilities', [this.gameDataObjectTypes.ABILITY]);
  }

  checkStatusEffects(): void {
    this.gameDataService.checkGameDataObjectArray('statuseffects', [this.gameDataObjectTypes.AFFLICTION, this.gameDataObjectTypes.STATUSEFFECT]);
  }

  checkAttacks(): void {
    this.gameDataService.checkGameDataObjectArray('attacks', 
    [
      this.gameDataObjectTypes.ATTACK_AOE,
      this.gameDataObjectTypes.ATTACK_AURA,
      this.gameDataObjectTypes.ATTACK_BEAM,
      this.gameDataObjectTypes.ATTACK_FIREARM,
      this.gameDataObjectTypes.ATTACK_GRAPPLE,
      this.gameDataObjectTypes.ATTACK_MELEE,
      this.gameDataObjectTypes.ATTACK_PULSEDAOE,
      this.gameDataObjectTypes.ATTACK_RANDOMAOE,
      this.gameDataObjectTypes.ATTACK_RANGED,
      this.gameDataObjectTypes.ATTACK_SUMMON,
      this.gameDataObjectTypes.ATTACK_SUMMONRANDOM,
      this.gameDataObjectTypes.ATTACK_TELEPORT,
      this.gameDataObjectTypes.ATTACK_BEAMTELEPORT,
      this.gameDataObjectTypes.ATTACK_TELEPORT,
      this.gameDataObjectTypes.ATTACK_HAZARD,
      this.gameDataObjectTypes.ATTACK_MASTERSCALL,
      this.gameDataObjectTypes.ATTACK_WALL
    ]);
  }

  selectAbility(abilityID: string) {
    this.resetSubAbilitySelections();
    let abilityQueryResult = this.gameDataService.getAbilityByID(abilityID);
    if (Object.keys(abilityQueryResult).length) {
      this.selectedAbility = abilityQueryResult;
    } else {
      this.messageService.add(this.messageEmitterName, `No ability with ID of "${abilityID}" exists. Obsidian, why you do dis?`);
    }
     
  }

  resetSubAbilitySelections() {
    this.selectedAttack = {};
    this.selectedStatusEffect = {};
    this.selectedAbility = {};
  }

  fillAbilityPTData(abilityID) {
    let abilityUnlocksList = this.ptForm.value.AbilityUnlocks;
    let result = this.gameDataService.filterGameDataObjectArray(abilityUnlocksList, abilityID);
    return result;
  }

  goToStatusEffect(statusEffectID) {
    let allStatusEffects = this.checkStatusEffects();
    let result = this.gameDataService.filterGameDataObjectArray(allStatusEffects, statusEffectID);
    this.selectedStatusEffect = result[0];
  }

  goToAttack(attackID) {
    let allAttacks = this.checkAttacks();
    let result = this.gameDataService.filterGameDataObjectArray(allAttacks, attackID);
    this.selectedAttack = result[0];
  }

  prepareSaveProgressionTable(): ProgressionTable {
    const formModel = this.ptForm.value;

    const abilitiesListDeepCopy: Object[] = formModel.AbilityUnlocks.map(
      (ability: Object) => Object.assign({}, ability)
    );

    const saveProgressionTable: ProgressionTable = {
      DebugName: this.progressionTable.DebugName,
      ID: this.progressionTable.ID,
      Components: [
        {
          AbilityUnlocks: abilitiesListDeepCopy
        }
      ]
    };
    return saveProgressionTable;
  }
}