import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgressionTablesComponent } from './progression-tables.component';

describe('ProgressionTablesComponent', () => {
  let component: ProgressionTablesComponent;
  let fixture: ComponentFixture<ProgressionTablesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgressionTablesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressionTablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
