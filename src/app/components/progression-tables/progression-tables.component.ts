import { Component, OnInit } from '@angular/core';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';

@Component({
  selector: 'app-progression-tables',
  templateUrl: './progression-tables.component.html',
  styleUrls: ['./progression-tables.component.scss']
})

export class ProgressionTablesComponent implements OnInit {
  progressionTables: Object[];
  selectedProgressionTable: Object;

  constructor(private gameDataService: GameDataService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getProgressionTables();
  }

  onSelect(progressionTable: Object): void {
    this.selectedProgressionTable = progressionTable;
  }

  getProgressionTables(): void {
	  this.gameDataService.getGameDataObjectArray('progressiontables').subscribe(progressionTables => this.progressionTables = progressionTables);
  }
}