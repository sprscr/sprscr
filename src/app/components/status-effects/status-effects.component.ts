import { Component, OnInit } from '@angular/core';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';

@Component({
  selector: 'app-status-effects',
  templateUrl: './status-effects.component.html',
  styleUrls: ['./status-effects.component.scss']
})
export class StatusEffectsComponent implements OnInit {
  statusEffects: Object[];
  selectedStatusEffect: any;

  constructor(private gameDataService: GameDataService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getStatusEffects();
  }

  onSelect(statusEffect: Object): void {
    this.selectedStatusEffect = statusEffect;
  }

  getStatusEffects(): void {
	  this.gameDataService.getGameDataObjectArray('statuseffects').subscribe(statusEffects => this.statusEffects = statusEffects);
  }

}
