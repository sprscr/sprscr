import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatusEffectsComponent } from './status-effects.component';

describe('StatusEffectsComponent', () => {
  let component: StatusEffectsComponent;
  let fixture: ComponentFixture<StatusEffectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatusEffectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusEffectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
