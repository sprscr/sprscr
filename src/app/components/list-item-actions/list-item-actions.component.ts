import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { GameDataService } from '../../services/gamedata/gamedata.service';

@Component({
  selector: 'app-list-item-actions',
  templateUrl: './list-item-actions.component.html',
  styleUrls: ['./list-item-actions.component.scss']
})
export class ListItemActionsComponent implements OnInit {
  @Input() parentArray: any[];
  @Input() objectIndex: number;
  @Output() objAlteredSuccessfully = new EventEmitter<string>();
  @Output() goToListItem = new EventEmitter<string>();
  isConfirmationVisible: boolean = false;
  constructor(private gameDataService: GameDataService) { }
  listItemObjectID = '';

  ngOnInit() {
    this.listItemObjectID = this.parentArray[this.objectIndex].statusEffectIDValue;
  }

  goToListItemObject(event: Event) {
    event.stopPropagation();
    this.goToListItem.emit(this.listItemObjectID);
  }

  addNewObjectToCollection(event: Event) {
    event.stopPropagation();
    this.parentArray.push({statusEffectDebugName: 'New Object', statusEffectIDValue: '00000000-0000-0000-0000-000000000000'});
    this.alterObject();
  }

  deleteConfirmation(event: Event) {
    event.stopPropagation();
    this.isConfirmationVisible = true;
  }

  deleteObject(event: Event) {
    event.stopPropagation();
    this.parentArray.splice(this.objectIndex, 1);
    this.alterObject();
  }

  alterObject() {
    this.objAlteredSuccessfully.emit('objAlteredSuccessfully');
  }

  cancelDelete(event: Event) {
    event.stopPropagation();
    this.isConfirmationVisible = false;
  }
}