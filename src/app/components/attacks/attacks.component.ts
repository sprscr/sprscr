import { Component, OnInit } from '@angular/core';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';
import { GameDataObjectTypes } from '../../data-model/game-data-object-types';

@Component({
  selector: 'app-attacks',
  templateUrl: './attacks.component.html',
  styleUrls: ['./attacks.component.scss']
})
export class AttacksComponent implements OnInit {
  attacks: Object[];
  selectedAttack: any;
  selectedStatusEffect: any = {};

  constructor(private gameDataService: GameDataService, private messageService: MessageService, private gameDataObjectTypes: GameDataObjectTypes) { }

  ngOnInit(): void {
    this.getAttacks();
  }

  onSelect(attack: Object): void {
    this.resetSubAttackSelections();
    this.selectedAttack = attack;
  }

  getAttacks(): void {
	  this.gameDataService.getGameDataObjectArray('attacks').subscribe(attacks => this.attacks = attacks);
  }

  resetSubAttackSelections(): void {
    this.selectedStatusEffect = {};
  }

  goToStatusEffect(statusEffectID: string): void {
    let allStatusEffects = this.gameDataService.checkGameDataObjectArray('statuseffects', [this.gameDataObjectTypes.STATUSEFFECT]);
    let result = this.gameDataService.filterGameDataObjectArray(allStatusEffects, statusEffectID);
    this.selectedStatusEffect = result[0];
  }

}
