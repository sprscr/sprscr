import { Component, OnInit } from '@angular/core';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';

@Component({
  selector: 'app-abilities',
  templateUrl: './abilities.component.html',
  styleUrls: ['./abilities.component.scss']
})

export class AbilitiesComponent implements OnInit {
  abilities: Object[];
  selectedAbility: Object;
  selectedAttack: any = {};
  selectedStatusEffect: any = {};

  constructor(private gameDataService: GameDataService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getAbilities();
  }

  onSelect(ability: Object): void {
    this.resetSubAbilitySelections();
    this.selectedAbility = ability;
  }

  getAbilities(): void {
	  this.gameDataService.getGameDataObjectArray('abilities').subscribe(abilities => this.abilities = abilities);
  }

  resetSubAbilitySelections(): void {
    this.selectedAttack = {};
    this.selectedStatusEffect = {};
  }

  goToStatusEffect(statusEffectID: string): void {
    let allStatusEffects = this.gameDataService.checkGameDataObjectArray('statuseffects', []);
    let result = this.gameDataService.filterGameDataObjectArray(allStatusEffects, statusEffectID);
    this.selectedStatusEffect = result[0];
  }

  goToAttack(attackID: string): void {
    let allAttacks = this.gameDataService.checkGameDataObjectArray('attacks', []);
    let result = this.gameDataService.filterGameDataObjectArray(allAttacks, attackID);
    this.selectedAttack = result[0];
  }
}
