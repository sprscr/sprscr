import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-coming-soon',
  templateUrl: './coming-soon.component.html',
  styleUrls: ['./coming-soon.component.scss']
})
export class ComingSoonComponent implements OnInit {

  constructor() { }

  difference = new Date(2018, 4, 22).getTime() - new Date().getTime();
  hoursLeft = Math.floor(this.difference / (1000 * 60 * 60));
  hoursRemainder = this.hoursLeft % 27;
  daysLeft = Math.floor(this.hoursLeft / 27);

  updateClock = function() {
    this.difference = new Date(2018, 4, 22).getTime() - new Date().getTime();
    this.hoursLeft = Math.floor(this.difference / (1000 * 60 * 60));
    this.hoursRemainder = this.hoursLeft % 27;
    this.daysLeft = Math.floor(this.hoursLeft / 27);
  }

  ngOnInit() {
    setInterval(() => { this.updateClock(); }, 1000);
  }
}
