import { Component, OnInit } from '@angular/core';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';

@Component({
  selector: 'app-export-data',
  templateUrl: './export-data.component.html',
  styleUrls: ['./export-data.component.scss']
})
export class ExportDataComponent implements OnInit {
  sortedModifiedObjects:any = {};
  allPTs = '';
  anyModificationBeenMade: Boolean = false;
  constructor(private gameDataService: GameDataService, private messageService: MessageService) { }
  messageEmitterName: string = 'Export Data Component';

  ngOnInit() {
    this.requestModifiedObjects();
  }

  requestModifiedObjects() {
    this.sortedModifiedObjects = this.gameDataService.getModifiedObjects();
    this.anyModificationBeenMade = this.checkForModdedData();
  }

  checkForModdedData() {
    for (let moddedObjList in this.sortedModifiedObjects) {
      if (this.sortedModifiedObjects[moddedObjList].length) {
        return true;
      }
    }
    return false;
  }
  
  prepareObjectsForExport(filename: String) {
    let formattedObjectString = '';
    switch (filename) {
      case 'characters':
        formattedObjectString = '{\r    "GameDataObjects": [\r';
        for (let i = 0; i < this.sortedModifiedObjects.modifiedCharacters.length; i++) {
          formattedObjectString += ("    " + JSON.stringify(this.sortedModifiedObjects.modifiedCharacters[i], null, "    "));
          if (i < (this.sortedModifiedObjects.modifiedCharacters.length - 1)) {
            formattedObjectString += ",";
          }
        }
        formattedObjectString += "]\r}";
      break;
      case 'progressiontables':
        formattedObjectString = '{\r    "GameDataObjects": [\r';
        for (let i = 0; i < this.sortedModifiedObjects.modifiedProgressionTables.length; i++) {
          formattedObjectString += ("    " + JSON.stringify(this.sortedModifiedObjects.modifiedProgressionTables[i], null, "    "));
          if (i < (this.sortedModifiedObjects.modifiedProgressionTables.length - 1)) {
            formattedObjectString += ",";
          }
        }
        formattedObjectString += "]\r}";
      break;
      case 'abilities':
        formattedObjectString = '{\r    "GameDataObjects": [\r';
        for (let i = 0; i < this.sortedModifiedObjects.modifiedAbilities.length; i++) {
          formattedObjectString += ("    " + JSON.stringify(this.sortedModifiedObjects.modifiedAbilities[i], null, "    "));
          if (i < (this.sortedModifiedObjects.modifiedAbilities.length - 1)) {
            formattedObjectString += ",";
          }
        }
        formattedObjectString += "]\r}";
      break;
      case 'attacks':
        formattedObjectString = '{\r    "GameDataObjects": [\r';
        for (let i = 0; i < this.sortedModifiedObjects.modifiedAttacks.length; i++) {
          formattedObjectString += ("    " + JSON.stringify(this.sortedModifiedObjects.modifiedAttacks[i], null, "    "));
          if (i < (this.sortedModifiedObjects.modifiedAttacks.length - 1)) {
            formattedObjectString += ",";
          }
        }
        formattedObjectString += "]\r}";
      break;
      case 'statuseffects':
        formattedObjectString = '{\r    "GameDataObjects": [\r';
        for (let i = 0; i < this.sortedModifiedObjects.modifiedStatusEffects.length; i++) {
          formattedObjectString += ("    " + JSON.stringify(this.sortedModifiedObjects.modifiedStatusEffects[i], null, "    "));
          if (i < (this.sortedModifiedObjects.modifiedStatusEffects.length - 1)) {
            formattedObjectString += ",";
          }
        }
        formattedObjectString += "]\r}";
      break;
    }
    return formattedObjectString;
  }

  exportFile(filename: String) {
    let formattedObjectString = this.prepareObjectsForExport(filename);
    let dataUri = 'data:application/json;charset=utf-8,'+ encodeURIComponent(formattedObjectString);
    let exportFileDefaultName = filename + '.gamedatabundle';   
    let linkElement = document.createElement('a');
    linkElement.setAttribute('href', dataUri);
    linkElement.setAttribute('download', exportFileDefaultName);
    linkElement.click();
  }

  copyToClipboard(objectID: string) {
    let objectToCopy = document.getElementById(objectID);
    if (objectToCopy) {
      var dummy = document.createElement("textarea");
      document.body.appendChild(dummy);
      dummy.innerHTML = objectToCopy.innerHTML;
      dummy.select();
      document.execCommand("copy");
      document.body.removeChild(dummy);
      this.messageService.add(this.messageEmitterName, 'Object copied to Clipboard');
    } else {
      this.messageService.add(this.messageEmitterName, 'Object not found');
    }
  }

}
