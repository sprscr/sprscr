import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatuseffectDetailComponent } from './statuseffect-detail.component';

describe('StatuseffectDetailComponent', () => {
  let component: StatuseffectDetailComponent;
  let fixture: ComponentFixture<StatuseffectDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatuseffectDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatuseffectDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
