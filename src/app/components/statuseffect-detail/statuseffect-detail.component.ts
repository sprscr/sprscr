import { Component, Input, OnChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { GameDataService } from '../../services/gamedata/gamedata.service';

@Component({
  selector: 'app-statuseffect-detail',
  templateUrl: './statuseffect-detail.component.html',
  styleUrls: ['./statuseffect-detail.component.scss']
})
export class StatuseffectDetailComponent implements OnChanges {
  @Input() statusEffect:any = undefined;
  statusEffectForm: FormGroup;
  statusEffectBaseIndex: number = -1;

  constructor(private fb: FormBuilder, private gameDataService: GameDataService) { 
    this.createForm();
  }

  ngOnChanges() {
    if (this.statusEffect) { // give proper error message here
      if (this.statusEffectBaseIndex === -1 && this.statusEffect.$type) {
        this.statusEffectBaseIndex = this.findStatusEffectBaseIndex();
      }
      this.rebuildForm();
    }
  }

  createForm() {
    this.statusEffectForm = this.fb.group({
      ID: '',
      DebugName: '',
      StatusEffectType: '',
      UseStatusEffectValueAs: '',
      BaseValue: 0,
      DurationType: '',
      Duration: 1,
      MaxStackQuantity: 0,
      ApplicationBehavior: '',
      ApplicationType: '',
      IntervalRateID: '',
      IsHostile: false,
      ClearOnCombatEnd: false,
      ClearOnRest: false,
      ClearOnFoodRest: false,
      ClearWhenAttacks: false,
      HideFromCombatTooltip: false,
      HideFromUI: false
    });
  }

  rebuildForm() {
    if (!this.statusEffect) {
      return;
    }
    let i = this.statusEffectBaseIndex;
    this.statusEffectForm.reset({
      ID: this.statusEffect.ID,
      DebugName: this.statusEffect.DebugName,
      StatusEffectType: this.statusEffect.Components[i].StatusEffectType,
      UseStatusEffectValueAs: this.statusEffect.Components[i].UseStatusEffectValueAs,
      BaseValue: this.statusEffect.Components[i].BaseValue,
      DurationType: this.statusEffect.Components[i].DurationType,
      Duration: this.statusEffect.Components[i].Duration,
      MaxStackQuantity: this.statusEffect.Components[i].MaxStackQuantity,
      ApplicationBehavior: this.statusEffect.Components[i].ApplicationBehavior,
      ApplicationType: this.statusEffect.Components[i].ApplicationType,
      IntervalRateID: this.statusEffect.Components[i].IntervalRateID,
      IsHostile: this.statusEffect.Components[i].IsHostile,
      ClearOnCombatEnd: this.statusEffect.Components[i].ClearOnCombatEnd,
      ClearOnRest: this.statusEffect.Components[i].ClearOnRest,
      ClearOnFoodRest: this.statusEffect.Components[i].ClearOnFoodRest,
      ClearWhenAttacks: this.statusEffect.Components[i].ClearWhenAttacks,
      HideFromCombatTooltip: this.statusEffect.Components[i].HideFromCombatTooltip,
      HideFromUI: this.statusEffect.Components[i].HideFromUI
    });
  }

  prepareSaveStatusEffect() {
    const formModel = this.statusEffectForm.value;

    const saveStatusEffect = {
      DebugName: this.statusEffect.DebugName,
      ID: this.statusEffect.ID,
      StatusEffectType: formModel.StatusEffectType,
      UseStatusEffectValueAs: formModel.UseStatusEffectValueAs,
      BaseValue: parseInt(formModel.BaseValue, 10),
      DurationType: formModel.DurationType,
      Duration: parseInt(formModel.Duration, 10),
      MaxStackQuantity: parseInt(formModel.MaxStackQuantity, 10),
      ApplicationBehavior: formModel.ApplicationBehavior,
      ApplicationType: formModel.ApplicationType,
      IntervalRateID: formModel.IntervalRateID,
      IsHostile: formModel.IsHostile,
      ClearOnCombatEnd: formModel.ClearOnCombatEnd,
      ClearOnRest: formModel.ClearOnRest,
      ClearOnFoodRest: formModel.ClearOnFoodRest,
      ClearWhenAttacks: formModel.ClearWhenAttacks,
      HideFromCombatTooltip: formModel.HideFromCombatTooltip,
      HideFromUI: formModel.HideFromUI
    };
    return saveStatusEffect;
  }

  findStatusEffectBaseIndex(): number {
    return this.statusEffect.Components[0].$type === "Game.GameData.StatusEffectComponent, Assembly-CSharp" ?  0 : 1;
  }
  

  onSubmit() {
    this.statusEffect = this.prepareSaveStatusEffect();
    this.gameDataService.recordChangedGameDataObject(this.statusEffect, 'statuseffects', []);
  }

}
