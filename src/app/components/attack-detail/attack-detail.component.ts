import { Component, Input, Output, OnInit, OnChanges, EventEmitter } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl, AbstractControl } from '@angular/forms';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';

@Component({
  selector: 'app-attack-detail',
  templateUrl: './attack-detail.component.html',
  styleUrls: ['./attack-detail.component.scss']
})
export class AttackDetailComponent implements OnInit, OnChanges  {
  @Input() attack: any = undefined;
  @Output() statusEffectIDPresent = new EventEmitter<string>();
  attackForm: FormGroup;
  statusEffects: Object[];
  attackType: string = '';
  messageEmitterName: string = 'Attacks Component';
  isAttackFormUpdated = false;

  constructor(private messageService: MessageService, private fb: FormBuilder, private gameDataService: GameDataService) {
    this.createForm();
  }

  /**
   * Add base form properties, existing for all types of attacks
   */
  createForm() {
    this.attackForm = this.fb.group({
      ID: '',
      DebugName: '',
      AttackDistance: 0,
      MinAttackDistance: 0,
      AttackVariationID: '',
      CastSpeedID: '',
      RecoveryTimeID: '',
      ImpactDelay: 0,
      ForcedTarget: '',
      AffectedTargetType: '',
      AffectedTargetDeathState: '',
      HostilityOverride: '',
      PushDistance: 0,
      FaceTarget: true,
      AccuracyBonus: 0,
      PenetrationRating: 0,
      DamageType: '',
      AlternateDamageType: '',
      Minimum: 0,
      Maximum: 0,
      DamageAttribute: '',
      StatusEffectsIDs: this.fb.array([]),
      RandomizeStatusEffect: false,
      CanGraze: false,
      CanCrit: true,
      DefendedBy: '',
      AfflictionsDefendedBy: '',
      AfflictionApplicationModifier: '',
      AttackOnImpactID: '',
      ExtraAttackID: '',
      LaunchBone: '',
      HitBone: '',
      NoiseLevelID: '',
      InterruptsOn: '',
      InterruptType: '',
      ApplyOnceOnly: '',
      HideFromCombatLog: false,
      DoesNotApplyDamage: false,
      TreatAsWeapon: false
    });
  }

  ngOnInit() {
    this.gameDataService.getStatusEffects().subscribe(statusEffects => this.statusEffects = statusEffects);
  }

  ngOnChanges() {
    if(this.attack){
      if (this.attack.$type && !this.isAttackFormUpdated) {
        this.attackType = this.attack.$type.substring(14, this.attack.$type.length - 17);
        this.updateAttackForm(this.attackType);
      }
    }
    
    this.rebuildForm();
  }

  /**
   * Add fields for whatever additional components the attack has
   */
  addAttackAOEComponent() {
    this.attackForm.setControl('BlastSize', new FormControl(''));
    this.attackForm.setControl('BlastRadiusOverride', new FormControl(0));
    this.attackForm.setControl('DamageAngle', new FormControl(0));
    this.attackForm.setControl('ExcludePrimaryTarget', new FormControl('false'));
    this.attackForm.setControl('IgnoreParentTarget', new FormControl('false'));
    this.attackForm.setControl('ExcludeSelf', new FormControl('false'));
    this.attackForm.setControl('BlastPhysicsForce', new FormControl(0));
    this.attackForm.setControl('PushFromCaster', new FormControl('false'));
  }

  addAttackRangedComponent() {
    this.attackForm.setControl('LaunchAttackOnCollision', new FormControl('false'));
    this.attackForm.setControl('ProjectileCount', new FormControl(0));
    this.attackForm.setControl('IgnoreMagicDefense', new FormControl('false'));
    this.attackForm.setControl('ProjectileConeAngle', new FormControl(0));
    this.attackForm.setControl('IsMultiHit', new FormControl('false'));
    this.attackForm.setControl('MultiHitTravelDist', new FormControl(0));
    this.attackForm.setControl('MultiHitMaxHits', new FormControl(0));
  }

  addAttackAuraComponent() {
    this.attackForm.setControl('Duration', new FormControl(0));
    this.attackForm.setControl('Scale', new FormControl('false'));
  }

  addAttackBeamComponent() {
    this.attackForm.setControl('BeamDuration', new FormControl(0));
    this.attackForm.setControl('BeamInterval', new FormControl(0));
    this.attackForm.setControl('BeamTargets', new FormControl('false'));
    this.attackForm.setControl('BeamExcludesMainTarget', new FormControl(0));
    this.attackForm.setControl('CasterOffset', new FormControl('false'));
    this.attackForm.setControl('TargetOffset', new FormControl(0));
  }

  addAttackFirearmComponent() {
    this.attackForm.setControl('ClipSize', new FormControl(0));
    this.attackForm.setControl('ReloadTime', new FormControl(0));
    this.attackForm.setControl('FirearmType', new FormControl(''));
  }

  addAttackMeleeComponent() {
    this.attackForm.setControl('EngagementRadius', new FormControl(0));
    this.attackForm.setControl('IsUnarmed', new FormControl('false'));
  }

  addAttackGrappleComponent() {
    this.attackForm.setControl('KnockdownTime', new FormControl(0));
    this.attackForm.setControl('DamageAttackID', new FormControl(''));
  }

  addAttackPulsedAOEComponent() {
    this.attackForm.setControl('Duration', new FormControl(0));
    this.attackForm.setControl('PulseIntervalRate', new FormControl(0));
    this.attackForm.setControl('OneValidTargetPerPulse', new FormControl('false'));
    this.attackForm.setControl('Scale', new FormControl('false'));
  }

  addAttackRandomAOEComponent() {
    this.attackForm.setControl('RandomAttackID', new FormControl(''));
    this.attackForm.setControl('RandomTargetStyle', new FormControl(''));
    this.attackForm.setControl('NumHits', new FormControl(0));
    this.attackForm.setControl('MaxHitsPerTarget', new FormControl(0));
  }

  addAttackSummonComponent() {
    this.attackForm.setControl('SummonType', new FormControl(''));
    this.attackForm.setControl('TeamType', new FormControl(''));
    this.attackForm.setControl('SummonCopyOfSelf', new FormControl('false'));
    this.attackForm.setControl('Duration', new FormControl(0));
    this.attackForm.setControl('HasLoot', new FormControl('false'));
  }

  addAttackSummonRandomComponent() {
    this.attackForm.setControl('MaxSummons', new FormControl(0));
  }

  addAttackWallComponent() {
    this.attackForm.setControl('WallLength', new FormControl(0));
    this.attackForm.setControl('FromCasterPosition', new FormControl('false'));
  }

  addBeamTeleportAttackComponent() {
    this.attackForm.setControl('HitWithPrimaryAttack', new FormControl('false'));
    this.attackForm.setControl('HitWithSecondaryAttack', new FormControl('false'));
    this.attackForm.setControl('SummonCopyOfSelf', new FormControl('false'));
    this.attackForm.setControl('HitTargetWithPrimaryAttack', new FormControl('false'));
    this.attackForm.setControl('HitTargetWithSecondaryAttack', new FormControl('false'));
  }

  addTeleportAttackComponent() {
    this.attackForm.setControl('SkipRaycast', new FormControl('false'));
    this.attackForm.setControl('Retreating', new FormControl('false'));
  }

  /**
   * Determine the type of attack object, and call methods to add the respective additional fields
   * @param attackType: String. The $type property of the attack, stripped of redundant text
   */
  updateAttackForm(attackType) {
    if (attackType === "AttackMeleeGameData" || attackType === "AttackAOEGameData" || attackType === "AttackAuraGameData" || attackType === "AttackBeamGameData" || 
    attackType === "AttackFirearmGameData" || attackType === "AttackGrappleGameData" || attackType === "AttackPulsedAOEGameData" || attackType === "AttackRandomAOEGameData" || 
    attackType === "AttackRangedGameData" || attackType === "AttackSummonGameData" || attackType === "AttackSummonRandomGameData" || attackType === "AttackWallGameData" || 
    attackType === "BeamTeleportAttackGameData" || attackType === "MastersCallAttackGameData" || attackType === "TeleportAttackGameData" || attackType === "HazardAttackGameData") {this.isAttackFormUpdated = true;}
    switch (attackType) {
      case "AttackMeleeGameData":
        this.addAttackMeleeComponent();
      break;
      case "AttackAOEGameData":
        this.addAttackAOEComponent();
        this.addAttackRangedComponent();
      break;
      case "AttackAuraGameData":
        this.addAttackAOEComponent();
        this.addAttackRangedComponent();
        this.addAttackAuraComponent();
      break;
      case "AttackBeamGameData":
        this.addAttackBeamComponent();
      break;
      case "AttackFirearmGameData":
        this.addAttackRangedComponent();
        this.addAttackFirearmComponent();
      break;
      case "AttackGrappleGameData":
        this.addAttackMeleeComponent();
        this.addAttackGrappleComponent();
      break;
      case "AttackPulsedAOEGameData":
        this.addAttackRangedComponent();
        this.addAttackAOEComponent();
        this.addAttackPulsedAOEComponent();
      break;
      case "AttackRandomAOEGameData":
        this.addAttackAOEComponent();
        this.addAttackPulsedAOEComponent();
        this.addAttackRandomAOEComponent();
        this.addAttackRangedComponent();
      break;
      case "AttackRangedGameData":
        this.addAttackRangedComponent();
      break;
      case "AttackSummonGameData":
        this.addAttackSummonComponent();
      break;
      case "AttackSummonRandomGameData":
        this.addAttackSummonComponent();
        this.addAttackSummonRandomComponent();
      break;
      case "AttackWallGameData":
        this.addAttackWallComponent();
      break;
      case "BeamTeleportAttackGameData":
        this.addBeamTeleportAttackComponent();
        this.addTeleportAttackComponent();
      break;
      case "MastersCallAttackGameData":
        this.addTeleportAttackComponent();
      break;
      case "TeleportAttackGameData":
        this.addTeleportAttackComponent();
      break;
      case "HazardAttackGameData": // we are not adding anything
      break;
      default:
        this.messageService.add(this.messageEmitterName, `Unknown Attack Type "${this.attackType}". Check if Attack Type "${this.attackType}" exists.`);
      break;
     }
  }

  /**
   * When the form is updated, call methods to update each of the additional fields, for every type of attack
   */
  rebuildAttackAOEComponent() {
    let componentObjectIndex = -1; 
    if (this.attack.Components[1].$type === "Game.GameData.AttackAOEComponent, Assembly-CSharp") { componentObjectIndex = 1 } 
    else if (this.attack.Components[2].$type === "Game.GameData.AttackAOEComponent, Assembly-CSharp"){ componentObjectIndex = 2 }

    this.attackForm.patchValue({BlastSize: this.attack.Components[componentObjectIndex].BlastSize});
    this.attackForm.patchValue({BlastRadiusOverride: this.attack.Components[componentObjectIndex].BlastRadiusOverride});
    this.attackForm.patchValue({DamageAngle: this.attack.Components[componentObjectIndex].DamageAngle});
    this.attackForm.patchValue({ExcludePrimaryTarget: this.attack.Components[componentObjectIndex].ExcludePrimaryTarget});
    this.attackForm.patchValue({IgnoreParentTarget: this.attack.Components[componentObjectIndex].IgnoreParentTarget});
    this.attackForm.patchValue({ExcludeSelf: this.attack.Components[componentObjectIndex].ExcludeSelf});
    this.attackForm.patchValue({BlastPhysicsForce: this.attack.Components[componentObjectIndex].BlastPhysicsForce});
    this.attackForm.patchValue({PushFromCaster: this.attack.Components[componentObjectIndex].PushFromCaster});
  }

  rebuildAttackRangedComponent() {
    let componentObjectIndex = -1; 
    if (this.attack.Components[1].$type === "Game.GameData.AttackRangedComponent, Assembly-CSharp") { componentObjectIndex = 1 } 
    else if (this.attack.Components[2].$type === "Game.GameData.AttackRangedComponent, Assembly-CSharp"){ componentObjectIndex = 2 }
    else if (this.attack.Components[3].$type === "Game.GameData.AttackRangedComponent, Assembly-CSharp"){ componentObjectIndex = 3 }
    else if (this.attack.Components[4].$type === "Game.GameData.AttackRangedComponent, Assembly-CSharp"){ componentObjectIndex = 4 }

    this.attackForm.patchValue({LaunchAttackOnCollision: this.attack.Components[componentObjectIndex].LaunchAttackOnCollision});
    this.attackForm.patchValue({ProjectileCount: this.attack.Components[componentObjectIndex].ProjectileCount});
    this.attackForm.patchValue({IgnoreMagicDefense: this.attack.Components[componentObjectIndex].IgnoreMagicDefense});
    this.attackForm.patchValue({ProjectileConeAngle: this.attack.Components[componentObjectIndex].ProjectileConeAngle});
    this.attackForm.patchValue({IsMultiHit: this.attack.Components[componentObjectIndex].IsMultiHit});
    this.attackForm.patchValue({MultiHitTravelDist: this.attack.Components[componentObjectIndex].MultiHitTravelDist});
    this.attackForm.patchValue({MultiHitMaxHits: this.attack.Components[componentObjectIndex].MultiHitMaxHits});
  }

  rebuildAttackAuraComponent() {
    let componentObjectIndex = -1; 
    if (this.attack.Components[1].$type === "Game.GameData.AttackAuraComponent, Assembly-CSharp") { componentObjectIndex = 1 } 
    else if (this.attack.Components[2].$type === "Game.GameData.AttackAuraComponent, Assembly-CSharp"){ componentObjectIndex = 2 }
    else if (this.attack.Components[3].$type === "Game.GameData.AttackAuraComponent, Assembly-CSharp"){ componentObjectIndex = 3 }
  
    this.attackForm.patchValue({Duration: this.attack.Components[componentObjectIndex].Duration});
    this.attackForm.patchValue({Scale: this.attack.Components[componentObjectIndex].Scale});
  }

  rebuildAttackBeamComponent() {
    let componentObjectIndex = -1; 
    if (this.attack.Components[1].$type === "Game.GameData.AttackBeamComponent, Assembly-CSharp") { componentObjectIndex = 1 }

    this.attackForm.patchValue({BeamDuration: this.attack.Components[componentObjectIndex].BeamDuration});
    this.attackForm.patchValue({BeamInterval: this.attack.Components[componentObjectIndex].BeamInterval});
    this.attackForm.patchValue({BeamTargets: this.attack.Components[componentObjectIndex].BeamTargets});
    this.attackForm.patchValue({BeamExcludesMainTarget: this.attack.Components[componentObjectIndex].BeamExcludesMainTarget});
    this.attackForm.patchValue({CasterOffset: this.attack.Components[componentObjectIndex].CasterOffset});
    this.attackForm.patchValue({TargetOffset: this.attack.Components[componentObjectIndex].TargetOffset});
  }

  rebuildAttackFirearmComponent() {
    this.attackForm.patchValue({ClipSize: this.attack.Components[1].ClipSize});
    this.attackForm.patchValue({ReloadTime: this.attack.Components[1].ReloadTime});
    this.attackForm.patchValue({FirearmType: this.attack.Components[1].FirearmType});
  }

  rebuildAttackMeleeComponent() {
    this.attackForm.patchValue({EngagementRadius: this.attack.Components[1].EngagementRadius});
    this.attackForm.patchValue({IsUnarmed: this.attack.Components[1].IsUnarmed});
  }

  rebuildAttackGrappleComponent() {
    this.attackForm.patchValue({KnockdownTime: this.attack.Components[2].KnockdownTime});
    this.attackForm.patchValue({DamageAttackID: this.attack.Components[2].DamageAttackID});
  }

  rebuildAttackPulsedAOEComponent() {
    let componentObjectIndex = -1; 
    if (this.attack.Components[1].$type === "Game.GameData.AttackPulsedAOEComponent, Assembly-CSharp") { componentObjectIndex = 1 } 
    else if (this.attack.Components[2].$type === "Game.GameData.AttackPulsedAOEComponent, Assembly-CSharp"){ componentObjectIndex = 2 }
    else if (this.attack.Components[3].$type === "Game.GameData.AttackPulsedAOEComponent, Assembly-CSharp"){ componentObjectIndex = 3 }

    this.attackForm.patchValue({Duration: this.attack.Components[componentObjectIndex].Duration});
    this.attackForm.patchValue({PulseIntervalRate: this.attack.Components[componentObjectIndex].PulseIntervalRate});
    this.attackForm.patchValue({OneValidTargetPerPulse: this.attack.Components[componentObjectIndex].OneValidTargetPerPulse});
    this.attackForm.patchValue({Scale: this.attack.Components[componentObjectIndex].Scale});
  }

  rebuildAttackRandomAOEComponent() {
    this.attackForm.patchValue({RandomAttackID: this.attack.Components[3].RandomAttackID});
    this.attackForm.patchValue({RandomTargetStyle: this.attack.Components[3].RandomTargetStyle});
    this.attackForm.patchValue({NumHits: this.attack.Components[3].NumHits});
    this.attackForm.patchValue({MaxHitsPerTarget: this.attack.Components[3].MaxHitsPerTarget});
  }

  rebuildAttackSummonComponent() {
    this.attackForm.patchValue({SummonType: this.attack.Components[1].SummonType});
    this.attackForm.patchValue({TeamType: this.attack.Components[1].TeamType});
    this.attackForm.patchValue({SummonCopyOfSelf: this.attack.Components[1].SummonCopyOfSelf});
    this.attackForm.patchValue({Duration: this.attack.Components[1].Duration});
    this.attackForm.patchValue({HasLoot: this.attack.Components[1].HasLoot});
  }

  rebuildAttackSummonRandomComponent() {
    this.attackForm.patchValue({MaxSummons: this.attack.Components[2].MaxSummons});
  }

  rebuildAttackWallComponent() {
    this.attackForm.patchValue({WallLength: this.attack.Components[1].WallLength});
    this.attackForm.patchValue({FromCasterPosition: this.attack.Components[1].FromCasterPosition});
  }

  rebuildBeamTeleportAttackComponent() {
    this.attackForm.patchValue({HitWithPrimaryAttack: this.attack.Components[1].HitWithPrimaryAttack});
    this.attackForm.patchValue({HitWithSecondaryAttack: this.attack.Components[1].HitWithSecondaryAttack});
    this.attackForm.patchValue({SummonCopyOfSelf: this.attack.Components[1].SummonCopyOfSelf});
    this.attackForm.patchValue({HitTargetWithPrimaryAttack: this.attack.Components[1].HitTargetWithPrimaryAttack});
    this.attackForm.patchValue({HitTargetWithSecondaryAttack: this.attack.Components[1].HitTargetWithSecondaryAttack});
  }

  rebuildTeleportAttackComponent() {
    let componentObjectIndex = -1; 
    if (this.attack.Components[1].$type === "Game.GameData.TeleportAttackComponent, Assembly-CSharp") { componentObjectIndex = 1 } 
    else if (this.attack.Components[2].$type === "Game.GameData.TeleportAttackComponent, Assembly-CSharp"){ componentObjectIndex = 2 }

    this.attackForm.patchValue({SkipRaycast: this.attack.Components[componentObjectIndex].SkipRaycast});
    this.attackForm.patchValue({Retreating: this.attack.Components[componentObjectIndex].Retreating});
  }
  
  /**
   * Update the base form fields, determine the attack type, and then call methods to update the respective 
   * additional fields
   */
  rebuildForm() {
    if (!this.attack) {
      return;
    }
    this.attackForm.reset({
      ID: this.attack.ID,
      DebugName: this.attack.DebugName,
      AttackDistance: this.attack.Components[0].AttackDistance,
      MinAttackDistance: this.attack.Components[0].MinAttackDistance,
      AttackVariationID: this.attack.Components[0].AttackVariationID,
      CastSpeedID: this.attack.Components[0].CastSpeedID,
      RecoveryTimeID: this.attack.Components[0].RecoveryTimeID,
      ImpactDelay: this.attack.Components[0].ImpactDelay,
      ForcedTarget: this.attack.Components[0].ForcedTarget,
      AffectedTargetType: this.attack.Components[0].AffectedTargetType,
      AffectedTargetDeathState: this.attack.Components[0].AffectedTargetDeathState,
      HostilityOverride: this.attack.Components[0].HostilityOverride,
      PushDistance: this.attack.Components[0].PushDistance,
      FaceTarget: this.attack.Components[0].FaceTarget,
      AccuracyBonus: this.attack.Components[0].AccuracyBonus,
      PenetrationRating: this.attack.Components[0].PenetrationRating,
      DamageType: this.attack.Components[0].DamageData.DamageType,
      AlternateDamageType: this.attack.Components[0].DamageData.AlternateDamageType,
      Minimum: this.attack.Components[0].DamageData.Minimum,
      Maximum: this.attack.Components[0].DamageData.Maximum,
      DamageAttribute: this.attack.Components[0].DamageAttribute,
      RandomizeStatusEffect: this.attack.Components[0].RandomizeStatusEffect,
      CanGraze: this.attack.Components[0].CanGraze,
      CanCrit: this.attack.Components[0].CanCrit,
      DefendedBy: this.attack.Components[0].DefendedBy,
      AfflictionsDefendedBy: this.attack.Components[0].AfflictionsDefendedBy,
      AfflictionApplicationModifier: this.attack.Components[0].AfflictionApplicationModifier,
      AttackOnImpactID: this.attack.Components[0].AttackOnImpactID,
      ExtraAttackID: this.attack.Components[0].ExtraAttackID,
      LaunchBone: this.attack.Components[0].LaunchBone,
      HitBone: this.attack.Components[0].HitBone,
      NoiseLevelID: this.attack.Components[0].NoiseLevelID,
      InterruptsOn: this.attack.Components[0].InterruptsOn,
      InterruptType: this.attack.Components[0].InterruptType,
      ApplyOnceOnly: this.attack.Components[0].ApplyOnceOnly,
      HideFromCombatLog: this.attack.Components[0].HideFromCombatLog,
      DoesNotApplyDamage: this.attack.Components[0].DoesNotApplyDamage,
      TreatAsWeapon: this.attack.Components[0].TreatAsWeapon
    });
    this.setStatusEffectsIDs(this.attack.Components[0].StatusEffectsIDs);
    if (this.attackType) {
      switch(this.attackType) {
        case 'AttackMeleeGameData':
          this.rebuildAttackMeleeComponent();
        break;
        case "AttackAOEGameData":
          this.rebuildAttackAOEComponent();
          this.rebuildAttackRangedComponent();
        break;
        case "AttackAuraGameData":
          this.rebuildAttackAOEComponent();
          this.rebuildAttackRangedComponent();
          this.rebuildAttackAuraComponent();
        break;
        case "AttackBeamGameData":
          this.rebuildAttackBeamComponent();
        break;
        case "AttackFirearmGameData":
          this.rebuildAttackRangedComponent();
          this.rebuildAttackFirearmComponent();
        break;
        case "AttackGrappleGameData":
          this.rebuildAttackMeleeComponent();
          this.rebuildAttackGrappleComponent();
        break;
        case "AttackPulsedAOEGameData":
          this.rebuildAttackRangedComponent();
          this.rebuildAttackAOEComponent();
          this.rebuildAttackPulsedAOEComponent();
        break;
        case "AttackRandomAOEGameData":
          this.rebuildAttackAOEComponent();
          this.rebuildAttackPulsedAOEComponent();
          this.rebuildAttackRandomAOEComponent();
          this.rebuildAttackRangedComponent();
        break;
        case "AttackRangedGameData":
          this.rebuildAttackRangedComponent();
        break;
        case "AttackSummonGameData":
          this.rebuildAttackSummonComponent();
        break;
        case "AttackSummonRandomGameData":
          this.rebuildAttackSummonComponent();
          this.rebuildAttackSummonRandomComponent();
        break;
        case "AttackWallGameData":
          this.rebuildAttackWallComponent();
        break;
        case "BeamTeleportAttackGameData":
          this.rebuildBeamTeleportAttackComponent();
          this.rebuildTeleportAttackComponent();
        break;
        case "MastersCallAttackGameData":
          this.rebuildTeleportAttackComponent();
        break;
        case "TeleportAttackGameData":
          this.rebuildTeleportAttackComponent();
        break;
        default: 
          this.messageService.add(this.messageEmitterName, `Unknown Attack Type "${this.attackType}". Check if Attack Type "${this.attackType}" exists.`);
        break;
      }
    }
  }

  setStatusEffectsIDs(StatusEffectsIDs) {
    const statusEffectsFormControls = StatusEffectsIDs.map(StatusEffectsID => {
      return this.fb.group({
        statusEffectDebugName: this.lookUpName('statuseffects', StatusEffectsID),
        statusEffectIDValue: StatusEffectsID
      });
    });
    const statusEffectsIDsFormArray = this.fb.array(statusEffectsFormControls);
    this.attackForm.setControl('StatusEffectsIDs', statusEffectsIDsFormArray);
  }

  lookUpName(gamedatabundle: string, ID: string): string {
    return this.gameDataService.lookUpName(gamedatabundle, ID);
  }

  isIDFieldValid(value) {
    if ((value.length < 36) || (value === "00000000-0000-0000-0000-000000000000")) {
      return false;
    }
      return true;
  }

  loadStatusEffect(statusEffectID) {
    this.statusEffectIDPresent.emit(statusEffectID);
  }

  /**
   * Prepare objects for respective
   */
  prepareAttackComponents(attackType): any[] {
    let AttackAOEComponent = {
      BlastSize: this.attackForm.value.BlastSize,
      BlastRadiusOverride: parseInt(this.attackForm.value.BlastRadiusOverride, 10),
      DamageAngle: parseInt(this.attackForm.value.DamageAngle, 10),
      ExcludePrimaryTarget: this.attackForm.value.ExcludePrimaryTarget,
      IgnoreParentTarget: this.attackForm.value.IgnoreParentTarget,
      ExcludeSelf: this.attackForm.value.ExcludeSelf,
      BlastPhysicsForce: parseInt(this.attackForm.value.BlastPhysicsForce, 10),
      PushFromCaster: this.attackForm.value.PushFromCaster
    };
    let AttackRangedComponent = {
      LaunchAttackOnCollision: this.attackForm.value.LaunchAttackOnCollision,
      ProjectileCount: parseInt(this.attackForm.value.ProjectileCount, 10),
      IgnoreMagicDefense: this.attackForm.value.IgnoreMagicDefense,
      ProjectileConeAngle: parseInt(this.attackForm.value.ProjectileConeAngle, 10),
      IsMultiHit: this.attackForm.value.IsMultiHit,
      MultiHitTravelDist: parseInt(this.attackForm.value.MultiHitTravelDist, 10),
      MultiHitMaxHits: parseInt(this.attackForm.value.MultiHitMaxHits, 10)
    };
    let AttackAuraComponent = {
      Duration: parseInt(this.attackForm.value.Duration, 10),
      Scale: this.attackForm.value.Scale
    };
    let AttackBeamComponent = {
      BeamDuration: parseInt(this.attackForm.value.BeamDuration, 10),
      BeamInterval: parseInt(this.attackForm.value.BeamInterval, 10),
      BeamTargets: this.attackForm.value.BeamTargets,
      BeamExcludesMainTarget: this.attackForm.value.BeamExcludesMainTarget,
      CasterOffset: parseInt(this.attackForm.value.CasterOffset, 10),
      TargetOffset: parseInt(this.attackForm.value.TargetOffset, 10)
    };
    let AttackFirearmComponent = {
      ClipSize: parseInt(this.attackForm.value.ClipSize, 10),
      ReloadTime: parseInt(this.attackForm.value.ReloadTime, 10),
      FirearmType: this.attackForm.value.FirearmType
    };
    let AttackMeleeComponent = {
      EngagementRadius: parseInt(this.attackForm.value.EngagementRadius, 10),
      IsUnarmed: this.attackForm.value.IsUnarmed
    };
    let AttackGrappleComponent = {
      KnockdownTime: parseInt(this.attackForm.value.KnockdownTime, 10),
      DamageAttackID: this.attackForm.value.DamageAttackID
    };
    let AttackPulsedAOEComponent = {
      Duration: parseInt(this.attackForm.value.Duration, 10),
      PulseIntervalRate: parseInt(this.attackForm.value.PulseIntervalRate, 10),
      OneValidTargetPerPulse: this.attackForm.value.OneValidTargetPerPulse,
      Scale: this.attackForm.value.Scale
    };
    let AttackRandomAOEComponent = {
      RandomAttackID: this.attackForm.value.RandomAttackID,
      RandomTargetStyle: this.attackForm.value.RandomTargetStyle,
      NumHits: parseInt(this.attackForm.value.NumHits, 10),
      MaxHitsPerTarget: parseInt(this.attackForm.value.MaxHitsPerTarget, 10)
    };
    let AttackSummonComponent = {
      SummonType: this.attackForm.value.SummonType,
      TeamType: this.attackForm.value.TeamType,
      SummonCopyOfSelf: this.attackForm.value.SummonCopyOfSelf,
      Duration: parseInt(this.attackForm.value.Duration, 10),
      HasLoot: this.attackForm.value.HasLoot
    };
    let AttackSummonRandomComponent = {
      MaxSummons: parseInt(this.attackForm.value.MaxSummons, 10)
    };
    let AttackWallComponent = {
      WallLength: parseInt(this.attackForm.value.WallLength, 10),
      FromCasterPosition: this.attackForm.value.FromCasterPosition
    };
    let BeamTeleportAttackComponent = {
      HitWithPrimaryAttack: this.attackForm.value.HitWithPrimaryAttack,
      HitWithSecondaryAttack: this.attackForm.value.HitWithSecondaryAttack,
      HitTargetWithPrimaryAttack: this.attackForm.value.HitTargetWithPrimaryAttack,
      HitTargetWithSecondaryAttack: this.attackForm.value.HitTargetWithSecondaryAttack
    };
    let TeleportAttackComponent = {
      SkipRaycast: this.attackForm.value.SkipRaycast,
      Retreating: this.attackForm.value.Retreating
    };


    let attackComponents = [];
    switch (attackType) {
      case 'AttackAOEGameData':
        attackComponents.push(AttackAOEComponent);
        attackComponents.push(AttackRangedComponent);
      break;
      case 'AttackAuraGameData':
        attackComponents.push(AttackAOEComponent);
        attackComponents.push(AttackRangedComponent);
        attackComponents.push(AttackAuraComponent);
      break;
      case 'AttackBeamGameData':
        attackComponents.push(AttackBeamComponent);
      break;
      case 'AttackFirearmGameData':
        attackComponents.push(AttackRangedComponent);
        attackComponents.push(AttackFirearmComponent);
      break;
      case 'AttackGrappleGameData':
        attackComponents.push(AttackMeleeComponent);
        attackComponents.push(AttackGrappleComponent);
      break;
      case 'AttackMeleeGameData':
        attackComponents.push(AttackMeleeComponent);
      break;
      case 'AttackPulsedAOEGameData':
        attackComponents.push(AttackRangedComponent);
        attackComponents.push(AttackAOEComponent);
        attackComponents.push(AttackPulsedAOEComponent);
      break;
      case 'AttackRandomAOEGameData':
        attackComponents.push(AttackAOEComponent);
        attackComponents.push(AttackPulsedAOEComponent);
        attackComponents.push(AttackRandomAOEComponent);
        attackComponents.push(AttackRangedComponent);
      break;
      case 'AttackRangedGameData':
        attackComponents.push(AttackRangedComponent);
      break;
      case 'AttackSummonGameData':
        attackComponents.push(AttackSummonComponent);
      break;
      case 'AttackSummonRandomGameData':
        attackComponents.push(AttackSummonComponent);
        attackComponents.push(AttackSummonRandomComponent);
      break;
      case 'AttackWallGameData':
        attackComponents.push(AttackWallComponent);
      break;
      case 'BeamTeleportAttackGameData':
        attackComponents.push(BeamTeleportAttackComponent);
      break;
      case 'MastersCallAttackGameData':
        attackComponents.push(TeleportAttackComponent);
      break;
      case 'TeleportAttackGameData':
        attackComponents.push(TeleportAttackComponent);
      break;
    }
    return attackComponents;
  }

  /**
   * Construct an object to be recorded into the in-memory database and prepared for exporting
   * from the Export screen
   */
  prepareSaveAttack() {
    const formModel = this.attackForm.value;

    const statusEffectsList: string[] = formModel.StatusEffectsIDs.map(
      (statuseffect: any) => statuseffect.statusEffectIDValue);

    let saveAttack = {
      DebugName: this.attack.DebugName,
      ID: this.attack.ID,
      Components: [
        {
          AttackDistance: parseInt(this.attackForm.value.AttackDistance, 10),
          MinAttackDistance: parseInt(this.attackForm.value.MinAttackDistance, 10),
          AttackVariationID: this.attackForm.value.AttackVariationID,
          CastSpeedID: this.attackForm.value.CastSpeedID,
          RecoveryTimeID: this.attackForm.value.RecoveryTimeID,
          ImpactDelay: parseInt(this.attackForm.value.ImpactDelay, 10),
          ForcedTarget: this.attackForm.value.ForcedTarget,
          AffectedTargetType: this.attackForm.value.AffectedTargetType,
          AffectedTargetDeathState: this.attackForm.value.AffectedTargetDeathState,
          HostilityOverride: this.attackForm.value.HostilityOverride,
          PushDistance: parseInt(this.attackForm.value.PushDistance, 10),
          FaceTarget: this.attackForm.value.FaceTarget,
          AccuracyBonus: parseInt(this.attackForm.value.AccuracyBonus, 10),
          PenetrationRating: parseInt(this.attackForm.value.PenetrationRating, 10),
          DamageData: {
            DamageType: this.attackForm.value.DamageType,
            AlternateDamageType: this.attackForm.value.AlternateDamageType,
            Minimum: parseInt(this.attackForm.value.Minimum, 10),
            Maximum: parseInt(this.attackForm.value.Maximum, 10),
            DamageAttribute: this.attackForm.value.DamageAttribute
          },
          StatusEffectsIDs: statusEffectsList,
          RandomizeStatusEffect: this.attackForm.value.RandomizeStatusEffect,
          CanGraze: this.attackForm.value.CanGraze,
          CanCrit: this.attackForm.value.CanCrit,
          DefendedBy: this.attackForm.value.DefendedBy,
          AfflictionsDefendedBy: this.attackForm.value.AfflictionsDefendedBy,
          AfflictionApplicationModifier: this.attackForm.value.AfflictionApplicationModifier,
          AttackOnImpactID: this.attackForm.value.AttackOnImpactID,
          ExtraAttackID: this.attackForm.value.ExtraAttackID,
          LaunchBone: this.attackForm.value.LaunchBone,
          HitBone: this.attackForm.value.HitBone,
          NoiseLevelID: this.attackForm.value.NoiseLevelID,
          InterruptsOn: this.attackForm.value.InterruptsOn,
          InterruptType: this.attackForm.value.InterruptType,
          ApplyOnceOnly: this.attackForm.value.ApplyOnceOnly,
          HideFromCombatLog: this.attackForm.value.HideFromCombatLog,
          DoesNotApplyDamage: this.attackForm.value.DoesNotApplyDamage,
          TreatAsWeapon: this.attackForm.value.TreatAsWeapon
        }
      ]
    };

    let additionalComponents = this.prepareAttackComponents(this.attackType);
    for (let i = 0; i < additionalComponents.length; i++) {
      saveAttack.Components.push(additionalComponents[i]);
    }

    return saveAttack;
  }

  onStatusEffectIDChange(value, iterator) {
    this.attackForm.value.StatusEffectsIDs[iterator].statusEffectIDValue = value;
    this.attackForm.value.StatusEffectsIDs[iterator].statusEffectDebugName = value;
    this.onObjectListAltered();
  }

  onObjectListAltered() {
    this.attackForm.markAsDirty();
  }

  onSubmit() {
    this.attack = this.prepareSaveAttack();
    this.gameDataService.recordChangedGameDataObject(this.attack, 'attacks', []);
  }

}
