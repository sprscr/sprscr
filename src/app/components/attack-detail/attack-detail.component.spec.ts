import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttackDetailComponent } from './attack-detail.component';

describe('AttackDetailComponent', () => {
  let component: AttackDetailComponent;
  let fixture: ComponentFixture<AttackDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttackDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttackDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
