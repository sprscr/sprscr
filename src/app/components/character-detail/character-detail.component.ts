import { Component, OnInit, OnChanges, Input, Output, EventEmitter} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { Observable } from 'rxjs/observable';
import { GameDataObjectTypes } from '../../data-model/game-data-object-types';

@Component({
  selector: 'app-character-detail',
  templateUrl: './character-detail.component.html',
  styleUrls: ['./character-detail.component.scss']
})
export class CharacterDetailComponent implements OnInit, OnChanges {
  @Input() character: any = {};
  baseStats: any = {};
  selectedProgressionTable: any = {};
  isBaseStatsFound = false;
  characterForm: FormGroup;
  baseStatsForm: FormGroup;

  constructor(private fb: FormBuilder, private gameDataService: GameDataService, private gameDataObjectTypes: GameDataObjectTypes) {
    this.createForm();
    this.createBaseStatsForm();
  }

  ngOnInit(): void {
  }

  ngOnChanges(): void {
    this.rebuildForm();
    if (this.character) {
      this.findBaseStatsObject(); 
      this.rebuildBaseStatsForm();
    }
    this.resetSubSelections();
  }

  findBaseStatsObject(): void {
    //let charactersDB = this.gameDataService.checkCharacterTypesArray();
    let charactersDB = this.gameDataService.checkGameDataObjectArray('characters', [this.gameDataObjectTypes.CREATURETYPES]);
    let creatureType = this.gameDataService.filterGameDataObjectArray(charactersDB, this.character.Components[0].CreatureTypeGDID);
    //charactersDB = this.gameDataService.checkBaseStatsArray();
    charactersDB = this.gameDataService.checkGameDataObjectArray('characters', [this.gameDataObjectTypes.BASESTATS])
    let baseStats = this.gameDataService.filterGameDataObjectArray(charactersDB, creatureType[0].Components[0].BaseStatsID);
    this.baseStats = baseStats[0];
    this.isBaseStatsFound = true;
  }

  createForm() {
    this.characterForm = this.fb.group({
      ID: '',
      DebugName: '',
      DisplayName: 0,
      Gender: '',
      CharacterProgressionTableID: '',
      BaseClassLevel: 0,
      BaseMight: 0,
      BaseConstitution: 0,
      BaseDexterity: 0,
      BasePerception: 0,
      BaseIntellect: 0,
      BaseResolve: 0,
      ImmuneToEngagement: false,
      ImmuneToAttacks: false,
      StartingMaxEngageableEnemyCount: 0,
      StartingAttackSpeedMultiplier: 0,
      StartingRateOfFireMultiplier: 0,
      StartingReloadTimeMultiplier: 0,
      MinLevelAdjustment: 0,
      MaxLevelAdjustment: 0 
    });
  }

  createBaseStatsForm() {
    this.baseStatsForm = this.fb.group({
      ID: '',
      DebugName: '',
      BaseDeflection: 0,
      BaseFortitude: 0,
      BaseReflexes: 0,
      BaseWill: 0,
      MeleeAccuracyBonus: 0,
      RangedAccuracyBonus: 0,
      MaxHealth: 0,
      HealthPerLevel: 0
    });
  }

  rebuildForm() {
    if (!this.character) {
      return;
    }
    this.characterForm.reset({
      ID: this.character.ID,
      DebugName: this.character.DebugName,
      DisplayName: this.character.Components[0].DisplayName,
      Gender: this.character.Components[0].Gender,
      CharacterProgressionTableID: this.character.Components[0].CharacterProgressionTableID,
      BaseClassLevel: this.character.Components[0].BaseClassLevel,
      BaseMight: this.character.Components[0].BaseMight,
      BaseConstitution: this.character.Components[0].BaseConstitution,
      BaseDexterity: this.character.Components[0].BaseDexterity,
      BasePerception: this.character.Components[0].BasePerception,
      BaseIntellect: this.character.Components[0].BaseIntellect,
      BaseResolve: this.character.Components[0].BaseResolve,
      ImmuneToEngagement: this.character.Components[0].ImmuneToEngagement,
      ImmuneToAttacks: this.character.Components[0].ImmuneToAttacks,
      StartingMaxEngageableEnemyCount: this.character.Components[0].StartingMaxEngageableEnemyCount,
      StartingAttackSpeedMultiplier: this.character.Components[0].StartingAttackSpeedMultiplier,
      StartingRateOfFireMultiplier: this.character.Components[0].StartingRateOfFireMultiplier,
      StartingReloadTimeMultiplier: this.character.Components[0].StartingReloadTimeMultiplier,
      MinLevelAdjustment: this.character.Components[0].MinLevelAdjustment,
      MaxLevelAdjustment: this.character.Components[0].MaxLevelAdjustment 
    });
  }

  rebuildBaseStatsForm() {
    if (!this.baseStats) {
      return;
    }
    this.baseStatsForm.reset({
      ID: this.baseStats.ID,
      DebugName: this.baseStats.DebugName,
      BaseDeflection: this.baseStats.Components[0].BaseDeflection,
      BaseFortitude: this.baseStats.Components[0].BaseFortitude,
      BaseReflexes: this.baseStats.Components[0].BaseReflexes,
      BaseWill: this.baseStats.Components[0].BaseWill,
      MeleeAccuracyBonus: this.baseStats.Components[0].MeleeAccuracyBonus,
      RangedAccuracyBonus: this.baseStats.Components[0].RangedAccuracyBonus,
      MaxHealth: this.baseStats.Components[0].MaxHealth,
      HealthPerLevel: this.baseStats.Components[0].HealthPerLevel
    });
  }

  isIDFieldValid(value) {
    if ((value.length < 36) || (value === "00000000-0000-0000-0000-000000000000")) {
      return false;
    }
      return true;
  }

  resetSubSelections() {
    this.selectedProgressionTable = {};
  }

  goToProgressionTable(progressionTableID) {
    event.stopPropagation();
    //let allProgressionTables = this.gameDataService.checkProgressionTablesArray();
    let allProgressionTables = this.gameDataService.checkGameDataObjectArray('progressiontables', [this.gameDataObjectTypes.PROGRESSIONTABLE]);
    let result = this.gameDataService.filterGameDataObjectArray(allProgressionTables, progressionTableID);
    this.selectedProgressionTable = result[0];
  }

  prepareSaveCharacter(): Object {
    const formModel = this.characterForm.value;

    const saveCharacter = {
      DebugName: this.character.DebugName,
      ID: this.character.ID,
      Components: [{
        DisplayName: parseInt(formModel.DisplayName, 10),
        Gender: formModel.Gender,
        CharacterProgressionTableID: formModel.CharacterProgressionTableID,
        BaseClassLevel: parseInt(formModel.BaseClassLevel, 10),
        BaseMight: parseInt(formModel.BaseMight, 10),
        BaseConstitution: parseInt(formModel.BaseConstitution, 10),
        BaseDexterity: parseInt(formModel.BaseDexterity, 10),
        BasePerception: parseInt(formModel.BasePerception, 10),
        BaseIntellect: parseInt(formModel.BaseIntellect, 10),
        BaseResolve: parseInt(formModel.BaseResolve, 10),
        ImmuneToEngagement: formModel.ImmuneToEngagement,
        ImmuneToAttacks: formModel.ImmuneToAttacks,
        StartingMaxEngageableEnemyCount: parseInt(formModel.StartingMaxEngageableEnemyCount, 10),
        StartingAttackSpeedMultiplier: parseInt(formModel.StartingAttackSpeedMultiplier, 10),
        StartingRateOfFireMultiplier: parseInt(formModel.StartingRateOfFireMultiplier, 10),
        StartingReloadTimeMultiplier: parseInt(formModel.StartingReloadTimeMultiplier, 10),
        MinLevelAdjustment: parseInt(formModel.MinLevelAdjustment, 10),
        MaxLevelAdjustment: parseInt(formModel.MaxLevelAdjustment, 10)
      }]
    };
    return saveCharacter;
  }

  prepareSaveBaseStats(): Object {
    const formModel = this.baseStatsForm.value;

    const saveBaseStats = {
      DebugName: this.baseStats.DebugName,
      ID: this.baseStats.ID,
      Components: [{
        BaseDeflection: parseInt(formModel.BaseDeflection, 10),
        BaseFortitude: parseInt(formModel.BaseFortitude, 10),
        BaseReflexes: parseInt(formModel.BaseReflexes, 10),
        BaseWill: parseInt(formModel.BaseWill, 10),
        MeleeAccuracyBonus: parseInt(formModel.MeleeAccuracyBonus, 10),
        RangedAccuracyBonus: parseInt(formModel.RangedAccuracyBonus, 10),
        MaxHealth: parseInt(formModel.MaxHealth, 10),
        HealthPerLevel: parseInt(formModel.HealthPerLevel, 10)
      }]
    };
    return saveBaseStats;
  }

  onSubmit() {
    this.character = this.prepareSaveCharacter();
    this.gameDataService.recordChangedGameDataObject(this.character, 'characters', []);
  }

  onSubmitBaseStats() {
    this.baseStats = this.prepareSaveBaseStats();
    this.gameDataService.recordChangedGameDataObject(this.baseStats, 'characters', []);
  }
}
