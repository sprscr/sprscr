import { Component, OnInit } from '@angular/core';
import { GameDataService } from '../../services/gamedata/gamedata.service';
import { MessageService } from '../../services/messages/message.service';

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})

export class CharactersComponent implements OnInit {
  characters: Object[];
  selectedCharacter: Object;
  messageEmitterName: string = 'Characters Component';

  constructor(private gameDataService: GameDataService, private messageService: MessageService) { }

  ngOnInit(): void {
	  this.getCharacters();
  }

  onSelect(character): void {
    this.messageService.add(this.messageEmitterName, `GameDataService: fetched character: ${character.debugName}`);
    this.selectedCharacter = character;
  }

  getCharacters(): void {
    this.gameDataService.getGameDataObjectArray('characters').subscribe(characters => this.characters = characters);
  }
}