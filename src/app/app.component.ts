import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Spiritual Successor - Modding Tool For Pillars Of Eternity: Deadfire';

  constructor(public router: Router) {
  }
}