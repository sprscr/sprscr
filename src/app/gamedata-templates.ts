export class Character {
  $type: string;
  debugName: string;
  id: string;
  baseMight: number;
  baseConstitution: number;
  baseDexterity: number;
  basePerception: number;
  baseIntellect: number;
  baseResolve: number;
  isNamedCharacter: boolean;
}

export class ProgressionTable {
  DebugName: string = '';
  ID: string = '';
  Components: any[];
}

export class ProgressionTableGameDataComponent {
  AbilityUnlocks: AbilityList[];
}

export class AbilityList {
  Note: string = '';
  Category: string = '';
  UnlockStyle: string = '';
  ActivationObject: string = '';
  AddAbilityID: string = '';
  RemoveAbilityID: '';
  Prerequisites: {
    MinimumCharacterLevel: 0,
    PowerLevelRequirement: {
      Class: '',
      MinimumPowerLevel: 0
    },
    RequiresAbilityID: ''
  }
}

export class Prerequisites {
  MinimumCharacterLevel = 0;
  PowerLevelRequirement: {
    Class: '',
    MinimumPowerLevel: 0
  };
  RequiresAbilityID: '';
}
