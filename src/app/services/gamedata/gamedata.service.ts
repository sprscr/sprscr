import { Injectable } from '@angular/core';
import { DbLoadService } from '../db-load/db-load.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { MessageService } from '../messages/message.service';
import { GameDataObjectTypes } from '../../data-model/game-data-object-types';
import * as _ from 'lodash';

@Injectable()
export class GameDataService {
  sortedModifiedObjects = {
    modifiedCharacters: [],
    modifiedProgressionTables: [],
    modifiedCreatureTypes: [],
    modifiedBaseStats: [],
    modifiedAbilities: [],
    modifiedAttacks: [],
    modifiedStatusEffects: [],
    modifiedItems: [],
    modifiedGeneral: []
  };
  messageEmitterName: string = 'GameData Service';

  constructor(private messageService: MessageService, private dbLoadService: DbLoadService, private gameDataObjectTypes: GameDataObjectTypes) {}

  determineGameDataQueryOptions(dbName: string): string[] {
    switch (dbName) {
      case 'characters':
        return [this.gameDataObjectTypes.CHARACTERSTATS];
      case 'progressiontables':
        return [this.gameDataObjectTypes.PROGRESSIONTABLE];
      case 'abilities':
        return [this.gameDataObjectTypes.ABILITY];
      case 'attacks':
        return [
          this.gameDataObjectTypes.ATTACK_AOE, this.gameDataObjectTypes.ATTACK_AURA, this.gameDataObjectTypes.ATTACK_BEAM,
          this.gameDataObjectTypes.ATTACK_FIREARM, this.gameDataObjectTypes.ATTACK_GRAPPLE, this.gameDataObjectTypes.ATTACK_MELEE,
          this.gameDataObjectTypes.ATTACK_PULSEDAOE, this.gameDataObjectTypes.ATTACK_RANDOMAOE, this.gameDataObjectTypes.ATTACK_RANGED,
          this.gameDataObjectTypes.ATTACK_SUMMON, this.gameDataObjectTypes.ATTACK_SUMMONRANDOM, this.gameDataObjectTypes.ATTACK_TELEPORT,
          this.gameDataObjectTypes.ATTACK_BEAMTELEPORT, this.gameDataObjectTypes.ATTACK_TELEPORT, this.gameDataObjectTypes.ATTACK_HAZARD,
          this.gameDataObjectTypes.ATTACK_MASTERSCALL, this.gameDataObjectTypes.ATTACK_WALL
        ];
      case 'statuseffects':
        return [this.gameDataObjectTypes.STATUSEFFECT];
    }
  }

  getGameDataObjectArray(dbName): Observable<Object[]> {
    let checkGameDataObjectArrayOpts: string[] = this.determineGameDataQueryOptions(dbName);
    let filteredObjects = this.checkGameDataObjectArray(dbName, checkGameDataObjectArrayOpts);
    return of(filteredObjects);
  }

  /**
   * @description Checks if a given database has been initialized with data, initializes it, and returns the database, possibly filtered
   * @param dbName the name of the databse, by convention this is the gamedatabundle file name
   * @param filters an array of strings, values of the $type property of root-level objects in the database, by which to filter out only those objects which match the type
   * Used for overwriting whole arrays of objects such as the arrays of StatusEffects objects.
   */
  checkGameDataObjectArray(dbName: string, filters: string[]): Object[] {
    let db: Object[] = this.dbLoadService.getDb(dbName);
    if (db.length === 0) {
      this.messageService.add(this.messageEmitterName, `No ${this.dbLoadService.formatDbName(dbName)} data loaded. Loading defaults...`);
      this.dbLoadService.createDb(dbName, '');
      db = this.dbLoadService.getDb(dbName);
    }
    if (filters.length > 0) {
      let filteredDb: Object[] = [];
      for (let i = 0; i < filters.length; i++) {
        filteredDb = filteredDb.concat(this.filterGameDataObjectArray(db, filters[i]));
      }
      db = filteredDb;
    }
    return db;
  }

  filterGameDataObjectArray(objectArray: any, filterCriterion: string) {
    let filteredObjectArray = objectArray.filter(function (el){
      if (!el.$type && !el.AddAbilityID && !el.ID) {
        return false;
      }
      return el.$type === filterCriterion || el.AddAbilityID === filterCriterion || el.ID === filterCriterion;
    });
    return filteredObjectArray;
  }

  lookUpName(gamedatabundle: string, ID: string): string {
    let dbName: Object[] = [];
    let lookUpResult: any[] = [];
    switch (gamedatabundle) {
      case 'statuseffects':
        dbName = this.dbLoadService.STATUSEFFECTS.GameDataObjects;
        lookUpResult = this.filterGameDataObjectArray(dbName, ID);
        return lookUpResult[0].DebugName;
      default:
        this.messageService.add(this.messageEmitterName, 'Incorrect game databundle target specified');
    }
  }

  setTargetGameDataBundle(gamedatabundle): Object[] {
    let result;
    switch (gamedatabundle) {
      case 'characters':
        result = this.dbLoadService.CHARACTERS;
      break;
      case 'progressiontables':
        result = this.dbLoadService.PROGRESSION_TABLES;
      break;
      case 'abilities':
        result = this.dbLoadService.ABILITIES;
      break;
      case 'attacks':
        result = this.dbLoadService.ATTACKS;
      break;
      case 'statuseffects':
        result = this.dbLoadService.STATUSEFFECTS;
      break;
      default:
        this.messageService.add(this.messageEmitterName, 'Incorrect game databundle target specified');
    }
    return result;
  }

  /**
   * Selects the needed gamedatabundle and sends the modified gamedata object to the dbLoad service to record it
   * @param incomingGameDataObject The modified object, the result of changes in the webform
   * @param gamedatabundle The gamedatabundle this changed object belongs to
   * @param overwrite Subobjects of this object, housed in one of its object arrays. These need to be completely replaced, not added to
   */
  recordChangedGameDataObject(incomingGameDataObject: any, gamedatabundle: string, overwrite: string[]) {
    let targetGameDataBundle = this.setTargetGameDataBundle(gamedatabundle);
    this.dbLoadService.recordChangedGameDataObject(incomingGameDataObject, targetGameDataBundle, overwrite);
  }

  // returns all objects the user has edited, as properties in an object
  getModifiedObjects(): Object {
    let allModifiedObjects = this.dbLoadService.getAllModifiedObjects();
    
    // Characters types
    this.sortedModifiedObjects.modifiedCharacters = [];
    this.sortedModifiedObjects.modifiedCharacters = this.sortedModifiedObjects.modifiedCharacters.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.CHARACTERSTATS));
    this.sortedModifiedObjects.modifiedCharacters = this.sortedModifiedObjects.modifiedCharacters.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.CREATURETYPES));
    this.sortedModifiedObjects.modifiedCharacters = this.sortedModifiedObjects.modifiedCharacters.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.BASESTATS));

    // Progression Tables types
    this.sortedModifiedObjects.modifiedProgressionTables = this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.PROGRESSIONTABLE);
    
    // Abilities types
    this.sortedModifiedObjects.modifiedAbilities = this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ABILITY);
    

    // Attacks tyoes. There are many types of attacks, so we construct an array from all possible types of attacks' arrays
    this.sortedModifiedObjects.modifiedAttacks = [];

    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_AOE));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_AURA));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_BEAM));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_FIREARM));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_GRAPPLE));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_MELEE));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_PULSEDAOE));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_RANGED));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_SUMMON));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_SUMMONRANDOM));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_WALL));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_BEAMTELEPORT));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_HAZARD));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_MASTERSCALL));
    this.sortedModifiedObjects.modifiedAttacks = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.ATTACK_TELEPORT));
    
    // Status Effect types
    this.sortedModifiedObjects.modifiedStatusEffects = []
    this.sortedModifiedObjects.modifiedStatusEffects = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.STATUSEFFECT));
    this.sortedModifiedObjects.modifiedStatusEffects = this.sortedModifiedObjects.modifiedAttacks.concat(this.filterGameDataObjectArray(allModifiedObjects, this.gameDataObjectTypes.AFFLICTION));
    return this.sortedModifiedObjects;
  }

  // return a single ability by looking for its ID
  getAbilityByID(abilityID): object {
    return this.dbLoadService.getAbilityByID(abilityID);
  }
}