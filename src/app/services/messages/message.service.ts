import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
  messages: string[] = [];

  add(emitter: string, message: string) {
    this.messages.push(emitter + ': ' + message);
  }

  clear() {
    this.messages = [];
  }

}
