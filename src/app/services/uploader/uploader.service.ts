import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DbLoadService } from '../db-load/db-load.service';
import { MessageService } from '../messages/message.service';

@Injectable()
export class UploaderService {
  constructor(private messageService: MessageService, private dbLoadService: DbLoadService) {}
  messageEmitterName: string = 'Uploader Service';
  uploadedFile: File;
  upload = (file: File, uploadProgressBar: any) => {
    var serviceMethodsScope = this;
    if (!file) { return; }
    let reader, fileContents, fileName;
    fileName = file.name;
    var uploadProgressBar = uploadProgressBar;
    if (uploadProgressBar) {
      uploadProgressBar.style.width = '0%';
      uploadProgressBar.textContent = '0%';
    }
    
    reader = new FileReader();
    reader.updateProgress = function(e) {
      // e is a ProgressEvent.
      if (e.lengthComputable && uploadProgressBar) {
        let percentLoaded = Math.round((e.loaded / e.total) * 100);
        // Progress bar animation.
        if (percentLoaded < 100) {
          uploadProgressBar.style.width = percentLoaded + '%';
          uploadProgressBar.textContent = percentLoaded + '%';
        }
      }
    };

    reader.errorHandler = function(e) {
      const userMessage = 'Upload failed.';
      let message: String;
      
      switch(e.target.error.code) {
        case e.target.error.NOT_FOUND_ERR:
        message = 'File Not Found!';
          break;
        case e.target.error.NOT_READABLE_ERR:
        message = 'File is not readable';
          break;
        case e.target.error.ABORT_ERR:
          break;
        default:
        message = 'An error occurred reading this file.';      
        serviceMethodsScope.messageService.add(serviceMethodsScope.messageEmitterName, `${message}`);
      };
    };

    reader.onprogress = reader.updateProgress;

    reader.onloadstart = function (e) {
      if (uploadProgressBar) {
        uploadProgressBar.parentElement.className = 'loading';
      }
    }
  
    reader.onload = function (e) {
      fileContents = reader.result;
      if (uploadProgressBar) {
        uploadProgressBar.parentElement.className = '';
      }
      serviceMethodsScope.loadGameDataIntoDB(fileContents, fileName);
    }
    reader.onerror = reader.errorHandler;
    reader.readAsText(file);
  }

  loadGameDataIntoDB(gameData: string, fileName: string) {
    switch (fileName) {
      case 'characters.gamedatabundle' || 'characters.json' :
      this.dbLoadService.createDb('characters', gameData);
      break;
      case 'progressiontables.gamedatabundle' || 'progressiontables.json' :
      this.dbLoadService.createDb('progressiontables', gameData);
      //this.dbLoadService.createProgressionTableDb(gameData);
      break;
      case 'abilities.gamedatabundle' || 'abilities.json' :
      this.dbLoadService.createDb('abilities', gameData);
      //this.dbLoadService.createAbilityDb(gameData);
      break;
      case 'attacks.gamedatabundle' || 'attacks.json' :
      this.dbLoadService.createDb('attacks', gameData);
      //this.dbLoadService.createAttacksDb(gameData);
      break;
      case 'statuseffects.gamedatabundle' || 'statuseffects.json' :
      this.dbLoadService.createDb('statuseffects', gameData);
      //this.dbLoadService.createStatusEffectsDb(gameData);
      break;
    default :
      this.messageService.add(this.messageEmitterName, 'Filename of uploaded file is not recognized');
    }
  }
}