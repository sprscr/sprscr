import { Injectable } from '@angular/core';
import { MessageService } from '../messages/message.service';
import { Observable } from 'rxjs/Observable';
import { GameDataObjectTypes } from '../../data-model/game-data-object-types';
import { DEF_CHARACTERS } from '../../data-model/default-characters';
import { DEF_PROGRESSION_TABLES } from '../../data-model/default-progression-tables';
import { DEF_ABILITIES } from '../../data-model/default-abilities';
import { DEF_ATTACKS } from '../../data-model/default-attacks';
import { DEF_STATUSEFFECTS } from '../../data-model/default-statuseffects';
import * as _ from 'lodash';


@Injectable()
export class DbLoadService {
    CHARACTERS: Object = {};
    PROGRESSION_TABLES: Object = {};
    ABILITIES: Object = {};
    ATTACKS: Object = {};
    STATUSEFFECTS: any = {};
    CHANGED_GAMEDATA: Object[] = [];
    messageEmitterName: string = 'Database Service';
    objectAlreadyPresent: any = {};

    determineDefaultDb(dbName: string): string {
        switch (dbName) {
            case 'characters': return DEF_CHARACTERS;
            case 'progressiontables': return DEF_PROGRESSION_TABLES;
            case 'abilities': return DEF_ABILITIES;
            case 'attacks': return DEF_ATTACKS;
            case 'statuseffects': return DEF_STATUSEFFECTS;
        }
    }

    formatDbName(dbName: string): string {
        switch (dbName) {
            case 'characters': return 'Characters';
            case 'progressiontables': return 'Progression Tables';
            case 'abilities': return 'Abilities';
            case 'attacks': return 'Attacks';
            case 'statuseffects': return 'Status Effects';
        }
    }

    createDb(dbName: string, gameData: string): void {
        let dataToLoad: string = gameData ? gameData : this.determineDefaultDb(dbName);
        let db = JSON.parse(dataToLoad);
        switch (dbName) {
            case 'characters': 
                this.CHARACTERS = db;
            break;
            case 'progressiontables': 
                this.PROGRESSION_TABLES = db;
            break;
            case 'abilities': 
                this.ABILITIES = db;
            break;
            case 'attacks': 
                this.ATTACKS = db;
            break;
            case 'statuseffects': 
                this.STATUSEFFECTS = db; 
            break;
        }
        this.messageService.add(this.messageEmitterName, `${this.formatDbName(dbName)} data has been loaded`);
    }

    getDb(dbName): Object[] {
        let db: any;
        switch (dbName) {
            case 'characters': 
                db = this.CHARACTERS;
            break;
            case 'progressiontables': 
                db = this.PROGRESSION_TABLES;
            break;
            case 'abilities': 
                db = this.ABILITIES;
            break;
            case 'attacks': 
                db = this.ATTACKS;
            break;
            case 'statuseffects': 
                db = this.STATUSEFFECTS; 
            break;
        }
        if(!db.GameDataObjects) {
            this.messageService.add(this.messageEmitterName, `Wrong or missing data for ${this.formatDbName(dbName)}`);
            return [];
        } else {
            return db.GameDataObjects;
        }
    }

    checkObjectPresent(updatedGameDataObject, listOfChangedObjects) {
        let that = this;
        let objectAlreadyPresent = this.filterGameDataObjectArray(listOfChangedObjects, updatedGameDataObject.ID);
        if (objectAlreadyPresent.length > 0) {
            that.objectAlreadyPresent = objectAlreadyPresent[0];
            return true;
        } else {
            return false;
        }
    }

    recordChangedGameDataObject(incomingGameDataObject: any, gameDataBundle: any, overwrite: string[]) {
        let that = this;
        const initialGameDataObject = gameDataBundle.GameDataObjects.find(obj => obj.ID === incomingGameDataObject.ID);
        // Overwrite any properties which have been marked for overwriting
        if (overwrite.length) {
            for (var i = 0; i < overwrite.length; i++) {
                switch (true) {
                    case (
                        ((initialGameDataObject.$type === "Game.GameData.GenericAbilityGameData, Assembly-CSharp") || 
                        (initialGameDataObject.$type === "Game.GameData.AttackAOEGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackAuraGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackBeamGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackFirearmGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackGrappleGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackMeleeGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackPulsedAOEGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackRandomAOEGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackRangedGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackSummonGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackSummonRandomGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.AttackWallGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.BeamTeleportAttackGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.HazardAttackGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.MastersCallAttackGameData, Assembly-CSharp") ||
                        (initialGameDataObject.$type === "Game.GameData.TeleportAttackGameData, Assembly-CSharp"))
                        && overwrite[i] === 'StatusEffectsIDs'):
                        delete initialGameDataObject.Components[0][overwrite[i]];
                    break;
                    default: that.messageService.add(this.messageEmitterName, 'Wrong or missing data for Status Effects');
                }
            }
        }
        let updatedGameDataObject = _.merge(initialGameDataObject, incomingGameDataObject);
        if (this.checkObjectPresent(updatedGameDataObject, this.CHANGED_GAMEDATA)) {
            updatedGameDataObject = _.merge(that.objectAlreadyPresent, updatedGameDataObject);
            that.CHANGED_GAMEDATA = _.remove(that.CHANGED_GAMEDATA, function (obj:any) {
                return obj.ID !== that.objectAlreadyPresent.ID;
            });
        }

        this.CHANGED_GAMEDATA.push(updatedGameDataObject);
        
    }

    filterGameDataObjectArray(objectArray: any, filterCriterion: string): Object[] {
        let filteredObjectArray = objectArray.filter(function (el){
          if (!el.$type) {
            return false;
          }
          return (el.$type === filterCriterion || el.ID === filterCriterion);
        });
        return filteredObjectArray;
    }

    getAllModifiedObjects(): Object[] {
        return this.CHANGED_GAMEDATA;
    }

    getAbilityByID(abilityID): Object {
        let result = this.filterGameDataObjectArray(this.getDb('abilities'), abilityID);
        return result[0];
    }

    constructor (private messageService: MessageService) {}
}