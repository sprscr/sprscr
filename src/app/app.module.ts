import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';

import { AppRoutingModule } from './/app-routing.module';

import { AppComponent } from './app.component';
import { CharactersComponent } from './components/characters/characters.component';
import { CharacterDetailComponent } from './components/character-detail/character-detail.component';
import { ProgressionTablesComponent } from './components/progression-tables/progression-tables.component';
import { ProgressionTableDetailComponent } from './components/progression-table-detail/progression-table-detail.component';
import { MessagesComponent } from './components/messages/messages.component';
import { LoadDataComponent } from './components/load-data/load-data.component';
import { UploaderComponent } from './components/uploader/uploader.component';
import { ComingSoonComponent } from './components/coming-soon/coming-soon.component';
import { ExportDataComponent } from './components/export-data/export-data.component';
import { AbilitiesComponent } from './components/abilities/abilities.component';
import { AbilityDetailComponent } from './components/ability-detail/ability-detail.component';
import { AttacksComponent } from './components/attacks/attacks.component';
import { AttackDetailComponent } from './components/attack-detail/attack-detail.component';
import { StatusEffectsComponent } from './components/status-effects/status-effects.component';
import { StatuseffectDetailComponent } from './components/statuseffect-detail/statuseffect-detail.component';
import { ListItemActionsComponent } from './components/list-item-actions/list-item-actions.component';

import { GameDataService } from './services/gamedata/gamedata.service';
import { MessageService } from './services/messages/message.service';
import { DbLoadService } from './services/db-load/db-load.service';
import { GameDataObjectTypes } from './data-model/game-data-object-types';

import { CollapsibleModule } from 'angular2-collapsible';





@NgModule({
  declarations: [
    AppComponent,
    CharactersComponent,
    CharacterDetailComponent,
    MessagesComponent,
    ProgressionTablesComponent,
    ProgressionTableDetailComponent,
    LoadDataComponent,
    UploaderComponent,
    ComingSoonComponent,
    ExportDataComponent,
    AbilityDetailComponent,
    ListItemActionsComponent,
    StatuseffectDetailComponent,
    AttackDetailComponent,
    StatusEffectsComponent,
    AbilitiesComponent,
    AttacksComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
	  FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    CollapsibleModule
  ],
  providers: [
    GameDataService,
    MessageService,
    DbLoadService,
    GameDataObjectTypes
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
