export const DEF_GLOBAL: string = `{
  "GameDataObjects": [
    {
      "$type": "Game.GameData.AttributeGameData, Assembly-CSharp",
      "DebugName": "Constitution",
      "ID": "72712dca-8f95-42ef-bca1-be2e99f69de3",
      "Components": [
        {
          "$type": "Game.GameData.AttributeComponent, Assembly-CSharp",
          "Type": "Constitution",
          "DisplayName": 273,
          "ShortName": 314,
          "Description": 473,
          "BonusesFormatString": 368,
          "Icon": "gui/icons/gamesystems/attribute_constitution.png",
          "GlossaryEntryID": "e8b97ee6-f626-4ec2-8c26-b891747ec0af"
        }
      ]
    },
    {
      "$type": "Game.GameData.AttributeGameData, Assembly-CSharp",
      "DebugName": "Dexterity",
      "ID": "736f0d1d-b98f-46f1-a528-80ee6f53516a",
      "Components": [
        {
          "$type": "Game.GameData.AttributeComponent, Assembly-CSharp",
          "Type": "Dexterity",
          "DisplayName": 46,
          "ShortName": 313,
          "Description": 471,
          "BonusesFormatString": 1582,
          "Icon": "gui/icons/gamesystems/attribute_dexterity.png",
          "GlossaryEntryID": "9a89d517-2bdf-4022-b67d-79d9b5225e4a"
        }
      ]
    },
    {
      "$type": "Game.GameData.AttributeGameData, Assembly-CSharp",
      "DebugName": "Intellect",
      "ID": "cadf2f85-9a68-4cfd-80ac-429bac09056f",
      "Components": [
        {
          "$type": "Game.GameData.AttributeComponent, Assembly-CSharp",
          "Type": "Intellect",
          "DisplayName": 47,
          "ShortName": 316,
          "Description": 472,
          "BonusesFormatString": 371,
          "Icon": "gui/icons/gamesystems/attribute_intellect.png",
          "GlossaryEntryID": "7691c089-6459-4cb0-9aa9-69158771a4fd"
        }
      ]
    },
    {
      "$type": "Game.GameData.AttributeGameData, Assembly-CSharp",
      "DebugName": "Might",
      "ID": "bbd14702-d38a-4627-964f-24cac6e0ee1f",
      "Components": [
        {
          "$type": "Game.GameData.AttributeComponent, Assembly-CSharp",
          "Type": "Might",
          "DisplayName": 45,
          "ShortName": 312,
          "Description": 470,
          "BonusesFormatString": 367,
          "Icon": "gui/icons/gamesystems/attribute_might.png",
          "GlossaryEntryID": "e19d51db-65ee-4e5c-bd20-d92e6353a99a"
        }
      ]
    },
    {
      "$type": "Game.GameData.AttributeGameData, Assembly-CSharp",
      "DebugName": "Perception",
      "ID": "6f687c04-019c-498f-b202-cc8679fc511f",
      "Components": [
        {
          "$type": "Game.GameData.AttributeComponent, Assembly-CSharp",
          "Type": "Perception",
          "DisplayName": 274,
          "ShortName": 315,
          "Description": 474,
          "BonusesFormatString": 370,
          "Icon": "gui/icons/gamesystems/attribute_perception.png",
          "GlossaryEntryID": "ccd4b925-c002-4b37-9625-d3420d04125a"
        }
      ]
    },
    {
      "$type": "Game.GameData.AttributeGameData, Assembly-CSharp",
      "DebugName": "Resolve",
      "ID": "2b56c512-6456-44c3-952a-e16431e48ccb",
      "Components": [
        {
          "$type": "Game.GameData.AttributeComponent, Assembly-CSharp",
          "Type": "Resolve",
          "DisplayName": 44,
          "ShortName": 317,
          "Description": 469,
          "BonusesFormatString": 372,
          "Icon": "gui/icons/gamesystems/attribute_resolve.png",
          "GlossaryEntryID": "5326bbbb-0bfb-478c-af75-66a21111ead3"
        }
      ]
    },
    {
      "$type": "Game.GameData.GraphicsQualityGameData, Assembly-CSharp",
      "DebugName": "GFX_High",
      "ID": "e27543e9-e692-4a22-bd2a-6a9ee8e21f5f",
      "Components": [
        {
          "$type": "Game.GameData.GraphicsQualityComponent, Assembly-CSharp",
          "SortOrder": 2,
          "DisplayName": 1863,
          "MSAA": 2,
          "Glow": "true",
          "AO": "true",
          "HQShadows": "true",
          "Buoyancy": "true",
          "Wildlife": "true",
          "WeatherEffects": "true",
          "Lighting": "true",
          "HairTransparency": "true",
          "FXAA": "true",
          "FootstepEffects": "true",
          "Vegetation": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.GraphicsQualityGameData, Assembly-CSharp",
      "DebugName": "GFX_Low",
      "ID": "2c1bbdfe-6aed-4e33-86b3-24382566448c",
      "Components": [
        {
          "$type": "Game.GameData.GraphicsQualityComponent, Assembly-CSharp",
          "SortOrder": 0,
          "DisplayName": 1862,
          "MSAA": 1,
          "Glow": "false",
          "AO": "false",
          "HQShadows": "false",
          "Buoyancy": "false",
          "Wildlife": "false",
          "WeatherEffects": "false",
          "Lighting": "false",
          "HairTransparency": "false",
          "FXAA": "false",
          "FootstepEffects": "false",
          "Vegetation": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.GraphicsQualityGameData, Assembly-CSharp",
      "DebugName": "GFX_Med",
      "ID": "5c6212a7-e4e3-4364-a7cb-b2f5567d436c",
      "Components": [
        {
          "$type": "Game.GameData.GraphicsQualityComponent, Assembly-CSharp",
          "SortOrder": 1,
          "DisplayName": 439,
          "MSAA": 1,
          "Glow": "false",
          "AO": "false",
          "HQShadows": "true",
          "Buoyancy": "true",
          "Wildlife": "true",
          "WeatherEffects": "true",
          "Lighting": "true",
          "HairTransparency": "true",
          "FXAA": "true",
          "FootstepEffects": "true",
          "Vegetation": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.LegacyHistoryMappingGameData, Assembly-CSharp",
      "DebugName": "LegacyHistoryMappings",
      "ID": "fc04c213-2c87-41c8-a469-e57f6509e3c9",
      "Components": [
        {
          "$type": "Game.GameData.LegacyHistoryMappingComponent, Assembly-CSharp",
          "DefaultHistories": [
            {
              "HistoryFile": "resources/histories/benevolentsoul.bytes",
              "DisplayName": 4404,
              "DisplayDescription": 4407
            },
            {
              "HistoryFile": "resources/histories/fairandbalanced.bytes",
              "DisplayName": 4405,
              "DisplayDescription": 4406
            },
            {
              "HistoryFile": "resources/histories/survivalofthefittest.bytes",
              "DisplayName": 4408,
              "DisplayDescription": 4409
            },
            {
              "HistoryFile": "resources/histories/darktimes.bytes",
              "DisplayName": 4410,
              "DisplayDescription": 4411
            },
            {
              "HistoryFile": "resources/histories/keeperofsecrets.bytes",
              "DisplayName": 4479,
              "DisplayDescription": 4480
            },
            {
              "HistoryFile": "resources/histories/everythingbad.bytes",
              "DisplayName": 4412,
              "DisplayDescription": 4413
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.PartySizeLevelScalingTableGameData, Assembly-CSharp",
      "DebugName": "PS-DefaultScalingRules",
      "ID": "60c7ffde-42b8-4a51-8d16-ed4fee2f1e58",
      "Components": [
        {
          "$type": "Game.GameData.PartySizeLevelScalingTableComponent, Assembly-CSharp",
          "ExpectedPartySizeLevelAdjustments": [
            {
              "ExpectedDifferenceMin": 4,
              "ExpectedDifferenceMax": 4,
              "AdjustedLevelAmount": 4
            },
            {
              "ExpectedDifferenceMin": 3,
              "ExpectedDifferenceMax": 3,
              "AdjustedLevelAmount": 3
            },
            {
              "ExpectedDifferenceMin": 2,
              "ExpectedDifferenceMax": 2,
              "AdjustedLevelAmount": 2
            },
            {
              "ExpectedDifferenceMin": 1,
              "ExpectedDifferenceMax": 1,
              "AdjustedLevelAmount": 1
            },
            {
              "ExpectedDifferenceMin": 0,
              "ExpectedDifferenceMax": 0,
              "AdjustedLevelAmount": 0
            },
            {
              "ExpectedDifferenceMin": -1,
              "ExpectedDifferenceMax": -1,
              "AdjustedLevelAmount": -1
            },
            {
              "ExpectedDifferenceMin": -2,
              "ExpectedDifferenceMax": -2,
              "AdjustedLevelAmount": -2
            },
            {
              "ExpectedDifferenceMin": -3,
              "ExpectedDifferenceMax": -3,
              "AdjustedLevelAmount": -3
            },
            {
              "ExpectedDifferenceMin": -4,
              "ExpectedDifferenceMax": -4,
              "AdjustedLevelAmount": -4
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.PartySizeLevelScalingTableGameData, Assembly-CSharp",
      "DebugName": "PS-NoScalingRules",
      "ID": "a203d7f4-7264-4ec2-9126-721c501b7339",
      "Components": [
        {
          "$type": "Game.GameData.PartySizeLevelScalingTableComponent, Assembly-CSharp",
          "ExpectedPartySizeLevelAdjustments": [
            {
              "ExpectedDifferenceMin": -100,
              "ExpectedDifferenceMax": 100,
              "AdjustedLevelAmount": 0
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.PlayerVoiceSetGameData, Assembly-CSharp",
      "DebugName": "PlayerVoiceSets",
      "ID": "5bdf83cc-88d8-47db-9a8b-7d01bf6ad4dd",
      "Components": [
        {
          "$type": "Game.GameData.PlayerVoiceSetComponent, Assembly-CSharp",
          "PlayerSpeakers": [
            {
              "Tag": "Cocky Male",
              "SpeakerDisplayName": 3968,
              "SpeakerReferenceID": "8a7fabf9-1057-4989-b2d5-6c6eb20aa1f5",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Cocky Female",
              "SpeakerDisplayName": 3968,
              "SpeakerReferenceID": "84932c6f-a47d-43a9-b6b4-3921915846d1",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Feisty Male",
              "SpeakerDisplayName": 3969,
              "SpeakerReferenceID": "3cedde6a-6d8a-4b51-97f9-119d492c5393",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Feisty Female",
              "SpeakerDisplayName": 3969,
              "SpeakerReferenceID": "218967e7-b4f9-436e-83e7-3526a98d2408",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Kind Male",
              "SpeakerDisplayName": 3970,
              "SpeakerReferenceID": "cde5b489-c68e-435f-803a-5ba69ae908b1",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Kind Female",
              "SpeakerDisplayName": 3970,
              "SpeakerReferenceID": "638b0eeb-6fa4-4149-a0f4-c5a88077295d",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Mystic Male",
              "SpeakerDisplayName": 2629,
              "SpeakerReferenceID": "96ca7cbb-99fa-437a-9fa7-49102fc27cd5",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Mystic Female",
              "SpeakerDisplayName": 2629,
              "SpeakerReferenceID": "e3dfea51-4444-45b1-9b93-b3bb73290d4f",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Noble Male",
              "SpeakerDisplayName": 3972,
              "SpeakerReferenceID": "f68bd2e1-2c7a-4b4c-92d2-bc0c3978c9a0",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Noble Female",
              "SpeakerDisplayName": 3972,
              "SpeakerReferenceID": "074d0084-e7ce-4024-9569-5a099c81d79b",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Sinister Male",
              "SpeakerDisplayName": 3973,
              "SpeakerReferenceID": "c1b14138-ead1-4c9c-83b0-2459548922e3",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Sinister Female",
              "SpeakerDisplayName": 3973,
              "SpeakerReferenceID": "aeaff45f-fa21-46fd-80e8-d74babe3760c",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Smooth Male",
              "SpeakerDisplayName": 3974,
              "SpeakerReferenceID": "e4b11255-7990-416b-9d2b-22616ebaae3e",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Smooth Female",
              "SpeakerDisplayName": 3974,
              "SpeakerReferenceID": "8866a042-7418-47a1-b973-71c3fef1b997",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Stoic Male",
              "SpeakerDisplayName": 3301,
              "SpeakerReferenceID": "4c3a4b35-8bef-492c-8be9-0fa07642d1b6",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "Stoic Female",
              "SpeakerDisplayName": 3301,
              "SpeakerReferenceID": "1c5ea911-7abd-4a47-9f90-f55c2346679d",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "None Male",
              "SpeakerDisplayName": 343,
              "SpeakerReferenceID": "d504098d-0dc0-4d89-ba5d-c1833bf43fe5",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "None Female",
              "SpeakerDisplayName": 343,
              "SpeakerReferenceID": "37b6c283-101f-430a-ae7c-1d95c6199449",
              "Conditionals": {
                "Operator": 0,
                "Components": []
              }
            },
            {
              "Tag": "VM_Grog",
              "SpeakerDisplayName": 4726,
              "SpeakerReferenceID": "431ad993-b484-49b5-9b12-0c9b1732356e",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Tag": "VM_Keyleth",
              "SpeakerDisplayName": 4728,
              "SpeakerReferenceID": "7b897c60-1ce8-4d36-a8f5-7af7bcd41291",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Tag": "VM_PercivalDeRolo",
              "SpeakerDisplayName": 4729,
              "SpeakerReferenceID": "98294120-ae03-40f7-b3f6-61911721289f",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Tag": "VM_Pike",
              "SpeakerDisplayName": 4730,
              "SpeakerReferenceID": "613c932b-1d9b-42da-8c95-f0449bde6ce0",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Tag": "VM_Scanlan",
              "SpeakerDisplayName": 4731,
              "SpeakerReferenceID": "b6633843-fbf5-4904-9d3c-9dd548de49a8",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Tag": "VM_ShaunGilmore",
              "SpeakerDisplayName": 4732,
              "SpeakerReferenceID": "e672492e-a792-4cf1-91bb-72a17baeebf2",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Tag": "VM_Vaxildan",
              "SpeakerDisplayName": 4733,
              "SpeakerReferenceID": "7023048b-f0ef-4342-be52-27f32f6ac40a",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Tag": "VM_Vexahlia",
              "SpeakerDisplayName": 4727,
              "SpeakerReferenceID": "d919f000-9446-472d-8289-495b0e9223ae",
              "Conditionals": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean HasPackage(ProductPackage)",
                      "Parameters": [
                        "LAXE"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 558842710,
                      "ParameterHash": -175338304
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.PoE1GodBoonsGameData, Assembly-CSharp",
      "DebugName": "PoE1GodBoons",
      "ID": "dbc694f1-8771-4b0a-9c82-f6552c452865",
      "Components": [
        {
          "$type": "Game.GameData.PoE1GodBoonsComponent, Assembly-CSharp",
          "Boons": [
            {
              "GlobalVariable": "b_Berath_Vow",
              "String": 4
            },
            {
              "GlobalVariable": "b_Galawain_Vow",
              "String": 5
            },
            {
              "GlobalVariable": "b_Hylea_Vow",
              "String": 6
            },
            {
              "GlobalVariable": "b_Rymrgand_Vow",
              "String": 7
            },
            {
              "GlobalVariable": "b_TLN_Skaens_Boon_Talent",
              "String": 8
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.SaveGameImportDataGameData, Assembly-CSharp",
      "DebugName": "SaveGameImportData",
      "ID": "6d121434-5ab9-4844-af31-0563cf4e585b",
      "Components": [
        {
          "$type": "Game.GameData.SaveGameImportDataComponent, Assembly-CSharp",
          "AbilitiesOrTalentsToSearchFor": [
            {
              "Comment": "Sacrificed Aloth",
              "LegacyVariableName": "TLN_Effigys_Resentment_Aloth_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Aloth_Talent"
            },
            {
              "Comment": "Sacrificed Eder",
              "LegacyVariableName": "TLN_Effigys_Resentment_Eder_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Eder_Talent"
            },
            {
              "Comment": "Sacrificed Devil of Caroc",
              "LegacyVariableName": "PX1_TLN_Effigys_Resentment_Devil_of_Caroc_Talent",
              "CurrentGameVariableName": "b_PX1_TLN_Effigys_Resentment_Devil_of_Caroc"
            },
            {
              "Comment": "Sacrificed Durance",
              "LegacyVariableName": "TLN_Effigys_Resentment_GGP_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Durance_Talent"
            },
            {
              "Comment": "Sacrificed Grieving Mother",
              "LegacyVariableName": "TLN_Effigys_Resentment_GM_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Grieving_Mother_Talent"
            },
            {
              "Comment": "Sacrificed Hiravias",
              "LegacyVariableName": "TLN_Effigys_Resentment_Hiravias_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Hiravias_Talent"
            },
            {
              "Comment": "Sacrificed Kana",
              "LegacyVariableName": "TLN_Effigys_Resentment_Kana_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Kana_Talent"
            },
            {
              "Comment": "Sacrificed Maneha",
              "LegacyVariableName": "PX2_TLN_Effigys_Resentment_Maneha_Talent",
              "CurrentGameVariableName": "b_PX2_TLN_Effigys_Resentment_Maneha_Talent"
            },
            {
              "Comment": "Sacrificed Pallegina",
              "LegacyVariableName": "TLN_Effigys_Resentment_Pallegina_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Pallegina_Talent"
            },
            {
              "Comment": "Sacrificed Sagani",
              "LegacyVariableName": "TLN_Effigys_Resentment_Sagani_Talent",
              "CurrentGameVariableName": "b_TLN_Effigys_Resentment_Sagani_Talent"
            },
            {
              "Comment": "Sacrificed Zahua",
              "LegacyVariableName": "PX1_TLN_Effigys_Resentment_Zahua_Talent",
              "CurrentGameVariableName": "b_PX1_TLN_Effigys_Resentment_Zahua_Talent"
            },
            {
              "Comment": "Skaen's Boon (i.e., Skaen's Pledge)",
              "LegacyVariableName": "TLN_Skaens_Boon_Talent",
              "CurrentGameVariableName": "b_TLN_Skaens_Boon_Talent"
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Alchemy",
      "ID": "1b26af87-fd19-4dc6-9380-4f5cec7aefd5",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2561,
          "Description": 2918,
          "Icon": "gui/icons/gamesystems/skill_herbalism.png",
          "ActiveSkill": "true",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "9b4f2f19-d69b-40a9-869a-68e72790bf7c"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Arcana",
      "ID": "e5f33551-ec5c-4cc8-b5f2-1eb03d210374",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2560,
          "Description": 2914,
          "Icon": "gui/icons/gamesystems/skill_arcana.png",
          "ActiveSkill": "true",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "eb74abcb-0f2e-4e6e-9343-7005d4a13336"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Athletics",
      "ID": "fefc4d3d-250d-4c32-85e0-62a851240e62",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 35,
          "Description": 1397,
          "Icon": "gui/icons/gamesystems/skill_athletics.png",
          "ActiveSkill": "true",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "bad4f03a-e394-4ca1-b0ea-47ea687ac748"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Bluff",
      "ID": "6fa62ec8-6695-4a1d-8553-7486b0ac20c4",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2546,
          "Description": 2547,
          "Icon": "gui/icons/gamesystems/skill_bluff.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "2327acc2-5f66-46da-8245-6c5e6a89e4e6"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Diplomacy",
      "ID": "c7e22ee0-bdd6-4ca8-99f0-a46f3dd66606",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2563,
          "Description": 2917,
          "Icon": "gui/icons/gamesystems/skill_diplomacy.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "b34ce9a3-e4bf-4c8a-be85-0f3a902e7760"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Explosives",
      "ID": "e67f20e3-5bf9-4d87-9bda-211405107362",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2559,
          "Description": 2915,
          "Icon": "gui/icons/gamesystems/skill_alchemy.png",
          "ActiveSkill": "true",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "e7efb28c-040c-4093-95af-4bf9ee4b13a4"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "History",
      "ID": "c6b6f87a-0381-4219-93eb-77ae2b64f0af",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2562,
          "Description": 2919,
          "Icon": "gui/icons/gamesystems/skill_history.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "8aad0dd8-b8c9-4d1e-854f-d30a30b27bc5"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Insight",
      "ID": "f5d2c2bf-1ffd-4741-b6d8-31ae01985d4b",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2548,
          "Description": 2549,
          "Icon": "gui/icons/gamesystems/skill_insight.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "41396f1a-19ab-4221-87f4-b29bfed3cdb2"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Intimidate",
      "ID": "3ad9194b-4f41-480e-aa75-1c82df43be84",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2564,
          "Description": 2920,
          "Icon": "gui/icons/gamesystems/skill_intimidate.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "ce820ef3-7eaa-434c-94dc-31a0550d7ed9"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Mechanics",
      "ID": "3affbb70-86bd-41f3-82c6-325326d40796",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 37,
          "Description": 1399,
          "Icon": "gui/icons/gamesystems/skill_mechanics.png",
          "ActiveSkill": "true",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "b7de4f19-2150-47c9-b1dd-f8cf0ebdbc7c"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Metaphysics",
      "ID": "98051b85-d7e5-4746-a66c-260e2fcd2d4b",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2565,
          "Description": 2921,
          "Icon": "gui/icons/gamesystems/skill_metaphysics.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "cf451c4d-ba71-4362-a04d-7522d15f0673"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Religion",
      "ID": "98cc5a0d-1387-42e3-9202-417bc42b53ad",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2566,
          "Description": 2922,
          "Icon": "gui/icons/gamesystems/skill_religion.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "9927afaf-b781-462a-a4c3-be793f65fe65"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Sleight Of Hand",
      "ID": "3b073ebf-0a4a-48a3-947d-ad38bef10b36",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2567,
          "Description": 2923,
          "Icon": "gui/icons/gamesystems/skill_sleight_of_hand.png",
          "ActiveSkill": "true",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "bf374e10-6457-4f4f-849e-6d6eb5cd5b9d"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Stealth",
      "ID": "8cc41e27-e62f-4e75-9f7d-ae47986895d2",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 34,
          "Description": 1396,
          "Icon": "gui/icons/gamesystems/skill_stealth.png",
          "ActiveSkill": "true",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "b7232834-92f2-4bf3-9cd9-5a375f93d7f3"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Streetwise",
      "ID": "eb8da173-4a3e-4910-94e8-dd0c1436ca40",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 2568,
          "Description": 2924,
          "Icon": "gui/icons/gamesystems/skill_streetwise.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "13c2b75d-7957-4d76-bc86-46ecc0f50559"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillGameData, Assembly-CSharp",
      "DebugName": "Survival",
      "ID": "9c7962b8-bf69-4670-ba67-acc86a09fca8",
      "Components": [
        {
          "$type": "Game.GameData.SkillComponent, Assembly-CSharp",
          "DisplayName": 38,
          "Description": 1400,
          "Icon": "gui/icons/gamesystems/skill_survival.png",
          "ActiveSkill": "false",
          "AudioEventListID": "cf7c7e4c-0248-47f7-88ed-faffbf904646",
          "GlossaryEntryID": "43814de1-9801-4a24-a6b9-2ec9f0ba8d7b"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponMetaTypeGameData, Assembly-CSharp",
      "DebugName": "WMT_Bows",
      "ID": "e9326c04-95a5-4eb4-87e7-d52417789f32",
      "Components": [
        {
          "$type": "Game.GameData.WeaponMetaTypeComponent, Assembly-CSharp",
          "WeaponTypesIDs": [
            "2e9ee8be-aa84-47ba-8590-c9652cf9cb42",
            "3a67f4c2-3bfd-4665-ae85-3d654b333e91",
            "7e80095e-047a-40c3-b85a-ca4fffdf7a4d",
            "ca1e480d-aa0c-4433-86e9-80094e238766"
          ],
          "DisplayName": 3745
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponMetaTypeGameData, Assembly-CSharp",
      "DebugName": "WMT_Firearms",
      "ID": "ad8e6fc5-1e04-43e1-9d05-29e2b2ef1680",
      "Components": [
        {
          "$type": "Game.GameData.WeaponMetaTypeComponent, Assembly-CSharp",
          "WeaponTypesIDs": [
            "7269d072-f54a-4609-a2cf-585d7b2421ec",
            "8076be03-6667-496d-b162-4b758a1fae58",
            "fd08a210-659c-44fa-821b-bd454e117ee8"
          ],
          "DisplayName": 3746
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponMetaTypeGameData, Assembly-CSharp",
      "DebugName": "WMT_MagicalImplements",
      "ID": "4ea44cd0-57ef-44eb-b0cf-5ab2517e5714",
      "Components": [
        {
          "$type": "Game.GameData.WeaponMetaTypeComponent, Assembly-CSharp",
          "WeaponTypesIDs": [
            "825a1d65-10f3-42d3-bc7d-32e78c69d286",
            "34605d2f-d6d4-4d24-a456-fdcf1bf09114",
            "8242e277-9b06-47e4-b928-78aa4bde1b08"
          ],
          "DisplayName": 3747
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponMetaTypeGameData, Assembly-CSharp",
      "DebugName": "WMT_OneHanded",
      "ID": "7f150feb-1997-49e9-8af9-fe3588511c88",
      "Components": [
        {
          "$type": "Game.GameData.WeaponMetaTypeComponent, Assembly-CSharp",
          "WeaponTypesIDs": [
            "22d2ffff-2b2d-42c1-ba2e-13093d6afe81",
            "2b9588f2-1f48-4134-af0d-da5b05e928d0",
            "553f80c7-f0d7-4420-a243-cee619e3c1e3",
            "cd00a95d-0ec7-42e0-8503-73bc6c2bd4e6",
            "51760dde-895a-4cda-896d-e84991de13a0",
            "7982d164-e693-49ed-a929-6c0fb042c305",
            "aeb351c0-8da1-44b2-ae8f-baf981ca732d",
            "74596090-97ff-41da-a879-e65167e1158a",
            "b267b73f-9691-44d3-80b9-c997bbff5e76",
            "a42f4b03-2b0d-4824-ab42-7e1fd82e1102",
            "69681e5e-1702-42c4-a14d-6efe79e5642c",
            "04e84873-5e83-4861-b75a-7ba6736e73b3"
          ],
          "DisplayName": 3748
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponMetaTypeGameData, Assembly-CSharp",
      "DebugName": "WMT_Shields",
      "ID": "6f6e5f2f-235f-492d-8f87-94b8c8e16aa2",
      "Components": [
        {
          "$type": "Game.GameData.WeaponMetaTypeComponent, Assembly-CSharp",
          "WeaponTypesIDs": [
            "5f1f070e-2f5a-431a-9845-cabded859421",
            "abf6e9ef-982d-4c1f-91fc-bb4d29a3e440",
            "a5446f73-af00-43fb-bade-a53c7b96dfbf"
          ],
          "DisplayName": 3750
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponMetaTypeGameData, Assembly-CSharp",
      "DebugName": "WMT_TwoHanded",
      "ID": "a03f3322-6de1-40de-9f96-869fd51ee681",
      "Components": [
        {
          "$type": "Game.GameData.WeaponMetaTypeComponent, Assembly-CSharp",
          "WeaponTypesIDs": [
            "ba9b7ae5-dbfe-41d0-971f-bd9b8a4ddf80",
            "60dacd25-de48-469d-a53c-448a982998c3",
            "ff97dccb-ba4f-425a-a9c7-d4233c81fde3",
            "682a111b-431e-4b61-9700-6da4894996ea",
            "fd0b335c-dbd0-4ef4-a85e-b48011c541a0",
            "c672c8d3-8f02-40db-a451-e991eeb71bdd"
          ],
          "DisplayName": 3749
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Arbalest",
      "ID": "2e9ee8be-aa84-47ba-8590-c9652cf9cb42",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 0,
          "ArchetypeID": "f4b5d8f4-8b66-4243-b9f6-1a626f9848fc",
          "Type": "Arbalest"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Arquebus",
      "ID": "7269d072-f54a-4609-a2cf-585d7b2421ec",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 1,
          "ArchetypeID": "2e523de0-b85c-42e2-acd3-644364d2efe9",
          "Type": "Arquebus"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "BattleAxe",
      "ID": "2b9588f2-1f48-4134-af0d-da5b05e928d0",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 2,
          "ArchetypeID": "431a8af0-0137-4605-b1a6-a67d501cc7a3",
          "Type": "BattleAxe"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Blunderbuss",
      "ID": "8076be03-6667-496d-b162-4b758a1fae58",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 3,
          "ArchetypeID": "4a42e444-2b48-43ee-8655-76fe36e89927",
          "Type": "Blunderbuss"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Club",
      "ID": "553f80c7-f0d7-4420-a243-cee619e3c1e3",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 4,
          "ArchetypeID": "28c3801b-4b79-411c-8d0c-2329d43309fb",
          "Type": "Club"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Crossbow",
      "ID": "3a67f4c2-3bfd-4665-ae85-3d654b333e91",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 5,
          "ArchetypeID": "fda980ec-4d4c-4a59-b1d9-fc931b1c7a12",
          "Type": "Crossbow"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Dagger",
      "ID": "22d2ffff-2b2d-42c1-ba2e-13093d6afe81",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 6,
          "ArchetypeID": "fffe4faa-8aae-4894-add5-2c9672aeadb4",
          "Type": "Dagger"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Estoc",
      "ID": "ba9b7ae5-dbfe-41d0-971f-bd9b8a4ddf80",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 7,
          "ArchetypeID": "5282f80a-2527-4382-8144-2b80765519bb",
          "Type": "Estoc"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Flail",
      "ID": "cd00a95d-0ec7-42e0-8503-73bc6c2bd4e6",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 8,
          "ArchetypeID": "c07155e3-cb06-4251-a030-66240b845fdc",
          "Type": "Flail"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "GreatSword",
      "ID": "60dacd25-de48-469d-a53c-448a982998c3",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 11,
          "ArchetypeID": "3530e7a2-ec4f-4d5f-a436-53e26ed3e075",
          "Type": "GreatSword"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Hatchet",
      "ID": "51760dde-895a-4cda-896d-e84991de13a0",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 12,
          "ArchetypeID": "c81e92d4-8cdb-4a77-ba1e-f48aaac2faef",
          "Type": "Hatchet"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "HuntingBow",
      "ID": "7e80095e-047a-40c3-b85a-ca4fffdf7a4d",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 13,
          "ArchetypeID": "46546968-6fdd-47e7-bdcb-d93a5f4e920d",
          "Type": "HuntingBow"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "LargeShield",
      "ID": "a5446f73-af00-43fb-bade-a53c7b96dfbf",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 57,
          "ArchetypeID": "59d27cb7-b780-447f-a803-ea3be9d5b9c8",
          "Type": "LargeShield"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Mace",
      "ID": "7982d164-e693-49ed-a929-6c0fb042c305",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 15,
          "ArchetypeID": "471b717a-402c-4005-b664-8873dd348023",
          "Type": "Mace"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "MediumShield",
      "ID": "abf6e9ef-982d-4c1f-91fc-bb4d29a3e440",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 3580,
          "ArchetypeID": "1c147d83-643e-4807-b2ab-e862d33b2648",
          "Type": "MediumShield"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "MorningStar",
      "ID": "ff97dccb-ba4f-425a-a9c7-d4233c81fde3",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 17,
          "ArchetypeID": "f1c45417-0b08-4d82-8b71-01b5b2293fec",
          "Type": "MorningStar"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Pike",
      "ID": "682a111b-431e-4b61-9700-6da4894996ea",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 18,
          "ArchetypeID": "605ef110-e7ec-4095-a2ff-28e4e7ebce9d",
          "Type": "Pike"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Pistol",
      "ID": "fd08a210-659c-44fa-821b-bd454e117ee8",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 19,
          "ArchetypeID": "982e3d9d-ce45-4796-8c12-b094879dc770",
          "Type": "Pistol"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Pollaxe",
      "ID": "fd0b335c-dbd0-4ef4-a85e-b48011c541a0",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 10,
          "ArchetypeID": "3213c60a-3fa4-48a0-8742-815ed9427b5f",
          "Type": "Pollaxe"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Quaterstaff",
      "ID": "c672c8d3-8f02-40db-a451-e991eeb71bdd",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 20,
          "ArchetypeID": "ad031f74-7dab-44c3-ad7b-78382463a07f",
          "Type": "Quarterstaff"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Rapier",
      "ID": "aeb351c0-8da1-44b2-ae8f-baf981ca732d",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 21,
          "ArchetypeID": "f5b35ad9-9d8e-4a1c-b032-22b566cb9da7",
          "Type": "Rapier"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Rod",
      "ID": "34605d2f-d6d4-4d24-a456-fdcf1bf09114",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 22,
          "ArchetypeID": "b5924a1b-2194-49a2-ac78-300d80dfa179",
          "Type": "Rod"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Sabre",
      "ID": "74596090-97ff-41da-a879-e65167e1158a",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 210,
          "ArchetypeID": "1a3485bc-ec1a-4089-8086-28da996c2aad",
          "Type": "Sabre"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Scepter",
      "ID": "825a1d65-10f3-42d3-bc7d-32e78c69d286",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 23,
          "ArchetypeID": "9f15c1ec-aa81-4846-82de-165d582bfd57",
          "Type": "Sceptre"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "SmallShield",
      "ID": "5f1f070e-2f5a-431a-9845-cabded859421",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 60,
          "ArchetypeID": "f90e05cf-5d27-4def-8fe3-eb2b6b162658",
          "Type": "SmallShield"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Spear",
      "ID": "b267b73f-9691-44d3-80b9-c997bbff5e76",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 24,
          "ArchetypeID": "82a6a6cc-3834-493e-99c9-07ad4bdf6210",
          "Type": "Spear"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Stiletto",
      "ID": "a42f4b03-2b0d-4824-ab42-7e1fd82e1102",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 25,
          "ArchetypeID": "2b19eb13-bb2a-4c29-bba7-4dc3258c753d",
          "Type": "Stiletto"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Sword",
      "ID": "69681e5e-1702-42c4-a14d-6efe79e5642c",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 26,
          "ArchetypeID": "a97a376c-8ad8-4fa5-8dbb-12840414b7d1",
          "Type": "Sword"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Unarmed",
      "ID": "e6e44e06-9eb7-4c71-8703-c5e4a0f6044a",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 5195,
          "ArchetypeID": "00000000-0000-0000-0000-000000000000",
          "Type": "Unarmed"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "Wand",
      "ID": "8242e277-9b06-47e4-b928-78aa4bde1b08",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 27,
          "ArchetypeID": "58a78533-9026-49c8-9e28-f2668a45c416",
          "Type": "Wand"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "WarBow",
      "ID": "ca1e480d-aa0c-4433-86e9-80094e238766",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 29,
          "ArchetypeID": "0bc6b44a-df87-4995-af66-196fbfd1d821",
          "Type": "WarBow"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeaponTypeGameData, Assembly-CSharp",
      "DebugName": "WarHammer",
      "ID": "04e84873-5e83-4861-b75a-7ba6736e73b3",
      "Components": [
        {
          "$type": "Game.GameData.WeaponTypeComponent, Assembly-CSharp",
          "DisplayName": 30,
          "ArchetypeID": "af824950-db20-421c-a60c-dd1f7c7a6f08",
          "Type": "WarHammer"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ClearSunnyDay",
      "ID": "02832d5f-0942-4971-b41f-e2983f4e8391",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Warm",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Light",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ClearSunnyDayWindy",
      "ID": "ff395f8e-0c67-402f-843f-09e1d083be12",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Warm",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Heavy",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Light",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ClearSunnyDayWithBreeze",
      "ID": "74b0568f-21b3-4727-90f6-36416812d2a0",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Warm",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Random",
          "WindDirection": 300,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Light",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "CloudyWindy",
      "ID": "41aadcdc-bd84-4ce7-9112-23a28a6391c1",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Warm",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Moderate",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "HeavyWindAndRainNoTL",
      "ID": "a060385d-d2cd-4ede-a527-3065871d5b14",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Heavy",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "Zero",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Heavy",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "Far",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "HeavyWindAndRainTNoL",
      "ID": "a82c75ba-37a8-436f-809c-079836c4fb29",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Heavy",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "Zero",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Heavy",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "Thunder",
          "LightningDistance": "Far",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "HotModerateWindLightDust",
      "ID": "f31b99f2-45ef-4746-92bc-31002883a9de",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Hot",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "Dust",
          "AirConditionStrengthType": "Light",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "LightRainLightWindOvercast",
      "ID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Light",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "LightWindAndHeavyRainTNoL",
      "ID": "bcd6a96b-bce7-41d1-bdd1-a470f7b65459",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "Zero",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Heavy",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "Thunder",
          "LightningDistance": "Far",
          "LightningStrengthType": "Moderate"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "LightWindAndRain",
      "ID": "1a0dc3b7-7609-4339-afc8-4042169fe954",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Light",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Light",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ModerateWindAndRain",
      "ID": "b9a76364-2021-4ee5-b6ff-06b4d4fa8d7f",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Moderate",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Moderate",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ModerateWindHeavyRainNoTL",
      "ID": "c0d29636-da6e-427d-bcbf-ffee8d2fa1cf",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "Zero",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Heavy",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "Far",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "MorningFog",
      "ID": "ad7db8ef-ec95-43cd-8109-dd9bcdb7cbd2",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Inactive",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Moderate",
          "CloudDensity": 1,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "DepthFog",
          "AirConditionStrengthType": "Heavy",
          "AirConditionsStrength": 0.8
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "OvercastDay",
      "ID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "OvercastHeavyWind_290Degrees",
      "ID": "f11afa53-b42e-4523-8dd6-b8e61b8a29c0",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Heavy",
          "WindDirectionType": "Custom",
          "WindDirection": 100,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "OvercastWindy",
      "ID": "164a99a9-2e80-4091-8837-810168a617c6",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "OvercastWindy_290Degrees",
      "ID": "1bbe12d1-0ff8-4bad-b4a2-d2a265307bdb",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Custom",
          "WindDirection": 100,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "SlumsDrizzle",
      "ID": "08079e5c-9c13-42c5-8061-4e028cdff6f2",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Inactive",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Light",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "SlumsRain",
      "ID": "8d767b8b-30d3-4522-8c84-af809402728e",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "true"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Inactive",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Moderate",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "VolumeFog",
          "AirConditionStrengthType": "Light",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "HeavySnowWindCloudsTL",
      "ID": "a30022f4-67ed-47d3-a925-d91a0866bbab",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Freezing",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Heavy",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Snow",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "ThunderAndLightning",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Moderate"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ModerateRainWindClouds",
      "ID": "152b8f0c-8e89-4d26-978e-5cb0115f03ec",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Random",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Moderate",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Moderate",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "HotLightWindHeavyDust",
      "ID": "426f40af-93b7-4484-8ab9-3a566d638c80",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Hot",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "Dust",
          "AirConditionStrengthType": "Heavy",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "WeatherConditionPokoKohara",
      "ID": "728a25fe-f20e-465f-9f34-a0d2021902ee",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Hot",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Heavy",
          "WindDirectionType": "Custom",
          "WindDirection": 110,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "Dust",
          "AirConditionStrengthType": "Heavy",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryFar",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "WeatherConditionPokoKoharaCalm",
      "ID": "edff21a2-3168-48c6-95ba-4bf3e44aefd0",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Hot",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Light",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "Dust",
          "AirConditionStrengthType": "Light",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "WeatherCondition_LightningNoRain",
      "ID": "7b188306-957b-4b13-847c-d174e6d26672",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Inactive",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "ThunderAndLightning",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Moderate"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "WeatherCondition_RagingStorms",
      "ID": "69a2f751-c50e-499e-afc2-52235d75cfe9",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Hot",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "Dust",
          "AirConditionStrengthType": "Light",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "ThunderAndLightning",
          "LightningDistance": "Far",
          "LightningStrengthType": "Light"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "true"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "WeatherCondition_SpookyFog",
      "ID": "af4a14b6-456f-4959-9835-52f5f495b7dd",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 1,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "DepthFog",
          "AirConditionStrengthType": "Custom",
          "AirConditionsStrength": 1
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "WeatherCondition_ThunderNoRain_02",
      "ID": "89ddb797-d70a-4292-9c3f-10af8e47d931",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Inactive",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "Thunder",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Moderate"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "Weather_HeavyFog",
      "ID": "e97e0859-c155-45b1-a99e-afa4747fea60",
      "Components": [
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Hot",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 90,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Heavy",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "DepthFog",
          "AirConditionStrengthType": "Custom",
          "AirConditionsStrength": 0.8
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "Thunder",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        },
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ShipClearSkies",
      "ID": "c43b6f92-032e-4e12-b4c5-542488440de6",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 315,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ShipDeadCalm",
      "ID": "334fa184-99d3-4bed-9ab9-53f311d633b7",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Custom",
          "WindDirectionType": "Custom",
          "WindDirection": 0,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "None",
          "PrecipitationStrengthType": "Inactive",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "VolumeFog",
          "AirConditionStrengthType": "Heavy",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ShipHeavyRain",
      "ID": "995f540c-876a-4c30-b701-3c8d2eb142e9",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Custom",
          "WindDirection": 315,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Heavy",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ShipHeavyRainWithLightning",
      "ID": "d876b3af-de38-4fd9-b129-8c5f6d4780b7",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Moderate",
          "WindDirectionType": "Custom",
          "WindDirection": 45,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Inactive",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Heavy",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "Thunder",
          "LightningDistance": "Far",
          "LightningStrengthType": "Moderate"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ShipLightRain",
      "ID": "deb654ac-36a0-4d8b-977d-3f6401ae73e7",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Neutral",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 315,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Light",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Light",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ShipModerateRain",
      "ID": "41021c41-bf5a-43bc-b07d-1d9b55b296e8",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 315,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Moderate",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Moderate",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "None",
          "LightningDistance": "VeryNear",
          "LightningStrengthType": "Inactive"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherConditionGameData, Assembly-CSharp",
      "DebugName": "ShipModerateRainWithLightning",
      "ID": "66f45fa8-4b86-4156-b487-a5f4c7ca63e6",
      "Components": [
        {
          "$type": "Game.GameData.GeneralWeatherConditionComponent, Assembly-CSharp",
          "IsTreacherous": "false"
        },
        {
          "$type": "Game.GameData.TemperatureComponent, Assembly-CSharp",
          "TemperatureType": "Cold",
          "Temperature": 0
        },
        {
          "$type": "Game.GameData.WindComponent, Assembly-CSharp",
          "WindStrengthType": "Light",
          "WindDirectionType": "Custom",
          "WindDirection": 45,
          "WindStrength": 0,
          "WindTurbulence": 0
        },
        {
          "$type": "Game.GameData.CloudinessComponent, Assembly-CSharp",
          "CloudStrength": "Moderate",
          "CloudDensity": 0,
          "CloudSpeedType": "MatchWindSpeed",
          "CloudSpeed": 0
        },
        {
          "$type": "Game.GameData.PrecipitationComponent, Assembly-CSharp",
          "PrecipitationType": "Rain",
          "PrecipitationStrengthType": "Moderate",
          "PrecipitationStrength": 0
        },
        {
          "$type": "Game.GameData.AirConditionsComponent, Assembly-CSharp",
          "AirConditionType": "None",
          "AirConditionStrengthType": "Inactive",
          "AirConditionsStrength": 0
        },
        {
          "$type": "Game.GameData.LightningComponent, Assembly-CSharp",
          "LightningType": "Thunder",
          "LightningDistance": "VeryFar",
          "LightningStrengthType": "Light"
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "01_Tikawara_Forecast",
      "ID": "6ba77aae-bc45-47dc-b5f5-cc708fdc2db6",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 100,
                "MaxDuration": 200
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 65,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 10,
                "MinDuration": 30,
                "MaxDuration": 60
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 75,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 100,
                "MaxDuration": 200
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 75,
                      "MinDuration": 65,
                      "MaxDuration": 100
                    },
                    "WeatherConditionID": "152b8f0c-8e89-4d26-978e-5cb0115f03ec"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 65,
                      "MaxDuration": 100
                    },
                    "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "02_Poko_Kohara_Forecast",
      "ID": "2f56acce-f8a0-44d8-9728-eebe81beb158",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 1000,
                "MaxDuration": 1000
              },
              "WeatherPattern": {
                "DebugName": "Poko Kohara Storm",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 1000,
                      "MaxDuration": 1000
                    },
                    "WeatherConditionID": "728a25fe-f20e-465f-9f34-a0d2021902ee"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 0,
                "MinDuration": 1000,
                "MaxDuration": 1000
              },
              "WeatherPattern": {
                "DebugName": "Poko Kohara Calm",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 1000,
                      "MaxDuration": 1000
                    },
                    "WeatherConditionID": "edff21a2-3168-48c6-95ba-4bf3e44aefd0"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "03_Vailian_Dist_Forecast",
      "ID": "5f1768d6-a305-4b77-96b4-a4db1cbbcb86",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 300,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 30,
                      "MaxDuration": 90
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 30,
                      "MaxDuration": 90
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 15,
                      "MinDuration": 30,
                      "MaxDuration": 90
                    },
                    "WeatherConditionID": "41aadcdc-bd84-4ce7-9112-23a28a6391c1"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 15,
                "MinDuration": 150,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "Overcast/Drizzle",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 75,
                      "MaxDuration": 150
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 75,
                      "MaxDuration": 150
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 15,
                "MinDuration": 200,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 100,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 75,
                      "MaxDuration": 150
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 75,
                      "MaxDuration": 150
                    },
                    "WeatherConditionID": "152b8f0c-8e89-4d26-978e-5cb0115f03ec"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "04_Palace_Dist_Forecast",
      "ID": "e19952b1-e313-4105-84fd-b236e2aedcf8",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 65,
                "MinDuration": 250,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 150,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 150,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 150,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "ff395f8e-0c67-402f-843f-09e1d083be12"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 250,
                "MaxDuration": 550
              },
              "WeatherPattern": {
                "DebugName": "Overcast/Windy",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 150,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 150,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "41aadcdc-bd84-4ce7-9112-23a28a6391c1"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 150,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 15,
                "MinDuration": 250,
                "MaxDuration": 500
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 15,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "152b8f0c-8e89-4d26-978e-5cb0115f03ec"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "05_Artisans_Dist_Forecast",
      "ID": "bd6a3d92-9bb3-4af1-8fd5-183122a252ac",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 150,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 34,
                      "MinDuration": 75,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 33,
                      "MinDuration": 75,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 33,
                      "MinDuration": 75,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "ff395f8e-0c67-402f-843f-09e1d083be12"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 200,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Overcast/Windy",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 150,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "41aadcdc-bd84-4ce7-9112-23a28a6391c1"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 150,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 10,
                "MinDuration": 250,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 150,
                      "MaxDuration": 250
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 30,
                      "MinDuration": 150,
                      "MaxDuration": 250
                    },
                    "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 10,
                      "MinDuration": 150,
                      "MaxDuration": 250
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "06_Slums_Dist_Forecast",
      "ID": "3993bc2b-4eae-4c2b-a9a6-03d49ef21bd0",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 40,
                "MinDuration": 150,
                "MaxDuration": 350
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 75,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 75,
                      "MaxDuration": 200
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 45,
                "MinDuration": 200,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Drizzle",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 65,
                      "MinDuration": 150,
                      "MaxDuration": 250
                    },
                    "WeatherConditionID": "08079e5c-9c13-42c5-8061-4e028cdff6f2"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 150,
                      "MaxDuration": 250
                    },
                    "WeatherConditionID": "1a0dc3b7-7609-4339-afc8-4042169fe954"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 15,
                "MinDuration": 250,
                "MaxDuration": 450
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "8d767b8b-30d3-4522-8c84-af809402728e"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "06_Slums_Dist_Forecast_Ruins",
      "ID": "e5dd81c3-a396-4df3-8365-33245a9068da",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 300,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "Dreary",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 140,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 240,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "07_Temple_Dist_Forecast",
      "ID": "2e492b9b-4a52-4e30-8ec6-d6efda0690f7",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": []
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "08_Rauatai_Dist_Forecast",
      "ID": "920a16d8-99b6-4efb-af25-64b3c098c2de",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": []
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "09_PortMaje_Forecast",
      "ID": "d3cc442a-10ae-4c8d-b42c-e205bb19708d",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 60,
                "MinDuration": 55,
                "MaxDuration": 110
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 30,
                "MaxDuration": 60
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 55,
                "MaxDuration": 110
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 40,
                      "MaxDuration": 60
                    },
                    "WeatherConditionID": "152b8f0c-8e89-4d26-978e-5cb0115f03ec"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 40,
                      "MaxDuration": 60
                    },
                    "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "09_PortMaje_Forecast_Huana",
      "ID": "0daa6123-7be4-4700-9cdc-55b315f43957",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 50,
                "MinDuration": 55,
                "MaxDuration": 110
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 15,
                "MinDuration": 30,
                "MaxDuration": 60
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 35,
                "MinDuration": 55,
                "MaxDuration": 110
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 40,
                      "MaxDuration": 60
                    },
                    "WeatherConditionID": "bcd6a96b-bce7-41d1-bdd1-a470f7b65459"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 40,
                      "MaxDuration": 60
                    },
                    "WeatherConditionID": "1a0dc3b7-7609-4339-afc8-4042169fe954"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "10_Magrans_Teeth_Forecast",
      "ID": "be333852-51f0-4a96-8da1-16a40cdd555f",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": []
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "11_Hasongo_Forecast",
      "ID": "d1372481-76e6-45cb-8038-f42bb798393e",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 40,
                "MinDuration": 150,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Sunny Day",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 300,
                      "MaxDuration": 400
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 60,
                "MinDuration": 150,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 30,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "bcd6a96b-bce7-41d1-bdd1-a470f7b65459"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "1a0dc3b7-7609-4339-afc8-4042169fe954"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "12_Ukaizo_Forecast_Ext",
      "ID": "b5bf6cbe-9ee1-41c2-8c98-1c564640bf87",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 300,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 300,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "f11afa53-b42e-4523-8dd6-b8e61b8a29c0"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "12_Ukaizo_Forecast_Eye",
      "ID": "a155b02c-41ca-419f-922f-450fbee6c8dc",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 300,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 300,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "12_Ukaizo_Forecast_Guardian",
      "ID": "9c2c585e-6ec4-4719-aa2a-c5dd49b9abcd",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 300,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "OvercastOvercast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 300,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "1bbe12d1-0ff8-4bad-b4a2-d2a265307bdb"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "12_Ukaizo_Forecast_Mach",
      "ID": "57630533-88d9-41a6-9a2c-d42dc0ef957c",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 300,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 300,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "f11afa53-b42e-4523-8dd6-b8e61b8a29c0"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "13_Fort_Deadlight_Forecast",
      "ID": "835b03c3-87f9-4888-9573-b07963898f65",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 55,
                "MinDuration": 55,
                "MaxDuration": 110
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 55,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 45,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 30,
                "MaxDuration": 60
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "ff395f8e-0c67-402f-843f-09e1d083be12"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "14_Crookspur_Forecast",
      "ID": "cb4e06e5-b370-4861-beba-d71644bb62ee",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 55,
                "MinDuration": 55,
                "MaxDuration": 110
              },
              "WeatherPattern": {
                "DebugName": "Standard",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 55,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 45,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "15_Sayuka_Forecast",
      "ID": "5158eeac-71cd-4233-aeae-dd879f7bf744",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 100,
                "MaxDuration": 300
              },
              "WeatherPattern": {
                "DebugName": "Sunny Day",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 300,
                      "MaxDuration": 400
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 300,
                      "MaxDuration": 400
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 150,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "bcd6a96b-bce7-41d1-bdd1-a470f7b65459"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "1a0dc3b7-7609-4339-afc8-4042169fe954"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "15_Takuro_Forecast",
      "ID": "c6fe4f4a-8223-46a7-ad8c-05ce8e3d5ab5",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": []
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "16_Ori_O_Koiki_Forecast",
      "ID": "cbb64b0f-a6a4-4f4f-bb97-d526209930b6",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 300,
                "MaxDuration": 500
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 33,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 33,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 34,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "ff395f8e-0c67-402f-843f-09e1d083be12"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 300,
                "MaxDuration": 500
              },
              "WeatherPattern": {
                "DebugName": "Overcast/Raining",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 300,
                      "MaxDuration": 400
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "17_Dunnage_Forecast",
      "ID": "4c61702c-05b5-4dbd-9ab4-99cb2d47753b",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 50,
                "MinDuration": 300,
                "MaxDuration": 450
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "ff395f8e-0c67-402f-843f-09e1d083be12"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 300,
                "MaxDuration": 450
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 34,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 33,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 33,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 300,
                "MaxDuration": 450
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "b9a76364-2021-4ee5-b6ff-06b4d4fa8d7f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 200,
                      "MaxDuration": 300
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "19_Observertory_Forecast",
      "ID": "7b33c279-76d9-46bf-a621-a3304995aeec",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 100,
                "MaxDuration": 200
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 70,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 30,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 100,
                "MaxDuration": 200
              },
              "WeatherPattern": {
                "DebugName": "Hot and Windy",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 35,
                      "MaxDuration": 55
                    },
                    "WeatherConditionID": "426f40af-93b7-4484-8ab9-3a566d638c80"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 35,
                      "MaxDuration": 55
                    },
                    "WeatherConditionID": "f31b99f2-45ef-4746-92bc-31002883a9de"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "20_Splintered_Reef_Forecast",
      "ID": "572e651c-78ef-4906-90d5-97f34d6f8982",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 300,
                "MaxDuration": 600
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 250,
                      "MaxDuration": 350
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 250,
                      "MaxDuration": 350
                    },
                    "WeatherConditionID": "164a99a9-2e80-4091-8837-810168a617c6"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 15,
                "MinDuration": 255,
                "MaxDuration": 380
              },
              "WeatherPattern": {
                "DebugName": "Storming",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 300,
                      "MaxDuration": 500
                    },
                    "WeatherConditionID": "152b8f0c-8e89-4d26-978e-5cb0115f03ec"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 300,
                      "MaxDuration": 500
                    },
                    "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "21_Prologue_Storm",
      "ID": "ecdf599c-94e0-49b4-9bc1-e5b63e0f26d6",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 100,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "Prologue Storm",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 100,
                      "MaxDuration": 100
                    },
                    "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "22_Huana_Ruins_Forecast",
      "ID": "086ae630-bd91-4642-a065-5493ce09a91c",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": []
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "23_Between_Forecast",
      "ID": "3be94f47-2739-42b6-bfc4-9c8c4ff08b4b",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 100,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "Display Name",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 100,
                      "MaxDuration": 100
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "27_Pyramid_Woe_Forecast",
      "ID": "4256050d-b257-43c9-97db-01c567cd8a4f",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 100,
                "MaxDuration": 200
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 70,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 30,
                      "MinDuration": 25,
                      "MaxDuration": 35
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 100,
                "MaxDuration": 200
              },
              "WeatherPattern": {
                "DebugName": "Hot and Windy",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 35,
                      "MaxDuration": 55
                    },
                    "WeatherConditionID": "426f40af-93b7-4484-8ab9-3a566d638c80"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 35,
                      "MaxDuration": 55
                    },
                    "WeatherConditionID": "f31b99f2-45ef-4746-92bc-31002883a9de"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "28_Drowned_Barrows_Forecast",
      "ID": "2d4f83d5-8ffc-42c5-b3d9-d4df0a74b601",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 75,
                "MinDuration": 300,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Sunny/Dusty",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 240,
                      "MaxDuration": 360
                    },
                    "WeatherConditionID": "f31b99f2-45ef-4746-92bc-31002883a9de"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 15,
                      "MinDuration": 240,
                      "MaxDuration": 360
                    },
                    "WeatherConditionID": "426f40af-93b7-4484-8ab9-3a566d638c80"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 35,
                      "MinDuration": 240,
                      "MaxDuration": 360
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 15,
                      "MinDuration": 240,
                      "MaxDuration": 360
                    },
                    "WeatherConditionID": "ff395f8e-0c67-402f-843f-09e1d083be12"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 25,
                "MinDuration": 300,
                "MaxDuration": 400
              },
              "WeatherPattern": {
                "DebugName": "Overcast/Rain",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 240,
                      "MaxDuration": 360
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 240,
                      "MaxDuration": 360
                    },
                    "WeatherConditionID": "1a0dc3b7-7609-4339-afc8-4042169fe954"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "DesertForecast",
      "ID": "71e83ec8-c129-4ba3-b439-0f9e746f406c",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 1000,
                "MaxDuration": 1000
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 1000,
                      "MaxDuration": 1000
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 1,
                "MinDuration": 1000,
                "MaxDuration": 1000
              },
              "WeatherPattern": {
                "DebugName": "ElewysFog",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 1000,
                      "MaxDuration": 1000
                    },
                    "WeatherConditionID": "af4a14b6-456f-4959-9835-52f5f495b7dd"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "E3_01_Moderate&HeavyStorms",
      "ID": "914cf9e6-8f64-4fe9-ae6f-927ef43d63cc",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 20,
                "MaxDuration": 30
              },
              "WeatherPattern": {
                "DebugName": "DynamicStrom",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "b9a76364-2021-4ee5-b6ff-06b4d4fa8d7f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "Forecast_HeavyFog",
      "ID": "3ec47418-5240-4f79-89b0-f8888b6ee2b5",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 20,
                "MaxDuration": 30
              },
              "WeatherPattern": {
                "DebugName": "DynamicStrom",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 40,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "b9a76364-2021-4ee5-b6ff-06b4d4fa8d7f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 60,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "LightWindOnly_Forecast",
      "ID": "b21d6d9e-5470-447d-852c-c548c1fe7f98",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 75,
                "MinDuration": 50,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "No Rain",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 80,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 25,
                "MinDuration": 50,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "Raining",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 80,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "3a41c635-1f22-41b5-94b9-4e2b09d0177f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "bcd6a96b-bce7-41d1-bdd1-a470f7b65459"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "SlumsWeatherForecast",
      "ID": "245a1705-9c83-4c74-84f1-8c45954d0912",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 70,
                "MinDuration": 15,
                "MaxDuration": 20
              },
              "WeatherPattern": {
                "DebugName": "ClearSkies",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 30,
                "MaxDuration": 40
              },
              "WeatherPattern": {
                "DebugName": "Rainy",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 75,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "08079e5c-9c13-42c5-8061-4e028cdff6f2"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 25,
                      "MinDuration": 20,
                      "MaxDuration": 20
                    },
                    "WeatherConditionID": "8d767b8b-30d3-4522-8c84-af809402728e"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "Sunny",
      "ID": "16afa8c6-d62e-437c-bf90-bcc8493aec68",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 1000,
                "MaxDuration": 1000
              },
              "WeatherPattern": {
                "DebugName": "Sunny",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 1000,
                      "MaxDuration": 1000
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "SunnyOrVariousRain",
      "ID": "133db375-27ba-4749-83c4-8282a01282fe",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 75,
                "MinDuration": 50,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "ClearSunnyDay",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 80,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 5,
                "MinDuration": 50,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "Overcast",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 80,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "c564874b-f4ff-4ba6-89e2-096ef0f6fa23"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 20,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "74b0568f-21b3-4727-90f6-36416812d2a0"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 10,
                "MinDuration": 50,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "LightRain",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "1a0dc3b7-7609-4339-afc8-4042169fe954"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 10,
                "MinDuration": 50,
                "MaxDuration": 100
              },
              "WeatherPattern": {
                "DebugName": "DynamicStorm",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 70,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "b9a76364-2021-4ee5-b6ff-06b4d4fa8d7f"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 30,
                      "MinDuration": 20,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "TestWeatherForecast",
      "ID": "373eca64-0a2c-4231-80c5-0fed764a238e",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 50,
                "MinDuration": 15,
                "MaxDuration": 20
              },
              "WeatherPattern": {
                "DebugName": "RainAndSnow",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 10,
                      "MaxDuration": 12
                    },
                    "WeatherConditionID": "a30022f4-67ed-47d3-a925-d91a0866bbab"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 50,
                      "MinDuration": 10,
                      "MaxDuration": 12
                    },
                    "WeatherConditionID": "152b8f0c-8e89-4d26-978e-5cb0115f03ec"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 50,
                "MinDuration": 15,
                "MaxDuration": 20
              },
              "WeatherPattern": {
                "DebugName": "HotHeavyDust",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 10,
                      "MaxDuration": 12
                    },
                    "WeatherConditionID": "426f40af-93b7-4484-8ab9-3a566d638c80"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "WeatherForecastPokoKohara",
      "ID": "54e2391e-1428-4ee4-8bf7-b1103046d313",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 100,
                "MinDuration": 1000,
                "MaxDuration": 1000
              },
              "WeatherPattern": {
                "DebugName": "Poko Kohara Storm",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 1000,
                      "MaxDuration": 1000
                    },
                    "WeatherConditionID": "728a25fe-f20e-465f-9f34-a0d2021902ee"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "ShipCombatForecast",
      "ID": "1f2a1a61-e367-4023-883b-6ad5f4a82000",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 50,
                "MinDuration": 15,
                "MaxDuration": 20
              },
              "WeatherPattern": {
                "DebugName": "ClearSunnyDay",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "c43b6f92-032e-4e12-b4c5-542488440de6"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 15,
                "MaxDuration": 20
              },
              "WeatherPattern": {
                "DebugName": "ShipDrizzle",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "deb654ac-36a0-4d8b-977d-3f6401ae73e7"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 30,
                "MaxDuration": 40
              },
              "WeatherPattern": {
                "DebugName": "ShipRain",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "41021c41-bf5a-43bc-b07d-1d9b55b296e8"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherForecastGameData, Assembly-CSharp",
      "DebugName": "ShipNonCombatForecast",
      "ID": "d05a7141-dbd6-4832-a210-c83eeaa42d99",
      "Components": [
        {
          "$type": "Game.GameData.WeatherForecastComponent, Assembly-CSharp",
          "WeatherPatterns": [
            {
              "RandomDuration": {
                "Probability": 50,
                "MinDuration": 15,
                "MaxDuration": 20
              },
              "WeatherPattern": {
                "DebugName": "ClearSunnyDay",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "c43b6f92-032e-4e12-b4c5-542488440de6"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 30,
                "MinDuration": 15,
                "MaxDuration": 20
              },
              "WeatherPattern": {
                "DebugName": "ShipDrizzle",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 100,
                      "MinDuration": 10,
                      "MaxDuration": 10
                    },
                    "WeatherConditionID": "deb654ac-36a0-4d8b-977d-3f6401ae73e7"
                  }
                ]
              }
            },
            {
              "RandomDuration": {
                "Probability": 20,
                "MinDuration": 40,
                "MaxDuration": 60
              },
              "WeatherPattern": {
                "DebugName": "ShipRain",
                "WeatherConditions": [
                  {
                    "RandomDuration": {
                      "Probability": 70,
                      "MinDuration": 15,
                      "MaxDuration": 30
                    },
                    "WeatherConditionID": "41021c41-bf5a-43bc-b07d-1d9b55b296e8"
                  },
                  {
                    "RandomDuration": {
                      "Probability": 30,
                      "MinDuration": 30,
                      "MaxDuration": 40
                    },
                    "WeatherConditionID": "995f540c-876a-4c30-b701-3c8d2eb142e9"
                  }
                ]
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "HeavyWindAndRainNoTL_WeatherPattern",
      "ID": "c021867e-56cb-483b-8e75-6bc5c20317d2",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "a060385d-d2cd-4ede-a527-3065871d5b14"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "HeavyWindAndRainTL_WeatherPattern",
      "ID": "d61a87e2-6bb1-479d-b5ad-5783e9a46774",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "MorningFog_WeatherPattern",
      "ID": "3c5f8ba9-a352-45e2-b7fc-4903d81fcf74",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Morning Fog",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 60,
                  "MaxDuration": 120
                },
                "WeatherConditionID": "ad7db8ef-ec95-43cd-8109-dd9bcdb7cbd2"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "PokoKoharaCalm_WeatherPattern",
      "ID": "94e8816b-9e30-464a-8da6-4818b52747f3",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Poko Kohara Calm",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 1000,
                  "MaxDuration": 1000
                },
                "WeatherConditionID": "edff21a2-3168-48c6-95ba-4bf3e44aefd0"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "ragingStorms_weatherPattern",
      "ID": "188154ed-52af-47e9-9714-0311f5159cac",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 100,
                  "MaxDuration": 200
                },
                "WeatherConditionID": "69a2f751-c50e-499e-afc2-52235d75cfe9"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "ShipStorms_WeatherPattern",
      "ID": "6e74d6c2-f18b-4b45-a94c-27fb8535f153",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 100,
                  "MaxDuration": 200
                },
                "WeatherConditionID": "995f540c-876a-4c30-b701-3c8d2eb142e9"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "SpookyFog_WeatherPattern",
      "ID": "4534b9e0-79ec-4c4e-8850-5359083177fc",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "af4a14b6-456f-4959-9835-52f5f495b7dd"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Sunny_WeatherPattern",
      "ID": "9ef37165-ef34-45a8-a18f-a1b878754a9c",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Sunny",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "02832d5f-0942-4971-b41f-e2983f4e8391"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "TestWeatherPattern",
      "ID": "a4ec2bcf-3119-4ca9-b647-ce50e4577bbc",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": []
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Test_DustStormsPK",
      "ID": "f03db50c-2879-45c5-a224-bf47c2750115",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "728a25fe-f20e-465f-9f34-a0d2021902ee"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Test_HeavyWindAndRain",
      "ID": "894736d8-889b-46a8-9705-849a7b4050db",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "a82c75ba-37a8-436f-809c-079836c4fb29"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Test_LightWindAndRain",
      "ID": "fb705134-83ce-4735-b5e3-20b7d4a75724",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "1a0dc3b7-7609-4339-afc8-4042169fe954"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Test_ModerateWindAndRain",
      "ID": "538725e1-629e-4e2d-861a-a2e82e940904",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "b9a76364-2021-4ee5-b6ff-06b4d4fa8d7f"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Test_ThunderLightningNoRain",
      "ID": "ca53ea39-9591-4bee-8d33-5e558ad81395",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "7b188306-957b-4b13-847c-d174e6d26672"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Test_ThunderNoRain",
      "ID": "9504f5e8-0b07-456f-b7ae-4c2def96e3c5",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "Display Name",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 300,
                  "MaxDuration": 300
                },
                "WeatherConditionID": "89ddb797-d70a-4292-9c3f-10af8e47d931"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.WeatherPatternGameData, Assembly-CSharp",
      "DebugName": "Ukaizo_Overcast_WeatherPattern",
      "ID": "ddcd6ef3-8618-4f8f-869f-bcb6d0a7040c",
      "Components": [
        {
          "$type": "Game.GameData.WeatherPatternComponent, Assembly-CSharp",
          "WeatherPattern": {
            "DebugName": "OvercastSunny",
            "WeatherConditions": [
              {
                "RandomDuration": {
                  "Probability": 100,
                  "MinDuration": 5,
                  "MaxDuration": 5
                },
                "WeatherConditionID": "f11afa53-b42e-4523-8dd6-b8e61b8a29c0"
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "01_Tikawara",
      "ID": "dcf2a099-c180-4ca5-b5a9-299e0a4a8253",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 0,
          "ZoneMapID": "4323a468-e574-456c-8690-617039022b8c",
          "MapsInZoneIDs": [
            "cbff745d-3053-4536-b591-c114b292d046",
            "9f7796d4-96e2-4d96-aeba-481aef6305f4",
            "54b32ea0-4304-45b9-9a48-841f3ca8fbdf",
            "2cd8e5d6-10af-4416-8541-c7c71e7356a2",
            "f954ed18-c44e-4eb1-abf4-b27547c96ad8"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "02_Poko_Kohara",
      "ID": "0f5ecd73-34f3-4277-8f29-565c11d8516f",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 6,
          "ZoneMapID": "d825196c-7d1c-4b52-883c-0a7e66f9f597",
          "MapsInZoneIDs": [
            "6367f1b5-5ce2-4599-9013-810cc1e5dfd8",
            "6c59ed92-0840-4a40-8157-c52fc59540f3",
            "8f8bca05-2d6d-4436-99d0-c0cbd1f73b7f",
            "367d3e31-790c-4178-976f-a7e8735d209e",
            "24403037-0ab3-493e-850b-8645cd5cd074"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "03_Vailian_District",
      "ID": "786932e5-024c-4c16-bf75-844b5d29e3d6",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 13,
          "ZoneMapID": "80da11de-5300-468e-b982-914656f5b269",
          "MapsInZoneIDs": [
            "6575cd6e-d79b-4f09-b024-e1f3039f7c7a",
            "54b2e44b-bdd3-432c-ba7a-f74bd3f99cde",
            "5df11672-0e60-4f89-a7a2-29d60473e000",
            "1fc78d71-ac14-438e-a887-541a6223536a",
            "fd3e85ee-bc59-4b7a-89ed-d28afe879eb6",
            "f3dd05a8-1a47-42f8-b0ad-bd3cd6ca296d",
            "fb1f202a-f14a-4557-bcdc-1314a86c2445",
            "bf1184f8-a705-4e5e-a355-8716941b6189",
            "861bba48-07f3-4d64-8cc5-e2cfcf3e62a6",
            "c0f55fd9-9c28-46be-8e55-64c05e29743d",
            "ea1a3c8f-6136-4f45-b046-675c158f7348",
            "20428053-f485-42b6-ba04-366c2b2f4fc6",
            "0eab6589-ef34-43a2-928d-14825da3823d"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "04_Palace_District",
      "ID": "accc2a1d-b721-4cd4-ac8e-e3a4da7302cd",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 35,
          "ZoneMapID": "39a113f6-d477-423b-bb9b-353ad2d4ed1b",
          "MapsInZoneIDs": [
            "dc8d1884-db6c-49ab-b73d-1b01e40698eb",
            "8dc0d210-64b3-4765-a6dc-2fa11a6cb487",
            "b68a5ac4-9d23-459e-9b37-6ab51f1d6838",
            "03e0a708-b219-4313-98fe-c8df98fa7c59",
            "ad1f229b-5b78-4336-a891-6e5903c114d9",
            "164006d2-9bba-4b55-aed6-6e9c28224172",
            "7a39fa26-9507-4a86-8294-0f69fa468072"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "05_Artisans_District",
      "ID": "9d13fd83-445e-4bd1-b296-9aaa95161892",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 11,
          "ZoneMapID": "2da1ff57-a874-49fd-b0b8-fee5e32120c4",
          "MapsInZoneIDs": [
            "9b38a40c-be0b-4f38-8685-9f15ab015ac6",
            "75065d3a-5d46-4f1a-9716-2c19b68bc8cb",
            "f07d08bd-20be-4624-8aa2-0be029e6ccf5",
            "df6f6d63-caf8-4c11-a194-e3447db4e9ab",
            "951c03ba-f431-4695-be25-542f9e983715",
            "003a82dd-66e5-409b-bbf6-751770b3f2cc",
            "024c9fbe-9d73-4ea8-a038-fe7ef29270b6",
            "180eb9a9-7fa4-4f08-85de-999b90212ef2",
            "21c6d0ac-f9b8-42a6-8e1c-a6122367fb72",
            "944961cb-2916-4896-bfb6-c35ccb368416",
            "2593a4bd-edc7-45c4-ab6b-35f5e5b74f87",
            "0d6bcee4-9993-4956-a577-c321bf19a51d",
            "a9070d2f-1e0c-4c9b-b97d-528b504613cb",
            "e7c0ad01-bc35-4c48-bf14-fe39ed7b6f7e",
            "747241a2-a9b4-43d8-9fe0-11495ecb577d"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "06_Slums_District",
      "ID": "e49bc725-bda7-4479-9bc5-8e7e83bd72ad",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 36,
          "ZoneMapID": "ff7933fc-6b3f-4efe-8a26-ef57704e240d",
          "MapsInZoneIDs": [
            "42211247-5346-44c5-8565-cc017bd40d9e",
            "2ff72bf4-b60f-43fa-a1c9-9351a2e69ef7",
            "9b1aeeda-b332-474d-b027-6ec90c6277a5",
            "e89d0ec2-89af-4e04-b921-4c7274b5876a",
            "7b51cbd9-3284-4aaf-a90b-2cf7523f74a4",
            "0aab3efd-6972-4682-8dc4-91378e91c669",
            "f7cc13a5-c9b0-4316-bf89-994dee36a9f6",
            "9884fa02-e8e5-4497-903b-fc7f407950d0",
            "a43a2deb-fd45-45db-8df5-3ad01dfddbfd",
            "5b3dd829-a21d-4158-a9af-5d2a1e564b34",
            "bd87900e-a73e-4e98-8814-7ba94f63f1a0",
            "61c1e971-f3eb-4be6-93e8-060ca001b158",
            "ea236faa-e278-45c2-9ac5-64bcc804c524",
            "66816780-2033-438b-b1a4-fc7f2eaa9561"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "07_Temple_District",
      "ID": "54d04105-42aa-4cfe-b556-512aa2e4238e",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 37,
          "ZoneMapID": "89a58ea3-6bf4-49e4-b5d6-3dd741f456f6",
          "MapsInZoneIDs": [
            "7cb374b5-42a9-4e2d-9851-5a67dbb0c408",
            "63d761da-2905-4dda-bc4b-4a27ccb02f83",
            "294d331b-3547-4d2e-86af-f8e40fcd084c",
            "e2600ca8-cd26-4cb3-a8e4-721be76b24e4",
            "5b4df54f-98c8-4ee5-a454-102e4a9df1e3",
            "7588900c-3c30-4095-aa3f-b8241c9117f7",
            "fd13cb9d-ec7d-492d-8ca8-8c1531da9413",
            "8b33271a-1cbe-46bc-a564-1fd9942181be",
            "06935cff-fea4-481f-8022-e53733273d76"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "08_Rauatai_District",
      "ID": "86584c1b-6933-415f-bf80-113fdd0a2bfd",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 38,
          "ZoneMapID": "877a299e-cb15-409f-93b2-d653a4a3717f",
          "MapsInZoneIDs": [
            "b0348d8d-74cd-4e72-bf12-93cfca6fac32",
            "e2e8ed35-563d-4a5a-afaf-4884e764de5f",
            "2a031e30-7154-4d70-a037-53e80632a941",
            "b6ce9a92-2c39-4e40-b887-43ea86b81e5a",
            "eee8ae78-2d2a-44bb-bc3a-1b41478fe8b4",
            "1dda2a0a-49fc-484b-857e-22412b3c4ca8",
            "8a364804-bca1-4e53-aa03-932dbaadaef8",
            "3ac54a68-f7be-4ef1-972d-e3c72cc667d9",
            "0d7c8c53-27a1-4a56-9b76-34f42eee4e40",
            "ef57ea06-172b-4aef-8526-39171ee7e90a"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "09_Port_Maje",
      "ID": "2262c84f-f2e7-471e-9e66-7efca602f3ae",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 63,
          "ZoneMapID": "8eab4220-b0a8-47e0-b3e2-260ccf8fae76",
          "MapsInZoneIDs": [
            "a2e9bce8-27f3-4d69-b9ac-cb6abde979c9",
            "64f6a4a2-fe56-48a4-b20c-defb9fc6e68d",
            "b2b470f8-85c2-4631-8116-c6695fd41580",
            "5b7ba519-e7df-44eb-82af-83592b885b60",
            "d83df9ba-aea5-4c0a-93ca-f6e9b204cf10",
            "6a31c820-e229-4539-97fe-8e6e2f10686d",
            "ae25894e-db29-4c98-b7f1-3475e1de4797",
            "6cbf23af-d77d-4027-b8fe-2ebd2a357afa",
            "cf77cc91-016b-4310-959b-53f18c0e4445",
            "680a2ca7-c3da-418d-b4b0-be2db6bd4b2b"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "10_Volcano_Mountain",
      "ID": "18ff4bce-849d-4608-ae5d-64e161e4a83a",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 64,
          "ZoneMapID": "37747f96-7154-43d7-942f-c45bcc7d230e",
          "MapsInZoneIDs": [
            "ba139aa2-d09a-40b5-a739-7bdfa449bcd6",
            "b7016c63-56d6-4d5c-9e57-1bf19210fcb7",
            "9f7ddaea-0a39-448e-bac4-e013c3dbfea3",
            "c355032c-654e-417a-8597-53bed5dc25c2",
            "454149df-66b2-4160-8baa-d9a430d32ac9",
            "40a162a1-0762-4f36-aeea-c3a959802bb0"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "11_Hasongo",
      "ID": "134e635f-03bc-44f2-8439-32ddaf35a9b9",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 100,
          "ZoneMapID": "be0599e1-a6b7-493e-b8ff-706e130e9b35",
          "MapsInZoneIDs": [
            "742661d5-70cb-4e83-bb7b-5d0ea9e9a9ee",
            "6ab99e17-dff4-4646-947e-60e2dc450c84",
            "2dd6355e-b7ad-4cc1-b87f-dd6a0a6d67da",
            "2ac95abf-b943-4c8a-b644-81834cc2de3c",
            "d7cfeaf4-2d74-4091-8d8b-86428ed10437",
            "f47b4748-16a1-4567-bae1-029cf2942ae1",
            "e870a95e-97a2-4eca-af34-4bbd75450af9",
            "1f37c45b-0ed5-43b4-966e-75d5e3578d4a"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "12_Ukaizo",
      "ID": "47ad755a-0f32-4206-83e3-d7dd8eabc389",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 65,
          "ZoneMapID": "a8e912ef-dbdb-4ce0-a94e-1d22cecb132f",
          "MapsInZoneIDs": [
            "818c61c2-6150-46ae-98c0-c011e97b8289",
            "e4bd7c78-401b-473b-8438-884f783ec1d8",
            "7ce6ad52-e95c-4a27-b8b5-0450384bcd67",
            "ba45df89-0393-45b6-80f8-e9b19bc882b0"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "13_Pirate_Fort",
      "ID": "2fcb43ae-08b2-4488-b585-3ac7579f850d",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 86,
          "ZoneMapID": "0a5637a9-7786-402d-a8c8-eea17eb80645",
          "MapsInZoneIDs": [
            "ebdaca1a-4437-4d7f-abc6-482984810a25",
            "c2b1c731-737f-49c2-849c-d2620cf5fac7",
            "08852b37-6a2a-4767-a8d4-8addc2047d6e",
            "c84fe64d-7c7a-4537-b8b9-2bbeab02a387",
            "6d02a14d-4bb5-4afe-9f3b-838ede8e806b"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "14_Slaver_Port",
      "ID": "bd675ee8-d56f-4317-97a4-d6b1f48f1643",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 66,
          "ZoneMapID": "e70e0265-a332-47cf-a0f8-ca3fcf0781eb",
          "MapsInZoneIDs": [
            "0d717f45-d300-4f1b-a08a-bacde7d722ef",
            "d86b3ff9-0f34-4b12-8750-2965befd3184",
            "0b270971-a0a1-482e-8194-7102685cc686",
            "6dfca515-9ec2-46bc-98e8-e798e4cf32b4"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "15_Rauatai_Port",
      "ID": "6b965e0f-6f9e-4c2c-b642-9fe5ec5bd251",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 67,
          "ZoneMapID": "46a16af0-3b0f-4394-ad51-e837f8ccf0ad",
          "MapsInZoneIDs": [
            "8192d11b-f495-4cd8-bde9-45baf9adeccf",
            "41f19f7b-d0c8-4359-bbc2-ba17111f92c5",
            "fa2d4aae-d2fb-4723-a31c-0083330cf483",
            "4f4d0d8b-f3f7-4adf-9192-27d3c0b2ab93",
            "7b17c268-b3b3-4e56-8d2e-c128885cdea2",
            "e8e169c2-e715-46d6-851d-bdd4ee398daa"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "16_Wahaki_Island",
      "ID": "fdc32a4f-967a-469a-92ae-f0dd9d1961ae",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 91,
          "ZoneMapID": "79a2cf03-b3e6-469c-a347-e13b1dd8782b",
          "MapsInZoneIDs": [
            "3c88678f-5fb3-4b06-80d4-0e1e30672bf3",
            "63ca706b-b9aa-41b0-8617-8a70e58f7367"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "17_Principi_Base",
      "ID": "7930edda-2ba8-477e-983c-343a355bb561",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 69,
          "ZoneMapID": "3701ea26-8686-4683-a4de-278170f85afb",
          "MapsInZoneIDs": [
            "b7e5a658-50ce-4b8f-9873-61257a3c0a45",
            "3a957c53-aea0-446a-b320-0bc685bc8eee",
            "fa771aa2-514d-4d1d-924b-a46a5711dc87",
            "6fcb9658-baa2-41c8-b456-c4776ff1b2cf",
            "0a966c26-51f2-4b9a-b762-212f83262742",
            "cf0af132-126e-410b-839b-10b80115b992",
            "1bbe04a8-0694-46a9-8550-101ebf220c45"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "18_Uncharted_Naga_Temple",
      "ID": "6685709b-445c-4896-82f7-0efca8890249",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 70,
          "ZoneMapID": "2a1e679e-6093-45a2-9d79-0f7f3f553fce",
          "MapsInZoneIDs": [
            "98de489e-8985-461e-8f20-897133540d08"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "19_Uncharted_Dungeon",
      "ID": "c6aa17c1-51ba-4672-a9c2-8147f0c7cefe",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 71,
          "ZoneMapID": "49c90ace-a86a-4b78-902b-d6b2445da9da",
          "MapsInZoneIDs": [
            "5f9021b9-acb3-4bca-aa9a-3f0c6d039787",
            "fed77285-2179-41e8-9d48-f6486ee24bd6"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "20_Ship_Graveyard",
      "ID": "d9b45486-cc54-40d0-bae0-01217c824c07",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 72,
          "ZoneMapID": "e070f1ce-4df5-48cc-a5f3-d8e11718f302",
          "MapsInZoneIDs": [
            "98b527a1-5e52-4add-af82-acce89381dd3",
            "0ef216ac-6168-4255-a44f-aa28bdc5fd68",
            "d09eaaed-f821-4f9c-9261-ee0f1ee284d4",
            "7dd34f5b-9b0e-4e82-a20d-9e50812f05e0",
            "dccc29bb-1c8a-43ef-84b7-c34c536b7931",
            "e693d386-19ca-4f8c-beec-512ee9be7332"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "21_Prologue",
      "ID": "85e73c9a-f819-45cd-b0bc-666196cadaa7",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 73,
          "ZoneMapID": "b3cec9a5-b1f4-4b76-9ba2-4446131914bb",
          "MapsInZoneIDs": [
            "d3484f20-80e1-46bf-a8ec-5d7b8ac71892",
            "9e11bd69-5203-4272-825e-31769733ae5d",
            "33a21ea5-1b74-4fbb-a2d3-524a09c488f3"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "22_Huana_Ruins",
      "ID": "84318812-cd08-4ed5-8377-8e90488fe09d",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 103,
          "ZoneMapID": "df3c3ecc-a048-4d4b-a1d4-82b7406aea27",
          "MapsInZoneIDs": [
            "067b8450-499f-45ad-aaf5-32e7c1242a08"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "23_VTC_Between",
      "ID": "6d6053ae-f8e7-4c7d-bdd4-94788c884c4a",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 102,
          "ZoneMapID": "20884bab-162b-4e42-a646-2c8b2da554c8",
          "MapsInZoneIDs": [
            "0159a190-8857-480f-bfd5-0f43b61d6424",
            "79a61bfc-18f7-47bb-a7bb-eb30402b7023",
            "59c59925-1f6d-45cc-a3fe-32f4f071a843"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "25_Iceburg_Dungeon",
      "ID": "acd2c997-f0ce-4258-82da-0aae8fa14408",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 187,
          "ZoneMapID": "2349e728-e30a-4214-aa71-ade4c58b2256",
          "MapsInZoneIDs": [
            "18a13ab7-b0fd-4165-aa4e-2c0d8cea84c9",
            "d2e02a21-b0f0-4d8b-92f1-3406990c4da5",
            "e6e1a955-4a0f-4b93-a276-6325a2ed0453",
            "cc350d6b-47f3-444a-b4a1-135481bbf01a",
            "6c220ea4-a8ab-471b-abc2-c0804b54d308",
            "47d48c9b-d556-471b-a4ae-75f2f5cffabb",
            "2bb956f9-acf6-4e30-8a89-ce190c6ce564"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "27_Pyramid_Of_Woedica",
      "ID": "a2eac69a-0aa5-4e77-bbbb-2c0c9c75cca2",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 188,
          "ZoneMapID": "5d265c2b-1760-4306-ada0-53766aa8c11d",
          "MapsInZoneIDs": [
            "64c1fe63-9dc7-4f68-a87f-9071daa86834",
            "664c7261-6446-48f0-a453-9190cc997be8",
            "168ae4b9-ab70-43cf-aab8-f28dc57cbc63",
            "bd7c6cce-a828-4a8b-997e-3c462258a44a"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "28_Drowned_Barrows",
      "ID": "cae820df-a928-4e79-b959-8a2f700597cb",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 199,
          "ZoneMapID": "9ed8b2fc-b226-4343-8aef-70e7c48b655c",
          "MapsInZoneIDs": [
            "ec56ba52-9ee1-4799-bc50-b8a4c9b9066b",
            "fc0bf404-a0c3-4586-9e91-19a599b646b1",
            "bcfcb0ce-93e7-4129-aad7-56ef40da5f3c",
            "9597352d-1490-46bc-9042-de4392deba80"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "29_Maje_Beach",
      "ID": "f0a7a377-6fd2-4e9d-b0e2-41731081ef88",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 104,
          "ZoneMapID": "90defb17-111c-4f44-9b03-cc501beaf163",
          "MapsInZoneIDs": [
            "c977673a-94d9-4669-88ba-3a44b67cf939",
            "7c713d7d-c19b-4f0a-bf77-4c26a1da4e70"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ZoneGameData, Assembly-CSharp",
      "DebugName": "30_Engwithan_Digsite",
      "ID": "79aff38f-35d7-4b74-b78c-0bc6fee4de51",
      "Components": [
        {
          "$type": "Game.GameData.ZoneComponent, Assembly-CSharp",
          "DisplayName": 162,
          "ZoneMapID": "10b02858-4c7a-4544-b3b1-e346ba0a5f4a",
          "MapsInZoneIDs": [
            "97010d45-2922-4557-8c44-9eb35fc25530",
            "e8e3942f-2478-48bf-9457-e967d873de83",
            "b9183368-d99b-4683-b200-7759e21737d9"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "02_StartingMoney_Minor",
      "ID": "108b5620-ba81-4a5e-9cdc-85f6044e5f0b",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 2,
          "DescriptionString": 1,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 3,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void GivePlayerMoney(Int32)",
                    "Parameters": [
                      "5000"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": -1536216995,
                    "ParameterHash": 726205404
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_5000_starting.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "03_StartingMoney_Major",
      "ID": "d3f20e6b-bdba-40c0-8c24-4826b695ee94",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 3,
          "DescriptionString": 1,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 15,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void GivePlayerMoney(Int32)",
                    "Parameters": [
                      "50000"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": -1536216995,
                    "ParameterHash": 913700812
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_50000_starting.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "04_StartingLevel",
      "ID": "9dc1af06-efe4-408e-92fc-2d85c694e018",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 15,
          "DescriptionString": 16,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 12,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void AddExperienceToLevel(Int32)",
                    "Parameters": [
                      "4"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": -140293163,
                    "ParameterHash": 317908565
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_start_at_4.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "05_AttributeBonus_Player",
      "ID": "932b2228-1c85-4ca4-8743-a459810cba14",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 7,
          "DescriptionString": 50,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 15,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void AddAbility(Guid, Guid)",
                    "Parameters": [
                      "b1a8e901-0000-0000-0000-000000000000",
                      "16081b3e-324b-4854-8149-4bdbb7e82044"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 613997600,
                    "ParameterHash": -1299160249
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_bonus_attributes.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "07_ExploredWorldMap",
      "ID": "02cad6ed-07a5-4f8f-97d1-ebf6ccf77ec5",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 47,
          "DescriptionString": 48,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 12,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void SetGlobalValue(String, Int32)",
                    "Parameters": [
                      "bb_explored_worldmap",
                      "1"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 1935477378,
                    "ParameterHash": -554798280
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_experienced_captain.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "08_SkillBonus",
      "ID": "3bafdf3e-a2d2-4317-95e5-0da075017510",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 9,
          "DescriptionString": 49,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 3,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void AddAbility(Guid, Guid)",
                    "Parameters": [
                      "b1a8e901-0000-0000-0000-000000000000",
                      "79913a7b-903c-4cb5-9765-99acc52f2949"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 613997600,
                    "ParameterHash": 491497092
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_bonus_skills.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "09_FineEquipment",
      "ID": "d873709d-e685-40c2-8c70-d7709505fce6",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 11,
          "DescriptionString": 12,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnGameStart",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 5,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void SetGlobalValue(String, Int32)",
                    "Parameters": [
                      "bb_fine_equipment",
                      "1"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 1935477378,
                    "ParameterHash": 1575045299
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_fine_equipment.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "10_Upgraded_Sails",
      "ID": "c78f7eba-97b7-478e-a693-1c250cb5d5a6",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 43,
          "DescriptionString": 44,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 8,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void GiveItems(Guid, Int32)",
                    "Parameters": [
                      "793574fd-4fbe-4fa0-bbc4-e9026e3f2c1e",
                      "1"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 1926025091,
                    "ParameterHash": -1959788097
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                },
                {
                  "Data": {
                    "FullName": "Void GiveItems(Guid, Int32)",
                    "Parameters": [
                      "5b572b32-d1b0-45fc-aec1-1beadbdc522c",
                      "1"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 1926025091,
                    "ParameterHash": -274853637
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_upgraded_ship.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "11_Experienced_Captain",
      "ID": "1d5c8730-5753-47dd-baf3-a7931094e821",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 45,
          "DescriptionString": 46,
          "Requirements": {
            "Operator": 0,
            "Components": []
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnPrologueComplete",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 4,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void GiveSailorTales(Int32)",
                    "Parameters": [
                      "15"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 1325187031,
                    "ParameterHash": -1010515347
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_worldmap_explored.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.NewGameBonusGroupGameData, Assembly-CSharp",
      "DebugName": "12_ItemVendor",
      "ID": "00efaaf2-c10d-43ba-a299-3cb9b65f3b67",
      "Components": [
        {
          "$type": "Game.GameData.NewGameBonusGroupComponent, Assembly-CSharp",
          "DisplayString": 22,
          "DescriptionString": 23,
          "Requirements": {
            "Operator": 0,
            "Components": [
              {
                "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                "Data": {
                  "FullName": "Boolean CompareGlobalAchievementStat(TrackedAchievementStat, Operator, Int32)",
                  "Parameters": [
                    "NumSoulboundWeaponsFullyUnlocked",
                    "GreaterThanOrEqualTo",
                    "1"
                  ],
                  "UnrealCall": "",
                  "FunctionHash": 338075157,
                  "ParameterHash": -100960452
                },
                "Not": false,
                "Operator": 0
              }
            ]
          },
          "DisabledAchievementsAndChallengesIDs": [],
          "WhenToRunScripts": "OnGameStart",
          "Bonuses": [
            {
              "DisplayString": -1,
              "Cost": 5,
              "ScriptsToRun": [
                {
                  "Data": {
                    "FullName": "Void SetGlobalValue(String, Int32)",
                    "Parameters": [
                      "bb_soulbound_vendor",
                      "1"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": 1935477378,
                    "ParameterHash": -1649316768
                  },
                  "Conditional": {
                    "Operator": 0,
                    "Components": []
                  }
                }
              ],
              "SpriteIcon": "gui/images/ingamehud/newgamebonuses/bb_unique_vendor.png"
            }
          ],
          "IsChallenge": "false"
        }
      ]
    },
    {
      "$type": "Game.GameData.AbilitySettingsGameData, Assembly-CSharp",
      "DebugName": "AbilitySettings",
      "ID": "ba532e70-90db-40ed-a15e-4786d73b23dd",
      "Components": [
        {
          "$type": "Game.GameData.AbilitySettingsComponent, Assembly-CSharp",
          "BasePhraseRecitation": 6,
          "BasePhraseLinger": 3,
          "ChantRadius": 4,
          "AbilityBaseAccuracyPerAbilityLevel": 2,
          "AbilityBasePenetrationPerAbilityLevel": 0.5,
          "EmpoweredWeaponAccuracyBonus": 20,
          "EmpoweredWeaponPenetrationBonus": 5,
          "NotReadyData": [
            {
              "Value": "None",
              "TooltipExplanation": -1
            },
            {
              "Value": "AlreadyActivated",
              "TooltipExplanation": 455
            },
            {
              "Value": "InRecovery",
              "TooltipExplanation": -1
            },
            {
              "Value": "AtMaxPer",
              "TooltipExplanation": 456
            },
            {
              "Value": "OnlyInCombat",
              "TooltipExplanation": 457
            },
            {
              "Value": "FailedPrerequisite",
              "TooltipExplanation": -1
            },
            {
              "Value": "NotWhileMoving",
              "TooltipExplanation": 458
            },
            {
              "Value": "GrimoireCooldown",
              "TooltipExplanation": 460
            },
            {
              "Value": "NotEnoughAccruedResource",
              "TooltipExplanation": 4448
            },
            {
              "Value": "NoGrimoire",
              "TooltipExplanation": 975
            },
            {
              "Value": "NotInitialized",
              "TooltipExplanation": -1
            },
            {
              "Value": "Dead",
              "TooltipExplanation": -1
            },
            {
              "Value": "OnlyOutsideCombat",
              "TooltipExplanation": 714
            },
            {
              "Value": "InModalRecovery",
              "TooltipExplanation": 2532
            },
            {
              "Value": "InStealth",
              "TooltipExplanation": 2527
            },
            {
              "Value": "Invisible",
              "TooltipExplanation": 2528
            },
            {
              "Value": "Disabled",
              "TooltipExplanation": 2530
            },
            {
              "Value": "NotEnoughPowerPool",
              "TooltipExplanation": -1
            },
            {
              "Value": "InvalidWeaponsEquipped",
              "TooltipExplanation": -1
            },
            {
              "Value": "InvalidLevel",
              "TooltipExplanation": -1
            }
          ],
          "ClassResourceConversionRates": [
            {
              "ClassType": "None",
              "Ratio": 1
            },
            {
              "ClassType": "Fighter",
              "Ratio": 1
            },
            {
              "ClassType": "Rogue",
              "Ratio": 1
            },
            {
              "ClassType": "Priest",
              "Ratio": 1
            },
            {
              "ClassType": "Wizard",
              "Ratio": 1
            },
            {
              "ClassType": "Barbarian",
              "Ratio": 1
            },
            {
              "ClassType": "Ranger",
              "Ratio": 1
            },
            {
              "ClassType": "Druid",
              "Ratio": 1
            },
            {
              "ClassType": "Paladin",
              "Ratio": 1
            },
            {
              "ClassType": "Monk",
              "Ratio": 1
            },
            {
              "ClassType": "Cipher",
              "Ratio": 10
            },
            {
              "ClassType": "Chanter",
              "Ratio": 1
            }
          ],
          "DefaultMultiProjectileScaling": {
            "BaseLevel": 0,
            "LevelIncrement": 1,
            "MaxLevel": 0,
            "DamageAdjustment": 1.05,
            "DurationAdjustment": 1,
            "BounceCountAdjustment": 0,
            "ProjectileCountAdjustment": 0.5,
            "AccuracyAdjustment": 1,
            "PenetrationAdjustment": 0.25
          },
          "DefaultBounceScaling": {
            "BaseLevel": 0,
            "LevelIncrement": 1,
            "MaxLevel": 0,
            "DamageAdjustment": 1.05,
            "DurationAdjustment": 1,
            "BounceCountAdjustment": 0.5,
            "ProjectileCountAdjustment": 0,
            "AccuracyAdjustment": 1,
            "PenetrationAdjustment": 0.25
          },
          "DefaultEffectScaling": {
            "BaseLevel": 0,
            "LevelIncrement": 1,
            "MaxLevel": 0,
            "DamageAdjustment": 1.1,
            "DurationAdjustment": 1.05,
            "BounceCountAdjustment": 0,
            "ProjectileCountAdjustment": 0,
            "AccuracyAdjustment": 1,
            "PenetrationAdjustment": 0.25
          },
          "DefaultWeaponAttackScaling": {
            "BaseLevel": 0,
            "LevelIncrement": 1,
            "MaxLevel": 0,
            "DamageAdjustment": 1.05,
            "DurationAdjustment": 1.05,
            "BounceCountAdjustment": 0,
            "ProjectileCountAdjustment": 0,
            "AccuracyAdjustment": 1,
            "PenetrationAdjustment": 0.25
          },
          "DefaultFallbackScaling": {
            "BaseLevel": 0,
            "LevelIncrement": 1,
            "MaxLevel": 0,
            "DamageAdjustment": 1.05,
            "DurationAdjustment": 1,
            "BounceCountAdjustment": 0,
            "ProjectileCountAdjustment": 0,
            "AccuracyAdjustment": 2,
            "PenetrationAdjustment": 0.25
          },
          "OpposedKeywords": [
            {
              "KeywordAID": "f0dbb330-1b06-4175-8ce4-efe790b99213",
              "KeywordBID": "97b18bd3-b3e4-4777-a8bc-f0808022dd79"
            },
            {
              "KeywordAID": "f0dbb330-1b06-4175-8ce4-efe790b99213",
              "KeywordBID": "f8a513bb-3e92-4d10-b59f-abbdf71d8af4"
            },
            {
              "KeywordAID": "f0dbb330-1b06-4175-8ce4-efe790b99213",
              "KeywordBID": "dd8434fd-a194-47e9-90d0-05af8834296d"
            },
            {
              "KeywordAID": "1a03089e-0ef6-413e-97d0-3808c77c5781",
              "KeywordBID": "bbc89bf9-be71-4793-88e4-ef1e0affecaf"
            },
            {
              "KeywordAID": "56895011-1aab-4413-827b-51a126c04555",
              "KeywordBID": "92842156-c40a-4bfe-ae84-54bc7aa969f9"
            },
            {
              "KeywordAID": "b1533df4-9d22-468a-9671-1187714ba91c",
              "KeywordBID": "40d41e77-446e-4e69-8761-df60936eed5e"
            },
            {
              "KeywordAID": "c3e2ddf8-7df5-4a4d-9b09-6410ff36e80c",
              "KeywordBID": "5dfc1732-748c-43a6-8604-decb8267329a"
            },
            {
              "KeywordAID": "3863b3d1-5d16-46f6-b971-aaea803cd530",
              "KeywordBID": "5dfc1732-748c-43a6-8604-decb8267329a"
            }
          ],
          "FizzleEffect": "prefabs/effects/abilities/fx_fizzle.prefab",
          "FizzleAttachPoint": "Chest"
        }
      ]
    },
    {
      "$type": "Game.GameData.CharacterCreationSettingsGameData, Assembly-CSharp",
      "DebugName": "CharacterCreationSettings",
      "ID": "b300c73a-667e-4905-8b8c-3951643f9aab",
      "Components": [
        {
          "$type": "Game.GameData.CharacterCreationSettingsComponent, Assembly-CSharp",
          "MulticlassAttributeSilverStarThreshold": 2,
          "MulticlassAttributeGoldStarThreshold": 4,
          "DefaultEquipmentSetID": "691dedde-29e5-419e-9034-fdbf8e6c82fe",
          "Loadouts": [
            {
              "EquipmentID": "607ac362-3774-450c-b18c-a32b3eabb459",
              "ClassesIDs": [],
              "CultureID": "00000000-0000-0000-0000-000000000000"
            }
          ],
          "MaleAppearances": [
            {
              "Subrace": "None",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Meadow_Human",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair12.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_hum_head01/a_m_hum_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.3490196,
                "G": 0.2235294,
                "B": 0.1333333
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.6901961,
                "G": 0.5215687,
                "B": 0.3019608
              },
              "SkinColor": {
                "A": 1,
                "R": 0.8039216,
                "G": 0.6941177,
                "B": 0.5450981
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1686275,
                "G": 0.254902,
                "B": 0.3019608
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3333333,
                "G": 0.2470588,
                "B": 0.1686275
              }
            },
            {
              "Subrace": "Ocean_Human",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair16.asset",
              "FacialHairVisualDataPath": "prefabs/facialhair/a_facialhair07.asset",
              "HeadVisualDataPath": "prefabs/heads/m_hum_head01/a_m_hum_head02.asset",
              "HairColor": {
                "A": 1,
                "R": 0.07450981,
                "G": 0.07058824,
                "B": 0.06666667
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.1686275,
                "G": 0.1372549,
                "B": 0.1254902
              },
              "SkinColor": {
                "A": 1,
                "R": 0.4156863,
                "G": 0.1882353,
                "B": 0.1019608
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1921569,
                "G": 0.282353,
                "B": 0.4509804
              },
              "MinorColor": {
                "A": 1,
                "R": 0.2627451,
                "G": 0.1372549,
                "B": 0.09411765
              }
            },
            {
              "Subrace": "Savannah_Human",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair15.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_hum_head01/a_m_hum_head10.asset",
              "HairColor": {
                "A": 1,
                "R": 0.07843138,
                "G": 0.06666667,
                "B": 0.0627451
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.1764706,
                "G": 0.1490196,
                "B": 0.1333333
              },
              "SkinColor": {
                "A": 1,
                "R": 0.5803922,
                "G": 0.4156863,
                "B": 0.2705882
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.8941177,
                "G": 0.6862745,
                "B": 0.2039216
              },
              "MinorColor": {
                "A": 1,
                "R": 0.372549,
                "G": 0.2196078,
                "B": 0.1803922
              }
            },
            {
              "Subrace": "Wood_Elf",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair20.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_elf_head01/a_m_elf_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.145098,
                "G": 0.0627451,
                "B": 0.02352941
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3490196,
                "G": 0.2235294,
                "B": 0.1333333
              },
              "SkinColor": {
                "A": 1,
                "R": 0.772549,
                "G": 0.6,
                "B": 0.4862745
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.03529412,
                "G": 0.3254902,
                "B": 0.1803922
              },
              "MinorColor": {
                "A": 1,
                "R": 0.04313726,
                "G": 0.4,
                "B": 0.3529412
              }
            },
            {
              "Subrace": "Snow_Elf",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair24.asset",
              "FacialHairVisualDataPath": "prefabs/facialhair/a_facialhair02.asset",
              "HeadVisualDataPath": "prefabs/heads/m_elf_head01/a_m_plf_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.4901961,
                "G": 0.572549,
                "B": 0.6156863
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.4901961,
                "G": 0.572549,
                "B": 0.6156863
              },
              "SkinColor": {
                "A": 1,
                "R": 0.7254902,
                "G": 0.7686275,
                "B": 0.8235294
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.04705882,
                "G": 0.07843138,
                "B": 0.07843138
              },
              "MinorColor": {
                "A": 1,
                "R": 0.0627451,
                "G": 0.08627451,
                "B": 0.09411765
              }
            },
            {
              "Subrace": "Mountain_Dwarf",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair05.asset",
              "FacialHairVisualDataPath": "prefabs/facialhair/a_facialhair13.asset",
              "HeadVisualDataPath": "prefabs/heads/m_dwa_head01/a_m_dwa_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.3411765,
                "G": 0.1333333,
                "B": 0.05098039
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.5529412,
                "G": 0.2941177,
                "B": 0.1647059
              },
              "SkinColor": {
                "A": 1,
                "R": 0.7137255,
                "G": 0.5176471,
                "B": 0.3372549
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.3529412,
                "G": 0.1607843,
                "B": 0.07058824
              },
              "MinorColor": {
                "A": 1,
                "R": 0.2039216,
                "G": 0.3333333,
                "B": 0.4039216
              }
            },
            {
              "Subrace": "Boreal_Dwarf",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair04.asset",
              "FacialHairVisualDataPath": "prefabs/facialhair/a_facialhair01.asset",
              "HeadVisualDataPath": "prefabs/heads/m_dwa_head01/a_m_dwa_head02.asset",
              "HairColor": {
                "A": 1,
                "R": 0.1176471,
                "G": 0.1058824,
                "B": 0.09803922
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3607843,
                "G": 0.172549,
                "B": 0.1137255
              },
              "SkinColor": {
                "A": 1,
                "R": 0.4941176,
                "G": 0.3411765,
                "B": 0.2039216
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.4862745,
                "G": 0.3843137,
                "B": 0.3372549
              },
              "MinorColor": {
                "A": 1,
                "R": 0.2352941,
                "G": 0.172549,
                "B": 0.1333333
              }
            },
            {
              "Subrace": "Death_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_godd_head01/a_m_godd_head02.asset",
              "HairColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "Hair2Color": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.2156863,
                "G": 0.2862745,
                "B": 0.3176471
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3568628,
                "G": 0.007843138,
                "B": 0.007843138
              }
            },
            {
              "Subrace": "Fire_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_godf_head01/a_m_godf_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "Hair2Color": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.3921569,
                "G": 0.2666667,
                "B": 0.1411765
              },
              "MinorColor": {
                "A": 1,
                "R": 0.509804,
                "G": 0,
                "B": 0
              }
            },
            {
              "Subrace": "Nature_Godlike",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair24.asset",
              "FacialHairVisualDataPath": "prefabs/facialhair/a_facialhair02.asset",
              "HeadVisualDataPath": "prefabs/heads/m_godn_head01/a_m_godn_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.282353,
                "G": 0.4,
                "B": 0.2078431
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.6156863,
                "G": 0.7686275,
                "B": 0.4823529
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.2941177,
                "G": 0.4470588,
                "B": 0.1294118
              },
              "MinorColor": {
                "A": 1,
                "R": 0.07843138,
                "G": 0.4941176,
                "B": 0.2862745
              }
            },
            {
              "Subrace": "Moon_Godlike",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair22.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_godm_head01/a_m_godm_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.2196078,
                "G": 0.6078432,
                "B": 0.7960784
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.8,
                "G": 0.8627451,
                "B": 0.8901961
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.2666667,
                "G": 0.5843138,
                "B": 0.6078432
              },
              "MinorColor": {
                "A": 1,
                "R": 0.4352941,
                "G": 0.6117647,
                "B": 0.7019608
              }
            },
            {
              "Subrace": "Hearth_Orlan",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair11.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_orl_head01/a_m_orl_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.1843137,
                "G": 0.1137255,
                "B": 0.06666667
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.227451,
                "G": 0.1803922,
                "B": 0.1058824
              },
              "SkinColor": {
                "A": 1,
                "R": 0.3529412,
                "G": 0.2470588,
                "B": 0.1803922
              },
              "TattooColor": {
                "A": 1,
                "R": 0.7137255,
                "G": 0.572549,
                "B": 0.4862745
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1843137,
                "G": 0.3607843,
                "B": 0.509804
              },
              "MinorColor": {
                "A": 1,
                "R": 0.4901961,
                "G": 0.2941177,
                "B": 0.1490196
              }
            },
            {
              "Subrace": "Wild_Orlan",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_orl_head01/a_m_wor_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.6627451,
                "G": 0.5254902,
                "B": 0.3098039
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3098039,
                "G": 0.1803922,
                "B": 0.1019608
              },
              "SkinColor": {
                "A": 1,
                "R": 0.2392157,
                "G": 0.1411765,
                "B": 0.07843138
              },
              "TattooColor": {
                "A": 1,
                "R": 0.6627451,
                "G": 0.5254902,
                "B": 0.3098039
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1019608,
                "G": 0.254902,
                "B": 0.4078431
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3882353,
                "G": 0.3098039,
                "B": 0.227451
              }
            },
            {
              "Subrace": "Coastal_Aumaua",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair04.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_aum_head01/a_m_cam_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.0627451,
                "G": 0.05098039,
                "B": 0.04705882
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.145098,
                "G": 0.2078431,
                "B": 0.2392157
              },
              "SkinColor": {
                "A": 1,
                "R": 0.282353,
                "G": 0.4745098,
                "B": 0.5490196
              },
              "TattooColor": {
                "A": 1,
                "R": 0.3098039,
                "G": 0.7098039,
                "B": 0.7921569
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1098039,
                "G": 0.1254902,
                "B": 0.1411765
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3215686,
                "G": 0.2862745,
                "B": 0.254902
              }
            },
            {
              "Subrace": "Island_Aumaua",
              "HairVisualDataPath": "prefabs/hair/male/a_m_hair02.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/m_aum_head01/a_m_iam_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.2784314,
                "G": 0.1372549,
                "B": 0.1058824
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.2784314,
                "G": 0.1372549,
                "B": 0.1058824
              },
              "SkinColor": {
                "A": 1,
                "R": 0.5921569,
                "G": 0.3411765,
                "B": 0.2078431
              },
              "TattooColor": {
                "A": 1,
                "R": 0.2666667,
                "G": 0.1294118,
                "B": 0.08235294
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1254902,
                "G": 0.09411765,
                "B": 0.08235294
              },
              "MinorColor": {
                "A": 1,
                "R": 0.1254902,
                "G": 0.1372549,
                "B": 0.1843137
              }
            },
            {
              "Subrace": "Avian_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Advanced_Construct",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Marine_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Meadow_Savannah_Human",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Storm_Human",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Drunk_Orlan",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            }
          ],
          "FemaleAppearances": [
            {
              "Subrace": "None",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Meadow_Human",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair16.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_hum_head01/a_f_hum_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.6901961,
                "G": 0.5215687,
                "B": 0.3019608
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.8509804,
                "G": 0.6745098,
                "B": 0.4
              },
              "SkinColor": {
                "A": 1,
                "R": 0.8039216,
                "G": 0.6941177,
                "B": 0.5450981
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1607843,
                "G": 0.2509804,
                "B": 0.2941177
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3333333,
                "G": 0.2470588,
                "B": 0.1686275
              }
            },
            {
              "Subrace": "Ocean_Human",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair22.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_hum_head01/a_f_hum_head02.asset",
              "HairColor": {
                "A": 1,
                "R": 0.145098,
                "G": 0.0627451,
                "B": 0.02352941
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.1843137,
                "G": 0.05882353,
                "B": 0.01176471
              },
              "SkinColor": {
                "A": 1,
                "R": 0.4666667,
                "G": 0.227451,
                "B": 0.1254902
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.2392157,
                "G": 0.3490196,
                "B": 0.5607843
              },
              "MinorColor": {
                "A": 1,
                "R": 0.2627451,
                "G": 0.1372549,
                "B": 0.09411765
              }
            },
            {
              "Subrace": "Savannah_Human",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair15.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_hum_head01/a_f_hum_head07.asset",
              "HairColor": {
                "A": 1,
                "R": 0.08627451,
                "G": 0.0627451,
                "B": 0.05490196
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.2470588,
                "G": 0.172549,
                "B": 0.1372549
              },
              "SkinColor": {
                "A": 1,
                "R": 0.6509804,
                "G": 0.4705882,
                "B": 0.3098039
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.8941177,
                "G": 0.6862745,
                "B": 0.2039216
              },
              "MinorColor": {
                "A": 1,
                "R": 0.372549,
                "G": 0.2196078,
                "B": 0.1803922
              }
            },
            {
              "Subrace": "Wood_Elf",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair01.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_elf_head01/a_f_wlf_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.2470588,
                "G": 0.145098,
                "B": 0.1058824
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.4,
                "G": 0.2705882,
                "B": 0.1490196
              },
              "SkinColor": {
                "A": 1,
                "R": 0.7882353,
                "G": 0.6431373,
                "B": 0.4941176
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.03529412,
                "G": 0.3254902,
                "B": 0.1803922
              },
              "MinorColor": {
                "A": 1,
                "R": 0.01960784,
                "G": 0.345098,
                "B": 0.4039216
              }
            },
            {
              "Subrace": "Snow_Elf",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair17.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_elf_head01/a_f_plf_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.4901961,
                "G": 0.572549,
                "B": 0.6156863
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.4901961,
                "G": 0.572549,
                "B": 0.6156863
              },
              "SkinColor": {
                "A": 1,
                "R": 0.7254902,
                "G": 0.7686275,
                "B": 0.8235294
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.7568628,
                "G": 0.8235294,
                "B": 0.8666667
              },
              "MinorColor": {
                "A": 1,
                "R": 0.1411765,
                "G": 0.2352941,
                "B": 0.282353
              }
            },
            {
              "Subrace": "Mountain_Dwarf",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair05.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_dwa_head01/a_f_dwa_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.2862745,
                "G": 0.04705882,
                "B": 0.01960784
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.4352941,
                "G": 0.1490196,
                "B": 0.1019608
              },
              "SkinColor": {
                "A": 1,
                "R": 0.7450981,
                "G": 0.6156863,
                "B": 0.4705882
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.4470588,
                "G": 0.3019608,
                "B": 0.2039216
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3882353,
                "G": 0.3058824,
                "B": 0.227451
              }
            },
            {
              "Subrace": "Boreal_Dwarf",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair16.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_dwa_head01/a_f_dwa_head02.asset",
              "HairColor": {
                "A": 1,
                "R": 0.1372549,
                "G": 0.1137255,
                "B": 0.07058824
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.1372549,
                "G": 0.1137255,
                "B": 0.07058824
              },
              "SkinColor": {
                "A": 1,
                "R": 0.5294118,
                "G": 0.4078431,
                "B": 0.2705882
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.8352941,
                "G": 0.682353,
                "B": 0.6078432
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3411765,
                "G": 0.227451,
                "B": 0.1568628
              }
            },
            {
              "Subrace": "Death_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_godd_head01/a_f_godd_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "Hair2Color": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.1333333,
                "G": 0.2470588,
                "B": 0.282353
              },
              "MinorColor": {
                "A": 1,
                "R": 0.2431373,
                "G": 0.2,
                "B": 0.1411765
              }
            },
            {
              "Subrace": "Fire_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_godf_head01/a_f_godf_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "Hair2Color": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.4470588,
                "G": 0.2509804,
                "B": 0.05490196
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3019608,
                "G": 0.227451,
                "B": 0.1764706
              }
            },
            {
              "Subrace": "Nature_Godlike",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair19.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_godn_head01/a_f_godn_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.2431373,
                "G": 0.4470588,
                "B": 0.145098
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.6470588,
                "G": 0.7411765,
                "B": 0.4431373
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.572549,
                "G": 0.6901961,
                "B": 0.7372549
              },
              "MinorColor": {
                "A": 1,
                "R": 0.1098039,
                "G": 0.4078431,
                "B": 0.2666667
              }
            },
            {
              "Subrace": "Moon_Godlike",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair09.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_godm_head01/a_f_godm_head02.asset",
              "HairColor": {
                "A": 1,
                "R": 0.2509804,
                "G": 0.5568628,
                "B": 0.7843137
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.6470588,
                "G": 0.7254902,
                "B": 0.8196079
              },
              "SkinColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "TattooColor": {
                "A": 1,
                "R": 0,
                "G": 0,
                "B": 0
              },
              "MajorColor": {
                "A": 1,
                "R": 0.05098039,
                "G": 0.282353,
                "B": 0.4078431
              },
              "MinorColor": {
                "A": 1,
                "R": 0.04313726,
                "G": 0.1686275,
                "B": 0.2156863
              }
            },
            {
              "Subrace": "Hearth_Orlan",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair18.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_orl_head01/a_f_orl_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.08627451,
                "G": 0.1215686,
                "B": 0.145098
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.1490196,
                "G": 0.2078431,
                "B": 0.2509804
              },
              "SkinColor": {
                "A": 1,
                "R": 0.1529412,
                "G": 0.2156863,
                "B": 0.2470588
              },
              "TattooColor": {
                "A": 1,
                "R": 0.7254902,
                "G": 0.5372549,
                "B": 0.4509804
              },
              "MajorColor": {
                "A": 1,
                "R": 0.5686275,
                "G": 0.3607843,
                "B": 0.1568628
              },
              "MinorColor": {
                "A": 1,
                "R": 0.2,
                "G": 0.1568628,
                "B": 0.1098039
              }
            },
            {
              "Subrace": "Wild_Orlan",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_orl_head01/a_f_wor_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.1803922,
                "G": 0.1294118,
                "B": 0.1137255
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.3058824,
                "B": 0.2117647
              },
              "SkinColor": {
                "A": 1,
                "R": 0.2862745,
                "G": 0.1529412,
                "B": 0.1058824
              },
              "TattooColor": {
                "A": 1,
                "R": 0.6470588,
                "G": 0.4901961,
                "B": 0.3372549
              },
              "MajorColor": {
                "A": 1,
                "R": 0.07450981,
                "G": 0.1372549,
                "B": 0.2
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3294118,
                "G": 0.2588235,
                "B": 0.1607843
              }
            },
            {
              "Subrace": "Coastal_Aumaua",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair02.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_aum_head01/a_f_cam_head02.asset",
              "HairColor": {
                "A": 1,
                "R": 0.0627451,
                "G": 0.05098039,
                "B": 0.04705882
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.145098,
                "G": 0.2078431,
                "B": 0.2392157
              },
              "SkinColor": {
                "A": 1,
                "R": 0.282353,
                "G": 0.4745098,
                "B": 0.5490196
              },
              "TattooColor": {
                "A": 1,
                "R": 0.3098039,
                "G": 0.7098039,
                "B": 0.7921569
              },
              "MajorColor": {
                "A": 1,
                "R": 0.09411765,
                "G": 0.1764706,
                "B": 0.2196078
              },
              "MinorColor": {
                "A": 1,
                "R": 0.4941176,
                "G": 0.4117647,
                "B": 0.2392157
              }
            },
            {
              "Subrace": "Island_Aumaua",
              "HairVisualDataPath": "prefabs/hair/female/a_f_hair02.asset",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "prefabs/heads/f_aum_head01/a_f_iam_head01.asset",
              "HairColor": {
                "A": 1,
                "R": 0.1058824,
                "G": 0.04313726,
                "B": 0.01960784
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.372549,
                "G": 0.2705882,
                "B": 0.1372549
              },
              "SkinColor": {
                "A": 1,
                "R": 0.5921569,
                "G": 0.3411765,
                "B": 0.2078431
              },
              "TattooColor": {
                "A": 1,
                "R": 0.2666667,
                "G": 0.1294118,
                "B": 0.08235294
              },
              "MajorColor": {
                "A": 1,
                "R": 0.08627451,
                "G": 0.3254902,
                "B": 0.4117647
              },
              "MinorColor": {
                "A": 1,
                "R": 0.3176471,
                "G": 0.08235294,
                "B": 0.01568628
              }
            },
            {
              "Subrace": "Avian_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Advanced_Construct",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Marine_Godlike",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Meadow_Savannah_Human",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Storm_Human",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            },
            {
              "Subrace": "Drunk_Orlan",
              "HairVisualDataPath": "",
              "FacialHairVisualDataPath": "",
              "HeadVisualDataPath": "",
              "HairColor": {
                "A": 1,
                "R": 0.5411765,
                "G": 0.254902,
                "B": 0.04313726
              },
              "Hair2Color": {
                "A": 1,
                "R": 0.3960784,
                "G": 0.05882353,
                "B": 0.05882353
              },
              "SkinColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "TattooColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MajorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              },
              "MinorColor": {
                "A": 1,
                "R": 1,
                "G": 1,
                "B": 1
              }
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.CharacterProgressionGameData, Assembly-CSharp",
      "DebugName": "CharacterProgressionSettings",
      "ID": "cbefca9f-5fab-44f3-9497-74c3d4114150",
      "Components": [
        {
          "$type": "Game.GameData.CharacterProgressionDataComponent, Assembly-CSharp",
          "MaxClasses": 2,
          "SuperChargeCountByCharacterLevel": [
            "0",
            "1",
            "0",
            "0",
            "0",
            "1",
            "0",
            "0",
            "0",
            "1",
            "0",
            "0",
            "0",
            "1",
            "0",
            "0",
            "0",
            "1",
            "0",
            "0"
          ],
          "WeaponProficiencyPointsByCharacterLevel": [
            "2",
            "0",
            "0",
            "1",
            "0",
            "0",
            "0",
            "1",
            "0",
            "0",
            "0",
            "1",
            "0",
            "0",
            "0",
            "1",
            "0",
            "0",
            "0",
            "1"
          ],
          "BaseMaxPowerPoolByPowerLevel": [
            "0",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11"
          ],
          "HybridClassTitles": [
            {
              "Class1": "Fighter",
              "Class2": "Barbarian",
              "Title": 2592
            },
            {
              "Class1": "Fighter",
              "Class2": "Chanter",
              "Title": 2593
            },
            {
              "Class1": "Fighter",
              "Class2": "Cipher",
              "Title": 2594
            },
            {
              "Class1": "Fighter",
              "Class2": "Druid",
              "Title": 2595
            },
            {
              "Class1": "Fighter",
              "Class2": "Paladin",
              "Title": 2611
            },
            {
              "Class1": "Fighter",
              "Class2": "Priest",
              "Title": 2610
            },
            {
              "Class1": "Fighter",
              "Class2": "Monk",
              "Title": 2609
            },
            {
              "Class1": "Fighter",
              "Class2": "Ranger",
              "Title": 2608
            },
            {
              "Class1": "Fighter",
              "Class2": "Rogue",
              "Title": 2607
            },
            {
              "Class1": "Fighter",
              "Class2": "Wizard",
              "Title": 2606
            },
            {
              "Class1": "Wizard",
              "Class2": "Barbarian",
              "Title": 2605
            },
            {
              "Class1": "Wizard",
              "Class2": "Chanter",
              "Title": 2604
            },
            {
              "Class1": "Wizard",
              "Class2": "Cipher",
              "Title": 2603
            },
            {
              "Class1": "Wizard",
              "Class2": "Druid",
              "Title": 2602
            },
            {
              "Class1": "Wizard",
              "Class2": "Paladin",
              "Title": 2601
            },
            {
              "Class1": "Wizard",
              "Class2": "Priest",
              "Title": 2600
            },
            {
              "Class1": "Wizard",
              "Class2": "Monk",
              "Title": 2599
            },
            {
              "Class1": "Wizard",
              "Class2": "Ranger",
              "Title": 2598
            },
            {
              "Class1": "Wizard",
              "Class2": "Rogue",
              "Title": 2597
            },
            {
              "Class1": "Rogue",
              "Class2": "Barbarian",
              "Title": 2612
            },
            {
              "Class1": "Rogue",
              "Class2": "Chanter",
              "Title": 2613
            },
            {
              "Class1": "Rogue",
              "Class2": "Cipher",
              "Title": 2614
            },
            {
              "Class1": "Rogue",
              "Class2": "Druid",
              "Title": 2615
            },
            {
              "Class1": "Rogue",
              "Class2": "Monk",
              "Title": 2616
            },
            {
              "Class1": "Rogue",
              "Class2": "Paladin",
              "Title": 2617
            },
            {
              "Class1": "Rogue",
              "Class2": "Priest",
              "Title": 2618
            },
            {
              "Class1": "Rogue",
              "Class2": "Ranger",
              "Title": 2619
            },
            {
              "Class1": "Paladin",
              "Class2": "Barbarian",
              "Title": 2620
            },
            {
              "Class1": "Paladin",
              "Class2": "Chanter",
              "Title": 2621
            },
            {
              "Class1": "Paladin",
              "Class2": "Cipher",
              "Title": 2622
            },
            {
              "Class1": "Paladin",
              "Class2": "Druid",
              "Title": 2623
            },
            {
              "Class1": "Paladin",
              "Class2": "Monk",
              "Title": 2624
            },
            {
              "Class1": "Paladin",
              "Class2": "Priest",
              "Title": 2625
            },
            {
              "Class1": "Paladin",
              "Class2": "Ranger",
              "Title": 2626
            },
            {
              "Class1": "Priest",
              "Class2": "Barbarian",
              "Title": 2627
            },
            {
              "Class1": "Priest",
              "Class2": "Chanter",
              "Title": 2628
            },
            {
              "Class1": "Priest",
              "Class2": "Cipher",
              "Title": 2629
            },
            {
              "Class1": "Priest",
              "Class2": "Druid",
              "Title": 2630
            },
            {
              "Class1": "Priest",
              "Class2": "Monk",
              "Title": 2631
            },
            {
              "Class1": "Priest",
              "Class2": "Ranger",
              "Title": 2632
            },
            {
              "Class1": "Monk",
              "Class2": "Barbarian",
              "Title": 2633
            },
            {
              "Class1": "Monk",
              "Class2": "Chanter",
              "Title": 2634
            },
            {
              "Class1": "Monk",
              "Class2": "Cipher",
              "Title": 2635
            },
            {
              "Class1": "Monk",
              "Class2": "Druid",
              "Title": 2636
            },
            {
              "Class1": "Monk",
              "Class2": "Ranger",
              "Title": 2637
            },
            {
              "Class1": "Ranger",
              "Class2": "Chanter",
              "Title": 2638
            },
            {
              "Class1": "Ranger",
              "Class2": "Cipher",
              "Title": 2639
            },
            {
              "Class1": "Ranger",
              "Class2": "Barbarian",
              "Title": 2640
            },
            {
              "Class1": "Ranger",
              "Class2": "Druid",
              "Title": 2641
            },
            {
              "Class1": "Chanter",
              "Class2": "Druid",
              "Title": 2642
            },
            {
              "Class1": "Chanter",
              "Class2": "Cipher",
              "Title": 2643
            },
            {
              "Class1": "Chanter",
              "Class2": "Barbarian",
              "Title": 2644
            },
            {
              "Class1": "Druid",
              "Class2": "Cipher",
              "Title": 2645
            },
            {
              "Class1": "Druid",
              "Class2": "Barbarian",
              "Title": 2646
            },
            {
              "Class1": "Cipher",
              "Class2": "Barbarian",
              "Title": 2647
            }
          ],
          "CharacterCreationValidRacesIDs": [
            "5ea84750-17c7-419b-8beb-c121c5d68c55",
            "1f60b08e-1966-4bb6-8cdb-757294dcc18a",
            "c3520c0a-dff9-45b1-9ee0-d905c933b62b",
            "b85d4148-b922-43a2-831c-f189c0b3569d",
            "74e606b7-022a-40af-858d-83dd90a98e62",
            "25308722-9480-477a-94cf-d40b6fd0886e"
          ],
          "CharacterCreationValidCulturesIDs": [
            "c8038e79-c612-45ec-be2a-ff428785e5d5",
            "7093880b-7527-4087-a764-d6613701a025",
            "589e4e1d-6b55-45f8-a2fd-c42eeb2b2c18",
            "5bb77c17-fbb2-489f-bdae-ecd9ebeae467",
            "59a4c801-24af-4fd8-bb6e-39c5c0b4ca5d",
            "3886897f-93f4-44a9-902f-6dcc012ae114",
            "80658ad7-bcbf-435f-b7a5-522fe629004e"
          ],
          "CharacterCreationValidGendersIDs": [
            "60ec0a05-7a1f-4f1e-838a-ee764fe3a218",
            "77369631-33c8-403f-8e12-068691cbf3c3"
          ],
          "SingleClassPowerLevelByCharacterLevel": [
            "1",
            "1",
            "2",
            "2",
            "3",
            "3",
            "4",
            "4",
            "5",
            "5",
            "6",
            "6",
            "7",
            "7",
            "7",
            "8",
            "8",
            "8",
            "9",
            "9",
            "9",
            "10",
            "10",
            "10",
            "11",
            "11",
            "11",
            "12",
            "12",
            "12"
          ],
          "MultiClassPowerLevelByCharacterLevel": [
            "1",
            "1",
            "1",
            "2",
            "2",
            "2",
            "3",
            "3",
            "3",
            "4",
            "4",
            "4",
            "5",
            "5",
            "5",
            "6",
            "6",
            "6",
            "7",
            "7",
            "7",
            "8",
            "8",
            "8",
            "9",
            "9",
            "9",
            "10",
            "10",
            "10"
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.GlobalShipSettingsGameData, Assembly-CSharp",
      "DebugName": "GlobalShipSettings",
      "ID": "68f6ef6b-b32c-401b-838f-ba4b20ce623d",
      "Components": [
        {
          "$type": "Game.GameData.GlobalShipSettingsComponent, Assembly-CSharp",
          "HungerSurvivalTable": [
            {
              "EfficiencyDecrease": 0.1,
              "MinimumDaysThreshold": 1,
              "DeathConstitutionModifier": 0
            },
            {
              "EfficiencyDecrease": 0.2,
              "MinimumDaysThreshold": 8,
              "DeathConstitutionModifier": 0
            },
            {
              "EfficiencyDecrease": 0.4,
              "MinimumDaysThreshold": 15,
              "DeathConstitutionModifier": 2
            }
          ],
          "ThirstSurvivalTable": [
            {
              "EfficiencyDecrease": 0.2,
              "MinimumDaysThreshold": 1,
              "DeathConstitutionModifier": 0
            },
            {
              "EfficiencyDecrease": 0.4,
              "MinimumDaysThreshold": 3,
              "DeathConstitutionModifier": 0
            },
            {
              "EfficiencyDecrease": 0.6,
              "MinimumDaysThreshold": 5,
              "DeathConstitutionModifier": 4
            }
          ],
          "BaseRepairUnitHeal": 2,
          "BaseRepairUnitUseRate": 0.25,
          "CrewTraitUnlockRequirements": [
            {
              "SailorTales": 0
            },
            {
              "SailorTales": 5
            },
            {
              "SailorTales": 10
            },
            {
              "SailorTales": 15
            },
            {
              "SailorTales": 25
            },
            {
              "SailorTales": 50
            }
          ],
          "SlotPlaceholderItems": [
            {
              "Slot": "Hull",
              "UpgradeID": "f6169592-78dd-40e5-a517-21338be17833"
            },
            {
              "Slot": "Sails",
              "UpgradeID": "c3a0f719-18bd-4f6c-8ee6-83aaecb6d940"
            },
            {
              "Slot": "Wheel",
              "UpgradeID": "550cb266-9b66-4874-8a30-f464442a9bc4"
            },
            {
              "Slot": "Lantern",
              "UpgradeID": "638eebaf-8d62-4239-8088-e85c9e40db08"
            },
            {
              "Slot": "Anchors",
              "UpgradeID": "d215c402-16d3-4359-af64-739c1e9b8b56"
            }
          ],
          "MedicalSuppliesPerInjuryDay": 5,
          "MoraleGlossaryEntryID": "7e972ad8-026e-40bb-9f5b-84ff74856939",
          "FoodGlossaryEntryID": "6676877a-1392-4ead-b1be-05f8400d517f",
          "DrinkGlossaryEntryID": "cb7f2b65-d4a7-488d-b8b4-ede1cf7ed781",
          "ShipDuelMoraleFleeInferior": -10,
          "ShipDuelMoraleFleeSuperior": -5,
          "ShipDuelMoraleWinInferior": 10,
          "ShipDuelMoraleWinSuperior": 15,
          "MoraleStates": [
            {
              "Type": "Lively",
              "MinimumMorale": 81,
              "DisplayName": 4267,
              "ExperienceMultiplier": 1.2
            },
            {
              "Type": "Content",
              "MinimumMorale": 61,
              "DisplayName": 4266,
              "ExperienceMultiplier": 1.1
            },
            {
              "Type": "Concerned",
              "MinimumMorale": 41,
              "DisplayName": 4265,
              "ExperienceMultiplier": 1
            },
            {
              "Type": "Angry",
              "MinimumMorale": 21,
              "DisplayName": 4264,
              "ExperienceMultiplier": 0.9
            },
            {
              "Type": "Mutinous",
              "MinimumMorale": 1,
              "DisplayName": 4263,
              "ExperienceMultiplier": 0.8
            }
          ],
          "ShipLootCoinToMoraleRate": 50,
          "NotEnoughCrewTravelSpeedMult": 0.75,
          "PlayerCaptainID": "ff0869fe-2a00-4216-8eec-6f189ffff3be"
        }
      ]
    },
    {
      "$type": "Game.GameData.SkillManagerGameData, Assembly-CSharp",
      "DebugName": "SkillManager",
      "ID": "ecfc6eff-f96f-48c0-8a23-2cd9c1342743",
      "Components": [
        {
          "$type": "Game.GameData.SkillManagerComponent, Assembly-CSharp",
          "StealthSkillID": "8cc41e27-e62f-4e75-9f7d-ae47986895d2",
          "AthleticsSkillID": "fefc4d3d-250d-4c32-85e0-62a851240e62",
          "MechanicsSkillID": "3affbb70-86bd-41f3-82c6-325326d40796",
          "SurvivalSkillID": "9c7962b8-bf69-4670-ba67-acc86a09fca8",
          "AlchemySkillID": "e67f20e3-5bf9-4d87-9bda-211405107362",
          "HerbalismSkillID": "1b26af87-fd19-4dc6-9380-4f5cec7aefd5",
          "ArcanaSkillID": "e5f33551-ec5c-4cc8-b5f2-1eb03d210374",
          "SleightOfHandSkillID": "3b073ebf-0a4a-48a3-947d-ad38bef10b36"
        },
        {
          "$type": "Game.GameData.SkillAssistThresholdComponent, Assembly-CSharp",
          "AssistThresholds": [
            {
              "Threshold": 1,
              "AssistValue": 1
            },
            {
              "Threshold": 2,
              "AssistValue": 2
            },
            {
              "Threshold": 4,
              "AssistValue": 3
            },
            {
              "Threshold": 7,
              "AssistValue": 4
            },
            {
              "Threshold": 11,
              "AssistValue": 5
            },
            {
              "Threshold": 16,
              "AssistValue": 6
            },
            {
              "Threshold": 22,
              "AssistValue": 7
            },
            {
              "Threshold": 29,
              "AssistValue": 8
            },
            {
              "Threshold": 37,
              "AssistValue": 9
            },
            {
              "Threshold": 46,
              "AssistValue": 10
            }
          ]
        },
        {
          "$type": "Game.GameData.SleightOfHandSettingsComponent, Assembly-CSharp",
          "VisionModifier": 2,
          "DetectionModifier": 2,
          "LevelModifiers": [
            {
              "Level": 1,
              "Modifier": 1
            },
            {
              "Level": 2,
              "Modifier": 2
            },
            {
              "Level": 3,
              "Modifier": 3
            },
            {
              "Level": 4,
              "Modifier": 3
            },
            {
              "Level": 5,
              "Modifier": 4
            },
            {
              "Level": 6,
              "Modifier": 4
            },
            {
              "Level": 7,
              "Modifier": 5
            },
            {
              "Level": 8,
              "Modifier": 5
            },
            {
              "Level": 9,
              "Modifier": 5
            },
            {
              "Level": 10,
              "Modifier": 6
            },
            {
              "Level": 11,
              "Modifier": 6
            },
            {
              "Level": 12,
              "Modifier": 6
            },
            {
              "Level": 13,
              "Modifier": 7
            },
            {
              "Level": 14,
              "Modifier": 7
            },
            {
              "Level": 15,
              "Modifier": 7
            },
            {
              "Level": 16,
              "Modifier": 8
            },
            {
              "Level": 17,
              "Modifier": 8
            },
            {
              "Level": 18,
              "Modifier": 8
            },
            {
              "Level": 19,
              "Modifier": 9
            },
            {
              "Level": 20,
              "Modifier": 9
            }
          ],
          "ItemModifiers": [
            {
              "ItemType": "Weapons",
              "Modifier": 2
            },
            {
              "ItemType": "Armor",
              "Modifier": 3
            },
            {
              "ItemType": "Clothing",
              "Modifier": 1
            },
            {
              "ItemType": "Consumables",
              "Modifier": 0
            },
            {
              "ItemType": "Ingredients",
              "Modifier": -1
            },
            {
              "ItemType": "Quest",
              "Modifier": -3
            },
            {
              "ItemType": "Misc",
              "Modifier": -3
            },
            {
              "ItemType": "ShipCrew",
              "Modifier": 0
            },
            {
              "ItemType": "FoodOrDrink",
              "Modifier": 0
            },
            {
              "ItemType": "ShipSails",
              "Modifier": 0
            },
            {
              "ItemType": "ShipHulls",
              "Modifier": 0
            },
            {
              "ItemType": "ShipFlags",
              "Modifier": 0
            },
            {
              "ItemType": "ShipCannons",
              "Modifier": 0
            },
            {
              "ItemType": "ShipUpgrades",
              "Modifier": 0
            },
            {
              "ItemType": "ShipPermanentUpgrades",
              "Modifier": 0
            },
            {
              "ItemType": "Artifact",
              "Modifier": 0
            }
          ]
        },
        {
          "$type": "Game.GameData.StealthSettingsComponent, Assembly-CSharp",
          "HearingSuspicionRate": 40,
          "VisionSuspicionRate": 80,
          "HearingMovementRatio": 0.75,
          "StealthDecayRate": 15,
          "StealthDecayDelay": 0.75,
          "MaxSuspicionStealthAccelerationMultiplier": 4,
          "MaxSuspicionStealthDecelerationMultiplier": 0.05,
          "MinDistanceMultiplier": 2,
          "MaxDistanceMultiplier": 0.5
        }
      ]
    },
    {
      "$type": "Game.GameData.WorldTimeSettingsGameData, Assembly-CSharp",
      "DebugName": "WorldTimeSettings",
      "ID": "8c7d839c-2a6b-4b30-a550-af57bb261b19",
      "Components": [
        {
          "$type": "Game.GameData.WorldTimeSettingsComponent, Assembly-CSharp",
          "GameToRealTimeRatio": 24,
          "StartTime": {
            "Year": 2823,
            "Month": 4,
            "Day": 18,
            "Hour": 7,
            "Minute": 0,
            "Second": 0
          },
          "DaytimeStartHour": 6,
          "NighttimeStartHour": 20,
          "YearSuffix": 33,
          "MonthTransitionNames": [
            {
              "Text": 21,
              "TranslatedText": 21
            },
            {
              "Text": 3927,
              "TranslatedText": 22
            },
            {
              "Text": 3927,
              "TranslatedText": 22
            },
            {
              "Text": 3927,
              "TranslatedText": 22
            },
            {
              "Text": 3928,
              "TranslatedText": 23
            },
            {
              "Text": 3928,
              "TranslatedText": 23
            },
            {
              "Text": 3928,
              "TranslatedText": 23
            },
            {
              "Text": 24,
              "TranslatedText": 24
            },
            {
              "Text": 3929,
              "TranslatedText": 25
            },
            {
              "Text": 3929,
              "TranslatedText": 25
            },
            {
              "Text": 3929,
              "TranslatedText": 25
            },
            {
              "Text": 3930,
              "TranslatedText": 26
            },
            {
              "Text": 3930,
              "TranslatedText": 26
            },
            {
              "Text": 3930,
              "TranslatedText": 26
            }
          ],
          "NormalMonthNames": [
            {
              "Text": 5,
              "TranslatedText": 1034
            },
            {
              "Text": 6,
              "TranslatedText": 1035
            },
            {
              "Text": 7,
              "TranslatedText": 1036
            },
            {
              "Text": 8,
              "TranslatedText": 1037
            },
            {
              "Text": 9,
              "TranslatedText": 1038
            },
            {
              "Text": 10,
              "TranslatedText": 1039
            },
            {
              "Text": 11,
              "TranslatedText": 1040
            },
            {
              "Text": 12,
              "TranslatedText": 1041
            },
            {
              "Text": 13,
              "TranslatedText": 1042
            },
            {
              "Text": 14,
              "TranslatedText": 1043
            },
            {
              "Text": 15,
              "TranslatedText": 1044
            },
            {
              "Text": 16,
              "TranslatedText": 1045
            },
            {
              "Text": 17,
              "TranslatedText": 1046
            },
            {
              "Text": 18,
              "TranslatedText": 1047
            },
            {
              "Text": 19,
              "TranslatedText": 1048
            },
            {
              "Text": 20,
              "TranslatedText": 1049
            }
          ],
          "DayNames": [
            {
              "Text": 0,
              "TranslatedText": -1
            },
            {
              "Text": 1,
              "TranslatedText": -1
            },
            {
              "Text": 2,
              "TranslatedText": -1
            },
            {
              "Text": 3,
              "TranslatedText": -1
            },
            {
              "Text": 4,
              "TranslatedText": -1
            }
          ],
          "TimesOfDay": [
            {
              "DisplayName": 2578,
              "EndHour": 2
            },
            {
              "DisplayName": 2579,
              "EndHour": 7
            },
            {
              "DisplayName": 2580,
              "EndHour": 12
            },
            {
              "DisplayName": 2581,
              "EndHour": 15
            },
            {
              "DisplayName": 2582,
              "EndHour": 19
            },
            {
              "DisplayName": 2583,
              "EndHour": 24
            },
            {
              "DisplayName": 2578,
              "EndHour": 26
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ShipAttributeGameData, Assembly-CSharp",
      "DebugName": "CombatSpeed",
      "ID": "aafe07d6-960e-4270-8526-a6d4924b127f",
      "Components": [
        {
          "$type": "Game.GameData.ShipAttributeComponent, Assembly-CSharp",
          "Type": "CombatSpeed",
          "DisplayName": 3615,
          "Icon": "gui/icons/gamesystems/icon_ship_combat_speed.png"
        }
      ]
    },
    {
      "$type": "Game.GameData.ShipAttributeGameData, Assembly-CSharp",
      "DebugName": "HullHealth",
      "ID": "07223240-cc8f-4a0b-8b66-51c6e8fe73cc",
      "Components": [
        {
          "$type": "Game.GameData.ShipAttributeComponent, Assembly-CSharp",
          "Type": "HullHealth",
          "DisplayName": 3276,
          "Icon": "gui/icons/gamesystems/icon_ship_hull_health.png"
        }
      ]
    },
    {
      "$type": "Game.GameData.ShipAttributeGameData, Assembly-CSharp",
      "DebugName": "SailHealth",
      "ID": "2a62efe9-7442-4122-b91e-98a80816804f",
      "Components": [
        {
          "$type": "Game.GameData.ShipAttributeComponent, Assembly-CSharp",
          "Type": "SailHealth",
          "DisplayName": 3275,
          "Icon": "gui/icons/gamesystems/icon_ship_sail_health.png"
        }
      ]
    },
    {
      "$type": "Game.GameData.ShipAttributeGameData, Assembly-CSharp",
      "DebugName": "TravelSpeed",
      "ID": "315413f3-93ee-4772-b3e5-a3ccbff537e4",
      "Components": [
        {
          "$type": "Game.GameData.ShipAttributeComponent, Assembly-CSharp",
          "Type": "TravelSpeed",
          "DisplayName": 3270,
          "Icon": "gui/icons/gamesystems/icon_ship_water_speed.png"
        }
      ]
    },
    {
      "$type": "Game.GameData.BestiarySettingsGameData, Assembly-CSharp",
      "DebugName": "BestiarySettings",
      "ID": "4261ca60-8077-4d09-b4aa-46b744b91e19",
      "Components": [
        {
          "$type": "Game.GameData.BestiarySettingsComponent, Assembly-CSharp",
          "LevelScalingThresholdModifier": 3,
          "DefaultReveal": [
            {
              "Stat": "None",
              "RevealPoint": 0
            },
            {
              "Stat": "Kills",
              "RevealPoint": 0
            },
            {
              "Stat": "Name",
              "RevealPoint": 0
            },
            {
              "Stat": "Picture",
              "RevealPoint": 0
            },
            {
              "Stat": "Level",
              "RevealPoint": 0
            },
            {
              "Stat": "ObsoleteHealth",
              "RevealPoint": 0.6
            },
            {
              "Stat": "Accuracy",
              "RevealPoint": 0.4
            },
            {
              "Stat": "Might",
              "RevealPoint": 0.5
            },
            {
              "Stat": "Dexterity",
              "RevealPoint": 0.5
            },
            {
              "Stat": "Resolve",
              "RevealPoint": 0.5
            },
            {
              "Stat": "Intellect",
              "RevealPoint": 0.5
            },
            {
              "Stat": "Constitution",
              "RevealPoint": 0.5
            },
            {
              "Stat": "Perception",
              "RevealPoint": 0.5
            },
            {
              "Stat": "Deflection",
              "RevealPoint": 0
            },
            {
              "Stat": "Fortitude",
              "RevealPoint": 0
            },
            {
              "Stat": "Reflexes",
              "RevealPoint": 0
            },
            {
              "Stat": "Will",
              "RevealPoint": 0
            },
            {
              "Stat": "ArmorRating",
              "RevealPoint": 0.2
            },
            {
              "Stat": "Description",
              "RevealPoint": 0
            },
            {
              "Stat": "SpecialArmorRatings",
              "RevealPoint": 0.2
            },
            {
              "Stat": "Health",
              "RevealPoint": 0.6
            },
            {
              "Stat": "PrimaryDamage",
              "RevealPoint": 0.7
            },
            {
              "Stat": "SecondaryDamage",
              "RevealPoint": 0.7
            },
            {
              "Stat": "Abilities",
              "RevealPoint": 1
            },
            {
              "Stat": "Race",
              "RevealPoint": 0
            },
            {
              "Stat": "Immunities",
              "RevealPoint": 1
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.DLCScriptGameData, Assembly-CSharp",
      "DebugName": "DLCStartingScripts",
      "ID": "7ab2136c-09bf-478f-adbf-40f8fb864e25",
      "Components": [
        {
          "$type": "Game.GameData.DLCScriptData, Assembly-CSharp",
          "LAX1ScriptSet": {
            "GlobalConditional": {
              "Operator": 0,
              "Components": [
                {
                  "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                  "Data": {
                    "FullName": "Boolean CallGlobalConditional(Guid)",
                    "Parameters": [
                      "8b284a9b-ed54-429f-be2e-cdf848302554"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": -1996378483,
                    "ParameterHash": 795935198
                  },
                  "Not": false,
                  "Operator": 0
                }
              ]
            },
            "GlobalScript": [
              {
                "Data": {
                  "FullName": "Void CallGlobalScript(Guid)",
                  "Parameters": [
                    "b4dce97c-2ab4-48ea-9929-272cb4f06a70"
                  ],
                  "UnrealCall": "",
                  "FunctionHash": -1122074996,
                  "ParameterHash": 699539195
                },
                "Conditional": {
                  "Operator": 0,
                  "Components": []
                }
              }
            ]
          },
          "LAX2ScriptSet": {
            "GlobalConditional": {
              "Operator": 0,
              "Components": [
                {
                  "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                  "Data": {
                    "FullName": "Boolean CallGlobalConditional(Guid)",
                    "Parameters": [
                      "00000000-0000-0000-0000-000000000000"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": -1996378483,
                    "ParameterHash": 1580774737
                  },
                  "Not": false,
                  "Operator": 0
                }
              ]
            },
            "GlobalScript": [
              {
                "Data": {
                  "FullName": "Void CallGlobalScript(Guid)",
                  "Parameters": [
                    "00000000-0000-0000-0000-000000000000"
                  ],
                  "UnrealCall": "",
                  "FunctionHash": -1122074996,
                  "ParameterHash": -1380588238
                },
                "Conditional": {
                  "Operator": 0,
                  "Components": []
                }
              }
            ]
          },
          "LAX3ScriptSet": {
            "GlobalConditional": {
              "Operator": 0,
              "Components": [
                {
                  "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                  "Data": {
                    "FullName": "Boolean CallGlobalConditional(Guid)",
                    "Parameters": [
                      "00000000-0000-0000-0000-000000000000"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": -1996378483,
                    "ParameterHash": 1580774737
                  },
                  "Not": false,
                  "Operator": 0
                }
              ]
            },
            "GlobalScript": [
              {
                "Data": {
                  "FullName": "Void CallGlobalScript(Guid)",
                  "Parameters": [
                    "00000000-0000-0000-0000-000000000000"
                  ],
                  "UnrealCall": "",
                  "FunctionHash": -1122074996,
                  "ParameterHash": -1380588238
                },
                "Conditional": {
                  "Operator": 0,
                  "Components": []
                }
              }
            ]
          },
          "LAX4ScriptSet": {
            "GlobalConditional": {
              "Operator": 0,
              "Components": [
                {
                  "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                  "Data": {
                    "FullName": "Boolean CallGlobalConditional(Guid)",
                    "Parameters": [
                      "00000000-0000-0000-0000-000000000000"
                    ],
                    "UnrealCall": "",
                    "FunctionHash": -1996378483,
                    "ParameterHash": 1580774737
                  },
                  "Not": false,
                  "Operator": 0
                }
              ]
            },
            "GlobalScript": [
              {
                "Data": {
                  "FullName": "Void CallGlobalScript(Guid)",
                  "Parameters": [
                    "00000000-0000-0000-0000-000000000000"
                  ],
                  "UnrealCall": "",
                  "FunctionHash": -1122074996,
                  "ParameterHash": -1380588238
                },
                "Conditional": {
                  "Operator": 0,
                  "Components": []
                }
              }
            ]
          }
        }
      ]
    },
    {
      "$type": "Game.GameData.ExperienceTableGameData, Assembly-CSharp",
      "DebugName": "ExperienceTable",
      "ID": "960ac31f-ed36-4a06-97ad-f5fc921a6866",
      "Components": [
        {
          "$type": "Game.GameData.ExperienceTableComponent, Assembly-CSharp",
          "ExperienceReference": [
            {
              "Level": 1,
              "TotalExperience": 1000,
              "QuestExperiencePool": 700,
              "CombatExperiencePool": 200,
              "ExplorationExperiencePool": 70,
              "TrapLockExperiencePool": 30
            },
            {
              "Level": 2,
              "TotalExperience": 3000,
              "QuestExperiencePool": 1400,
              "CombatExperiencePool": 400,
              "ExplorationExperiencePool": 140,
              "TrapLockExperiencePool": 60
            },
            {
              "Level": 3,
              "TotalExperience": 6000,
              "QuestExperiencePool": 2100,
              "CombatExperiencePool": 600,
              "ExplorationExperiencePool": 210,
              "TrapLockExperiencePool": 90
            },
            {
              "Level": 4,
              "TotalExperience": 10000,
              "QuestExperiencePool": 2800,
              "CombatExperiencePool": 800,
              "ExplorationExperiencePool": 280,
              "TrapLockExperiencePool": 120
            },
            {
              "Level": 5,
              "TotalExperience": 15000,
              "QuestExperiencePool": 6500,
              "CombatExperiencePool": 1000,
              "ExplorationExperiencePool": 350,
              "TrapLockExperiencePool": 150
            },
            {
              "Level": 6,
              "TotalExperience": 21000,
              "QuestExperiencePool": 4200,
              "CombatExperiencePool": 1200,
              "ExplorationExperiencePool": 420,
              "TrapLockExperiencePool": 180
            },
            {
              "Level": 7,
              "TotalExperience": 28000,
              "QuestExperiencePool": 4900,
              "CombatExperiencePool": 1400,
              "ExplorationExperiencePool": 490,
              "TrapLockExperiencePool": 210
            },
            {
              "Level": 8,
              "TotalExperience": 36000,
              "QuestExperiencePool": 5600,
              "CombatExperiencePool": 1600,
              "ExplorationExperiencePool": 560,
              "TrapLockExperiencePool": 240
            },
            {
              "Level": 9,
              "TotalExperience": 45000,
              "QuestExperiencePool": 6300,
              "CombatExperiencePool": 1800,
              "ExplorationExperiencePool": 630,
              "TrapLockExperiencePool": 270
            },
            {
              "Level": 10,
              "TotalExperience": 55000,
              "QuestExperiencePool": 7000,
              "CombatExperiencePool": 2000,
              "ExplorationExperiencePool": 700,
              "TrapLockExperiencePool": 300
            },
            {
              "Level": 11,
              "TotalExperience": 66000,
              "QuestExperiencePool": 7700,
              "CombatExperiencePool": 2200,
              "ExplorationExperiencePool": 770,
              "TrapLockExperiencePool": 330
            },
            {
              "Level": 12,
              "TotalExperience": 78000,
              "QuestExperiencePool": 8400,
              "CombatExperiencePool": 2400,
              "ExplorationExperiencePool": 840,
              "TrapLockExperiencePool": 360
            },
            {
              "Level": 13,
              "TotalExperience": 91000,
              "QuestExperiencePool": 9100,
              "CombatExperiencePool": 2600,
              "ExplorationExperiencePool": 910,
              "TrapLockExperiencePool": 390
            },
            {
              "Level": 14,
              "TotalExperience": 105000,
              "QuestExperiencePool": 9800,
              "CombatExperiencePool": 2800,
              "ExplorationExperiencePool": 980,
              "TrapLockExperiencePool": 420
            },
            {
              "Level": 15,
              "TotalExperience": 120000,
              "QuestExperiencePool": 10500,
              "CombatExperiencePool": 3000,
              "ExplorationExperiencePool": 1050,
              "TrapLockExperiencePool": 450
            },
            {
              "Level": 16,
              "TotalExperience": 136000,
              "QuestExperiencePool": 11200,
              "CombatExperiencePool": 3200,
              "ExplorationExperiencePool": 1120,
              "TrapLockExperiencePool": 480
            },
            {
              "Level": 17,
              "TotalExperience": 153000,
              "QuestExperiencePool": 11900,
              "CombatExperiencePool": 3400,
              "ExplorationExperiencePool": 1190,
              "TrapLockExperiencePool": 510
            },
            {
              "Level": 18,
              "TotalExperience": 171000,
              "QuestExperiencePool": 12600,
              "CombatExperiencePool": 3600,
              "ExplorationExperiencePool": 1260,
              "TrapLockExperiencePool": 540
            },
            {
              "Level": 19,
              "TotalExperience": 190000,
              "QuestExperiencePool": 13300,
              "CombatExperiencePool": 3800,
              "ExplorationExperiencePool": 1330,
              "TrapLockExperiencePool": 570
            },
            {
              "Level": 20,
              "TotalExperience": 210000,
              "QuestExperiencePool": 0,
              "CombatExperiencePool": 0,
              "ExplorationExperiencePool": 0,
              "TrapLockExperiencePool": 0
            }
          ],
          "MajorQuestWeight": 4,
          "StandardQuestWeight": 2,
          "MinorQuestWeight": 1,
          "StoredExperiencePercent": 90,
          "StoredExperienceHigherLevelPercent": 50,
          "PartySizeBonusExperiencePercent": 25,
          "BestiaryExperience": 7,
          "MapExplorationExperience": 5,
          "MapMarkerDiscoveryExperience": 2,
          "DisarmTrapExperienceModifier": 5,
          "PickLockExperienceModifier": 5,
          "QuestXPMultiplier": 1.35,
          "CombatXPMultiplier": 1.25,
          "ExplorationXPMultiplier": 1,
          "TrapLockXPMultiplier": 1,
          "WorldMapEncounterExperience": [
            {
              "ExperienceList": [
                {
                  "Level": 1,
                  "TotalExperience": 30,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 2,
                  "TotalExperience": 60,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 3,
                  "TotalExperience": 90,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 4,
                  "TotalExperience": 120,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 5,
                  "TotalExperience": 150,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 6,
                  "TotalExperience": 180,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 7,
                  "TotalExperience": 210,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 8,
                  "TotalExperience": 240,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 9,
                  "TotalExperience": 270,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 10,
                  "TotalExperience": 300,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 11,
                  "TotalExperience": 330,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 12,
                  "TotalExperience": 360,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 13,
                  "TotalExperience": 390,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 14,
                  "TotalExperience": 420,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 15,
                  "TotalExperience": 450,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 16,
                  "TotalExperience": 480,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 17,
                  "TotalExperience": 510,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 18,
                  "TotalExperience": 540,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 19,
                  "TotalExperience": 570,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 20,
                  "TotalExperience": 600,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                }
              ],
              "EncounterType": "Minor"
            },
            {
              "ExperienceList": [
                {
                  "Level": 1,
                  "TotalExperience": 50,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 2,
                  "TotalExperience": 100,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 3,
                  "TotalExperience": 150,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 4,
                  "TotalExperience": 200,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 5,
                  "TotalExperience": 250,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 6,
                  "TotalExperience": 300,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 7,
                  "TotalExperience": 350,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 8,
                  "TotalExperience": 400,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 9,
                  "TotalExperience": 450,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 10,
                  "TotalExperience": 500,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 11,
                  "TotalExperience": 550,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 12,
                  "TotalExperience": 600,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 13,
                  "TotalExperience": 650,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 14,
                  "TotalExperience": 700,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 15,
                  "TotalExperience": 750,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 16,
                  "TotalExperience": 800,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 17,
                  "TotalExperience": 850,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 18,
                  "TotalExperience": 900,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 19,
                  "TotalExperience": 950,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 20,
                  "TotalExperience": 1000,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                }
              ],
              "EncounterType": "Moderate"
            },
            {
              "ExperienceList": [
                {
                  "Level": 1,
                  "TotalExperience": 100,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 2,
                  "TotalExperience": 200,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 3,
                  "TotalExperience": 300,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 4,
                  "TotalExperience": 400,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 5,
                  "TotalExperience": 500,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 6,
                  "TotalExperience": 600,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 7,
                  "TotalExperience": 700,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 8,
                  "TotalExperience": 800,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 9,
                  "TotalExperience": 900,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 10,
                  "TotalExperience": 1000,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 11,
                  "TotalExperience": 1100,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 12,
                  "TotalExperience": 1200,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 13,
                  "TotalExperience": 1300,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 14,
                  "TotalExperience": 1400,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 15,
                  "TotalExperience": 1500,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 16,
                  "TotalExperience": 1600,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 17,
                  "TotalExperience": 1700,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 18,
                  "TotalExperience": 1800,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 19,
                  "TotalExperience": 1900,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                },
                {
                  "Level": 20,
                  "TotalExperience": 2000,
                  "QuestExperiencePool": 0,
                  "CombatExperiencePool": 0,
                  "ExplorationExperiencePool": 0,
                  "TrapLockExperiencePool": 0
                }
              ],
              "EncounterType": "Major"
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.GlobalGameSettingsGameData, Assembly-CSharp",
      "DebugName": "GlobalGameSettings",
      "ID": "eddfc852-ccb9-4884-b901-e77e8ca31b48",
      "Components": [
        {
          "$type": "Game.GameData.CharacterStatsSettingsComponent, Assembly-CSharp",
          "BasePlayerLevelCap": 20,
          "MaxCharacterLevel": 30,
          "AttributeAverage": 10,
          "MaxPerceptionDistance": 7,
          "MaxStealthRadius": 4,
          "DefaultWalkSpeed": 2,
          "DefaultRunSpeed": 4,
          "DefaultAnimalCompanionWalkSpeed": 2,
          "DefaultAnimalCompanionRunSpeed": 5.5,
          "CombatHealthRechargeRate": 1,
          "NormalHealthRechargeRate": 10,
          "HealthRechargeDelay": 3,
          "ModalRecoveryTime": 3,
          "HitMultiplier": 1,
          "GlobalRaceScale": [
            {
              "Race": "Human",
              "MaleScale": 1,
              "FemaleScale": 0.95
            },
            {
              "Race": "Elf",
              "MaleScale": 0.95,
              "FemaleScale": 0.92
            },
            {
              "Race": "Dwarf",
              "MaleScale": 0.85,
              "FemaleScale": 0.81
            },
            {
              "Race": "Godlike",
              "MaleScale": 1,
              "FemaleScale": 0.95
            },
            {
              "Race": "Orlan",
              "MaleScale": 0.76,
              "FemaleScale": 0.77
            },
            {
              "Race": "Aumaua",
              "MaleScale": 1.15,
              "FemaleScale": 1.12
            }
          ],
          "DefaultEnemiesToFlank": 2,
          "AttributeHealthMultiplier": 0.05,
          "AttributeDefenseAdjustment": 2,
          "AttributeDeflectionAdjustment": 1,
          "AttributeAccuracyAdjustment": 1,
          "AttributeDamageMultiplier": 0.03,
          "AttributeAttackSpeedMultiplier": 0.03,
          "AttributeDurationMultiplier": 0.05,
          "AttributeIncomingHostileEffectDuationMultiplier": -0.03,
          "AttributeAreaMultiplier": 0.1,
          "IngestibleDurationMultiplier": 0,
          "MaxAttributeScore": 35,
          "PersonalInventorySize": 14,
          "BaseQuickbarSize": 4,
          "BaseWeaponSets": 2
        },
        {
          "$type": "Game.GameData.SpellSettingsComponent, Assembly-CSharp",
          "MaxSpellLevel": 9,
          "GrimoireMaxSpellsPerLevel": 2
        },
        {
          "$type": "Game.GameData.AISettingsComponent, Assembly-CSharp",
          "MaxTargetingSearchDistance": 20,
          "RequestHelpOriginArrivalRadius": 2,
          "RequestHelpShoutRangePathingScalar": 1.5,
          "RequestHelpShoutInterval": 2,
          "PartyMemberIsNearCombatDistance": 20,
          "AIScheduleFastForwardPositionInterpolationTime": 0.5,
          "AIScheduleFastForwardPositionMovementSpeed": 2,
          "DefendSelfAutoAttackSearchDistance": 4.5,
          "DefensiveAutoAttackSearchDistance": 7,
          "AggressiveAutoAttackSearchDistance": 20,
          "DecisionTreeTargetingPreviousTargetDistanceThreshold": 0.5,
          "ActionReevaluationInterval": 1,
          "RetreatOutOfCombatTime": 2,
          "PrimaryAttackReevaluationInterval": 1,
          "MinimumMoverRadius": 0.5,
          "PartyFormationStartMovementMaxFrameDelay": 4,
          "PartyFormationStartMovementFrameTime": 0.04,
          "PathingBlockedDistanceDifferenceThreshold": 0.75,
          "StationaryInCombatRotationRate": 960,
          "StationaryOutOfCombatRotationRate": 480,
          "MovingRotationRate": 720,
          "MovingSubtleRotationRate": 180,
          "MovingSubtleRotationThreshold": 38,
          "OffMeshAreaMaxForwardDistance": 1.2,
          "OffMeshAreaMaxHorizontalDistance": 0.7,
          "OffMeshAreaMaxDestinationOffset": 0.7,
          "JumpTravelTimes": [
            "0",
            "0.533"
          ],
          "BlockingDoorMaxWaitDuration": 5,
          "AutomaticDoorCloseTime": 2,
          "SearchTetherTime": 5,
          "KiteTargetNearMinimumDistance": 12,
          "KiteTargetFarMinimumDistance": 3,
          "KiteTargetIgnoreDuration": 5,
          "RetreatMinimumDistance": 2,
          "ThreatTrackingDuration": 5,
          "LevelThreatModifier": 1,
          "DamageThreatModifier": 1,
          "RangeThreatModifier": 1,
          "PreviousTargetThreatThreshold": 1.2,
          "TargetScanMinimumCombatDelay": 0.25,
          "TargetScanMaximumCombatDelay": 0.75,
          "DefaultHoldTime": 5,
          "MovingBufferDistance": 5,
          "CombatMoveSpeedMatchDistance": 1.5
        },
        {
          "$type": "Game.GameData.WeaponSpecializationDataComponent, Assembly-CSharp",
          "AdventurerWeaponTypes": [
            {
              "WeaponType": "Pollaxe"
            },
            {
              "WeaponType": "Estoc"
            },
            {
              "WeaponType": "Flail"
            },
            {
              "WeaponType": "Wand"
            },
            {
              "WeaponType": "WarBow"
            }
          ],
          "KnightWeaponTypes": [
            {
              "WeaponType": "BattleAxe"
            },
            {
              "WeaponType": "Sword"
            },
            {
              "WeaponType": "MorningStar"
            },
            {
              "WeaponType": "Crossbow"
            }
          ],
          "NobleWeaponTypes": [
            {
              "WeaponType": "Dagger"
            },
            {
              "WeaponType": "Rapier"
            },
            {
              "WeaponType": "Mace"
            },
            {
              "WeaponType": "Sceptre"
            },
            {
              "WeaponType": "Rod"
            }
          ],
          "PeasantWeaponTypes": [
            {
              "WeaponType": "Hatchet"
            },
            {
              "WeaponType": "Spear"
            },
            {
              "WeaponType": "Quarterstaff"
            },
            {
              "WeaponType": "HuntingBow"
            },
            {
              "WeaponType": "Unarmed"
            }
          ],
          "RuffianWeaponTypes": [
            {
              "WeaponType": "Sabre"
            },
            {
              "WeaponType": "Stiletto"
            },
            {
              "WeaponType": "Club"
            },
            {
              "WeaponType": "Pistol"
            },
            {
              "WeaponType": "Blunderbuss"
            }
          ],
          "SoldierWeaponTypes": [
            {
              "WeaponType": "GreatSword"
            },
            {
              "WeaponType": "Pike"
            },
            {
              "WeaponType": "WarHammer"
            },
            {
              "WeaponType": "Arbalest"
            },
            {
              "WeaponType": "BattleAxe"
            }
          ]
        },
        {
          "$type": "Game.GameData.TownieSettingsComponent, Assembly-CSharp",
          "WaypointSearchRadius": 20,
          "GuardSearchRadius": 20,
          "MinWaypointDistanceToCombat": 9,
          "MinGuardDistanceFromCombat": 9,
          "MinFleeAngle": 90,
          "MinFleeAngleBackup": 60,
          "RequestHelpFrequency": 1,
          "CowerRequestHelpImmediately": "true",
          "WaypointRequestHelpImmediately": "true",
          "WaypointRequestHelpIntermittently": "false",
          "WaypointRequestHelpOnArrival": "false",
          "GuardRequestHelpImmediately": "true",
          "GuardRequestHelpIntermittently": "true",
          "GuardRequestHelpOnArrival": "true",
          "RandomCowerProbability": 0.4,
          "RandomFleeToWaypointProbability": 0.4,
          "RandomFleeToGuardProbability": 0.2,
          "MaxCowerDistanceToConsiderFleeing": 6,
          "MinCowerDistanceToBeAbleToFlee": 3,
          "MinCowerTime": 1.3,
          "CowerInCombatDuration": 5
        },
        {
          "$type": "Game.GameData.EconomyComponent, Assembly-CSharp",
          "MaxAdventurers": 8,
          "DefaultRatesID": "bd6efb6b-45e0-477f-b80e-95931b37fa53",
          "ItemModCostMultiplier": 250,
          "AdventurerCostMultiplier": 250,
          "RespecCostMultiplier": 125,
          "TwoHandedCostMultiplier": 1.5
        },
        {
          "$type": "Game.GameData.AppearanceVariationSettingsComponent, Assembly-CSharp",
          "SubraceSkinVariationTags": [
            {
              "Subrace": "Meadow_Human",
              "VariationString": "HUM"
            },
            {
              "Subrace": "Ocean_Human",
              "VariationString": "HUM"
            },
            {
              "Subrace": "Savannah_Human",
              "VariationString": "HUM"
            },
            {
              "Subrace": "Wood_Elf",
              "VariationString": "WLF"
            },
            {
              "Subrace": "Snow_Elf",
              "VariationString": "PLF"
            },
            {
              "Subrace": "Mountain_Dwarf",
              "VariationString": "MDA"
            },
            {
              "Subrace": "Boreal_Dwarf",
              "VariationString": "MDA"
            },
            {
              "Subrace": "Death_Godlike",
              "VariationString": "DGD"
            },
            {
              "Subrace": "Fire_Godlike",
              "VariationString": "FGD"
            },
            {
              "Subrace": "Nature_Godlike",
              "VariationString": "NGD"
            },
            {
              "Subrace": "Moon_Godlike",
              "VariationString": "MGD"
            },
            {
              "Subrace": "Hearth_Orlan",
              "VariationString": "HOR"
            },
            {
              "Subrace": "Wild_Orlan",
              "VariationString": "HOR"
            },
            {
              "Subrace": "Coastal_Aumaua",
              "VariationString": "CAM"
            },
            {
              "Subrace": "Island_Aumaua",
              "VariationString": "IAM"
            }
          ]
        },
        {
          "$type": "Game.GameData.WeatherSettingsComponent, Assembly-CSharp",
          "WindSettings": {
            "TransitionTime": 4,
            "StrengthSettings": [
              {
                "StrengthType": "Inactive",
                "Strength": 0.25,
                "Turbulence": 0.25,
                "OceanWindSpeed": 6
              },
              {
                "StrengthType": "Light",
                "Strength": 0.5,
                "Turbulence": 0.5,
                "OceanWindSpeed": 6
              },
              {
                "StrengthType": "Moderate",
                "Strength": 1,
                "Turbulence": 1,
                "OceanWindSpeed": 10
              },
              {
                "StrengthType": "Heavy",
                "Strength": 2,
                "Turbulence": 2,
                "OceanWindSpeed": 12
              }
            ]
          },
          "CloudinessSettings": {
            "TransitionTime": 10,
            "StrengthSettings": [
              {
                "StrengthType": "Inactive",
                "Density": 0.44,
                "Intensity": 0,
                "WaterIntensity": 0,
                "EdgeSoftness": 0.1,
                "Speed": 5
              },
              {
                "StrengthType": "Light",
                "Density": 0.45,
                "Intensity": 0.75,
                "WaterIntensity": 0.6,
                "EdgeSoftness": 0.1,
                "Speed": 12
              },
              {
                "StrengthType": "Moderate",
                "Density": 0.55,
                "Intensity": 0.75,
                "WaterIntensity": 0.6,
                "EdgeSoftness": 0.1,
                "Speed": 20
              },
              {
                "StrengthType": "Heavy",
                "Density": 0.7,
                "Intensity": 0.75,
                "WaterIntensity": 0.6,
                "EdgeSoftness": 0.35,
                "Speed": 40
              }
            ]
          },
          "PrecipitationSettings": {
            "RainSettings": {
              "TransitionTime": 5,
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "Strength": 0
                },
                {
                  "StrengthType": "Light",
                  "Strength": 0.2
                },
                {
                  "StrengthType": "Moderate",
                  "Strength": 0.6
                },
                {
                  "StrengthType": "Heavy",
                  "Strength": 0.8
                }
              ],
              "VFXSettings": {
                "LightPrecipitationMinEmissionRate": 3000,
                "LightPrecipitationMaxEmissionRate": 3000,
                "ModeratePrecipitationMinEmissionRate": 12500,
                "ModeratePrecipitationMaxEmissionRate": 12500,
                "HeavyPrecipitationMinEmissionRate": 16000,
                "HeavyPrecipitationMaxEmissionRate": 16000,
                "LightMeshPrecipitationMinEmissionRate": 0,
                "LightMeshPrecipitationMaxEmissionRate": 300,
                "ModerateMeshPrecipitationMinEmissionRate": 0,
                "ModerateMeshPrecipitationMaxEmissionRate": 450,
                "HeavyMeshPrecipitationMinEmissionRate": 0,
                "HeavyMeshPrecipitationMaxEmissionRate": 600
              }
            },
            "HailSettings": {
              "TransitionTime": 10,
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "Strength": 0
                },
                {
                  "StrengthType": "Light",
                  "Strength": 0.2
                },
                {
                  "StrengthType": "Moderate",
                  "Strength": 0.5
                },
                {
                  "StrengthType": "Heavy",
                  "Strength": 0.8
                }
              ],
              "VFXSettings": {
                "LightPrecipitationMinEmissionRate": 0,
                "LightPrecipitationMaxEmissionRate": 0,
                "ModeratePrecipitationMinEmissionRate": 0,
                "ModeratePrecipitationMaxEmissionRate": 0,
                "HeavyPrecipitationMinEmissionRate": 0,
                "HeavyPrecipitationMaxEmissionRate": 0,
                "LightMeshPrecipitationMinEmissionRate": 0,
                "LightMeshPrecipitationMaxEmissionRate": 0,
                "ModerateMeshPrecipitationMinEmissionRate": 0,
                "ModerateMeshPrecipitationMaxEmissionRate": 0,
                "HeavyMeshPrecipitationMinEmissionRate": 0,
                "HeavyMeshPrecipitationMaxEmissionRate": 0
              }
            },
            "SnowSettings": {
              "TransitionTime": 10,
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "Strength": 0
                },
                {
                  "StrengthType": "Light",
                  "Strength": 0.2
                },
                {
                  "StrengthType": "Moderate",
                  "Strength": 0.5
                },
                {
                  "StrengthType": "Heavy",
                  "Strength": 0.8
                }
              ],
              "VFXSettings": {
                "LightPrecipitationMinEmissionRate": 0,
                "LightPrecipitationMaxEmissionRate": 0,
                "ModeratePrecipitationMinEmissionRate": 0,
                "ModeratePrecipitationMaxEmissionRate": 0,
                "HeavyPrecipitationMinEmissionRate": 0,
                "HeavyPrecipitationMaxEmissionRate": 0,
                "LightMeshPrecipitationMinEmissionRate": 0,
                "LightMeshPrecipitationMaxEmissionRate": 0,
                "ModerateMeshPrecipitationMinEmissionRate": 0,
                "ModerateMeshPrecipitationMaxEmissionRate": 0,
                "HeavyMeshPrecipitationMinEmissionRate": 0,
                "HeavyMeshPrecipitationMaxEmissionRate": 0
              }
            }
          },
          "AirConditionsSettings": {
            "DepthFogSettings": {
              "TransitionTime": 10,
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "Strength": 0
                },
                {
                  "StrengthType": "Light",
                  "Strength": 0.33
                },
                {
                  "StrengthType": "Moderate",
                  "Strength": 0.66
                },
                {
                  "StrengthType": "Heavy",
                  "Strength": 1
                }
              ]
            },
            "VolumeFogSettings": {
              "TransitionTime": 1,
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "Strength": 0
                },
                {
                  "StrengthType": "Light",
                  "Strength": 0.33
                },
                {
                  "StrengthType": "Moderate",
                  "Strength": 0.66
                },
                {
                  "StrengthType": "Heavy",
                  "Strength": 1
                }
              ]
            },
            "FogVFXSettings": {
              "DepthFogColor": {
                "A": 1,
                "R": 0.7176471,
                "G": 0.8470588,
                "B": 1
              },
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "MinDepthFogDensity": 0,
                  "DepthFogDensity": 0,
                  "VolumeFogDensity": 0,
                  "VolumeFogWindSpeed": 2
                },
                {
                  "StrengthType": "Light",
                  "MinDepthFogDensity": 0.025,
                  "DepthFogDensity": 0.1,
                  "VolumeFogDensity": 0.4,
                  "VolumeFogWindSpeed": 2
                },
                {
                  "StrengthType": "Moderate",
                  "MinDepthFogDensity": 0.05,
                  "DepthFogDensity": 0.15,
                  "VolumeFogDensity": 0.55,
                  "VolumeFogWindSpeed": 3
                },
                {
                  "StrengthType": "Heavy",
                  "MinDepthFogDensity": 0.1,
                  "DepthFogDensity": 0.3,
                  "VolumeFogDensity": 0.7,
                  "VolumeFogWindSpeed": 4
                }
              ]
            },
            "DustSettings": {
              "TransitionTime": 10,
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "Strength": 0
                },
                {
                  "StrengthType": "Light",
                  "Strength": 0.33
                },
                {
                  "StrengthType": "Moderate",
                  "Strength": 0.66
                },
                {
                  "StrengthType": "Heavy",
                  "Strength": 1
                }
              ]
            },
            "DustVFXSettings": {
              "FogColor": {
                "A": 1,
                "R": 1,
                "G": 0.7568628,
                "B": 0.4039216
              },
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "FogDensity": 0,
                  "FogMinDepthDensity": 0,
                  "FogDepthDensity": 0,
                  "DustAlpha": 0,
                  "DustMeshAlpha": 0,
                  "DustMeshEmissionRate": 150
                },
                {
                  "StrengthType": "Light",
                  "FogDensity": 0,
                  "FogMinDepthDensity": 0,
                  "FogDepthDensity": 0,
                  "DustAlpha": 0,
                  "DustMeshAlpha": 1,
                  "DustMeshEmissionRate": 150
                },
                {
                  "StrengthType": "Moderate",
                  "FogDensity": 3.02,
                  "FogMinDepthDensity": 0.03,
                  "FogDepthDensity": 0.1,
                  "DustAlpha": 0.5,
                  "DustMeshAlpha": 1,
                  "DustMeshEmissionRate": 350
                },
                {
                  "StrengthType": "Heavy",
                  "FogDensity": 3.02,
                  "FogMinDepthDensity": 0.05,
                  "FogDepthDensity": 0.2,
                  "DustAlpha": 1,
                  "DustMeshAlpha": 1,
                  "DustMeshEmissionRate": 350
                }
              ]
            },
            "AshSettings": {
              "TransitionTime": 10,
              "StrengthSettings": [
                {
                  "StrengthType": "Inactive",
                  "Strength": 0
                },
                {
                  "StrengthType": "Light",
                  "Strength": 0.33
                },
                {
                  "StrengthType": "Moderate",
                  "Strength": 0.66
                },
                {
                  "StrengthType": "Heavy",
                  "Strength": 1
                }
              ]
            }
          },
          "LightningSettings": {
            "TransitionTime": 2.5,
            "StrengthSettings": [
              {
                "StrengthType": "Inactive",
                "MinTimeBetween": 100,
                "MaxTimeBetween": 100
              },
              {
                "StrengthType": "Light",
                "MinTimeBetween": 22,
                "MaxTimeBetween": 32
              },
              {
                "StrengthType": "Moderate",
                "MinTimeBetween": 12,
                "MaxTimeBetween": 22
              },
              {
                "StrengthType": "Heavy",
                "MinTimeBetween": 5,
                "MaxTimeBetween": 12
              }
            ],
            "VFXSettings": [
              {
                "StrengthType": "Inactive",
                "ThunderClusterProbability": 0,
                "ThunderClusterMinCount": 1,
                "ThunderClusterMaxCount": 1,
                "ThunderClusterMinTimeBetween": 0.3,
                "ThunderClusterMaxTimeBetween": 0.4,
                "ThunderVsLightningProbability": 0
              },
              {
                "StrengthType": "Light",
                "ThunderClusterProbability": 0.5,
                "ThunderClusterMinCount": 2,
                "ThunderClusterMaxCount": 2,
                "ThunderClusterMinTimeBetween": 0.3,
                "ThunderClusterMaxTimeBetween": 0.4,
                "ThunderVsLightningProbability": 0.8
              },
              {
                "StrengthType": "Moderate",
                "ThunderClusterProbability": 0.75,
                "ThunderClusterMinCount": 2,
                "ThunderClusterMaxCount": 3,
                "ThunderClusterMinTimeBetween": 0.3,
                "ThunderClusterMaxTimeBetween": 0.4,
                "ThunderVsLightningProbability": 0.8
              },
              {
                "StrengthType": "Heavy",
                "ThunderClusterProbability": 1,
                "ThunderClusterMinCount": 2,
                "ThunderClusterMaxCount": 4,
                "ThunderClusterMinTimeBetween": 0.3,
                "ThunderClusterMaxTimeBetween": 0.4,
                "ThunderVsLightningProbability": 0.8
              }
            ]
          },
          "TemperatureSettings": {
            "TransitionTime": 10,
            "TemperatureSettings": [
              {
                "TemperatureType": "Freezing",
                "Temperature": 0
              },
              {
                "TemperatureType": "VeryCold",
                "Temperature": 7
              },
              {
                "TemperatureType": "Cold",
                "Temperature": 14
              },
              {
                "TemperatureType": "Neutral",
                "Temperature": 20
              },
              {
                "TemperatureType": "Warm",
                "Temperature": 25
              },
              {
                "TemperatureType": "Hot",
                "Temperature": 32
              },
              {
                "TemperatureType": "Boiling",
                "Temperature": 40
              }
            ]
          },
          "ColorLevelSettings": {
            "TransitionTime": 2.5
          },
          "AnimationSettings": {
            "ReacterProportion": 0.25,
            "MinimumReactionDelay": 0.2,
            "MaximumReactionDelay": 4
          },
          "AISettings": {
            "SeekCoverMaxDistance": 20,
            "SeekCoverMinDelay": 1,
            "SeekCoverMaxDelay": 10,
            "LeaveCoverMinDelay": 1,
            "LeaveCoverMaxDelay": 10
          }
        },
        {
          "$type": "Game.GameData.AnimationControllerComponent, Assembly-CSharp",
          "RaceOverrides": [
            {
              "Race": "None",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": "art/animation/animationcontrollers/biped/default_othr_m.overridecontroller"
                },
                {
                  "Gender": "Female",
                  "Controller": "art/animation/animationcontrollers/biped/default_othr_f.overridecontroller"
                },
                {
                  "Gender": "Neuter",
                  "Controller": "art/animation/animationcontrollers/biped/default_othr_m.overridecontroller"
                }
              ]
            },
            {
              "Race": "Human",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": "art/animation/animationcontrollers/biped/default_hum_m.overridecontroller"
                },
                {
                  "Gender": "Female",
                  "Controller": "art/animation/animationcontrollers/biped/default_hum_f.overridecontroller"
                },
                {
                  "Gender": "Neuter",
                  "Controller": "art/animation/animationcontrollers/biped/default_hum_m.overridecontroller"
                }
              ]
            },
            {
              "Race": "Elf",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": "art/animation/animationcontrollers/biped/default_elf_m.overridecontroller"
                },
                {
                  "Gender": "Female",
                  "Controller": "art/animation/animationcontrollers/biped/default_elf_f.overridecontroller"
                },
                {
                  "Gender": "Neuter",
                  "Controller": "art/animation/animationcontrollers/biped/default_orl_m.overridecontroller"
                }
              ]
            },
            {
              "Race": "Dwarf",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": "art/animation/animationcontrollers/biped/default_dwa_m.overridecontroller"
                },
                {
                  "Gender": "Female",
                  "Controller": "art/animation/animationcontrollers/biped/default_dwa_f.overridecontroller"
                },
                {
                  "Gender": "Neuter",
                  "Controller": "art/animation/animationcontrollers/biped/default_orl_m.overridecontroller"
                }
              ]
            },
            {
              "Race": "Godlike",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": ""
                },
                {
                  "Gender": "Female",
                  "Controller": ""
                },
                {
                  "Gender": "Neuter",
                  "Controller": ""
                }
              ]
            },
            {
              "Race": "Orlan",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": "art/animation/animationcontrollers/biped/default_orl_m.overridecontroller"
                },
                {
                  "Gender": "Female",
                  "Controller": "art/animation/animationcontrollers/biped/default_orl_f.overridecontroller"
                },
                {
                  "Gender": "Neuter",
                  "Controller": "art/animation/animationcontrollers/biped/default_orl_m.overridecontroller"
                }
              ]
            },
            {
              "Race": "Aumaua",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": "art/animation/animationcontrollers/biped/default_aum_m.overridecontroller"
                },
                {
                  "Gender": "Female",
                  "Controller": "art/animation/animationcontrollers/biped/default_aum_f.overridecontroller"
                },
                {
                  "Gender": "Neuter",
                  "Controller": "art/animation/animationcontrollers/biped/default_aum_m.overridecontroller"
                }
              ]
            },
            {
              "Race": "Beast",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": ""
                },
                {
                  "Gender": "Female",
                  "Controller": ""
                },
                {
                  "Gender": "Neuter",
                  "Controller": ""
                }
              ]
            },
            {
              "Race": "Primordial",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": ""
                },
                {
                  "Gender": "Female",
                  "Controller": ""
                },
                {
                  "Gender": "Neuter",
                  "Controller": ""
                }
              ]
            },
            {
              "Race": "Spirit",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": ""
                },
                {
                  "Gender": "Female",
                  "Controller": ""
                },
                {
                  "Gender": "Neuter",
                  "Controller": ""
                }
              ]
            },
            {
              "Race": "Vessel",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": ""
                },
                {
                  "Gender": "Female",
                  "Controller": ""
                },
                {
                  "Gender": "Neuter",
                  "Controller": ""
                }
              ]
            },
            {
              "Race": "Wilder",
              "GenderOverride": [
                {
                  "Gender": "Male",
                  "Controller": ""
                },
                {
                  "Gender": "Female",
                  "Controller": ""
                },
                {
                  "Gender": "Neuter",
                  "Controller": ""
                }
              ]
            }
          ]
        },
        {
          "$type": "Game.GameData.CombatSettingsComponent, Assembly-CSharp",
          "SneakAttackAfflictionTypesIDs": [
            "912396df-94ce-454f-806e-ad15969a02b1",
            "fda623d6-29a2-4e02-be6a-25c332ff194f",
            "5d1b11b3-9435-4de1-a9dd-f23d85bbe09e",
            "a50f1a60-cf8d-413f-8b11-b973c0e78d6e",
            "5d192433-3732-42fb-8bd2-9043dbf5c4e9",
            "42a17f02-5ade-4826-a41a-09a6042598f8"
          ],
          "PenetrationRatioMultipliers": [
            {
              "Threshold": 0,
              "Value": 1
            },
            {
              "Threshold": 1,
              "Value": 1
            },
            {
              "Threshold": 2,
              "Value": 1.3
            }
          ],
          "FlatPenetrationMultipliers": [
            {
              "Threshold": -99,
              "Value": 0.25
            },
            {
              "Threshold": -2,
              "Value": 0.5
            },
            {
              "Threshold": -1,
              "Value": 0.75
            },
            {
              "Threshold": 0,
              "Value": 1
            }
          ],
          "NoPenetrationNotification": 0.3,
          "GlobalRecoveryMultiplier": 2,
          "DualWeaponRecoveryMultiplier": 0.7,
          "SingleWeaponAccuracyBonus": 12,
          "SwitchWeaponRecoveryTime": 2,
          "InterruptRecoveryTime": 2,
          "MovingRecoverySpeedMultiplier": 0.5,
          "ReengagementWaitPeriod": 2,
          "GrimoireCooldownTime": 2,
          "StoryTimeMinimumRollToCrit": 125,
          "MinimumRollToGraze": 25,
          "MinimumRollToHit": 50,
          "MinimumRollToCrit": 100,
          "GrazeDamageMult": 0.5,
          "GrazeEffectDurationMult": 0.5,
          "CritDamageMult": 1.25,
          "CritEffectDurationMult": 1.25,
          "CritPenetrationMult": 1.5,
          "AccuracyPerLevel": 3,
          "DefensePerLevel": 3,
          "DisengagementAccuracyBonus": 20,
          "DisengagementDamageBonus": 2,
          "TrapBaseAccuracy": 50,
          "TrapAccuracyPerLevel": 3,
          "StatusEffectDefaultPenetration": 9,
          "MaximumInjuriesBeforeDeath": 3,
          "DefaultCharacterLevelScalingRulesID": "311137d5-67c2-40ec-b21a-cf8dbd64ae1f",
          "DefaultSideQuestCharacterLevelScalingRulesID": "29276145-43e8-400c-b9c1-9ea63a0298ce",
          "DefaultNamedCharacterLevelScalingRulesID": "f2af3656-2352-483e-8dac-511ec8061734",
          "DefaultPartySizeLevelScalingRulesID": "a203d7f4-7264-4ec2-9126-721c501b7339",
          "EmpowerLevelBonus": 10,
          "DefaultBlastSizes": [
            {
              "Size": "Small",
              "Radius": 1.5
            },
            {
              "Size": "Medium",
              "Radius": 2.5
            },
            {
              "Size": "Large",
              "Radius": 5
            },
            {
              "Size": "Override",
              "Radius": 0
            }
          ],
          "TrapDefaultDifficulties": [
            {
              "DisarmMechanics": 0,
              "DetectionPerception": 0
            },
            {
              "DisarmMechanics": 1,
              "DetectionPerception": 10
            },
            {
              "DisarmMechanics": 2,
              "DetectionPerception": 11
            },
            {
              "DisarmMechanics": 3,
              "DetectionPerception": 11
            },
            {
              "DisarmMechanics": 4,
              "DetectionPerception": 12
            },
            {
              "DisarmMechanics": 4,
              "DetectionPerception": 12
            },
            {
              "DisarmMechanics": 5,
              "DetectionPerception": 13
            },
            {
              "DisarmMechanics": 5,
              "DetectionPerception": 13
            },
            {
              "DisarmMechanics": 6,
              "DetectionPerception": 14
            },
            {
              "DisarmMechanics": 6,
              "DetectionPerception": 14
            },
            {
              "DisarmMechanics": 7,
              "DetectionPerception": 15
            },
            {
              "DisarmMechanics": 7,
              "DetectionPerception": 15
            },
            {
              "DisarmMechanics": 8,
              "DetectionPerception": 15
            },
            {
              "DisarmMechanics": 8,
              "DetectionPerception": 16
            },
            {
              "DisarmMechanics": 9,
              "DetectionPerception": 16
            },
            {
              "DisarmMechanics": 9,
              "DetectionPerception": 16
            },
            {
              "DisarmMechanics": 10,
              "DetectionPerception": 17
            },
            {
              "DisarmMechanics": 10,
              "DetectionPerception": 17
            },
            {
              "DisarmMechanics": 11,
              "DetectionPerception": 18
            },
            {
              "DisarmMechanics": 11,
              "DetectionPerception": 18
            },
            {
              "DisarmMechanics": 12,
              "DetectionPerception": 19
            }
          ],
          "ReligionPositiveBonuses": [
            "0.2",
            "0.4",
            "0.6"
          ],
          "ReligionNegativeBonuses": [
            "-0.2",
            "-0.4",
            "-0.6"
          ],
          "EmpowerSelfVfx": "prefabs/effects/library/fx_empower_character_hit.prefab",
          "KnockDownDuration": 1,
          "PostCombatRefreshTime": 3,
          "PartyDefogRadius": 14,
          "EnemySightRange": 12,
          "RangedAutoAttackDistance": 2,
          "RemainingRecoveryDodgeTimer": 1,
          "LevelDifficultyIconNeutralSpriteName": "questDifficulty_None",
          "LevelDifficultyIconSettings": [
            {
              "LevelDifference": 1,
              "NumIcons": 1,
              "IconName": "icon_levelScale_1",
              "IconNameHighRes": "questDifficulty_1",
              "QuestTooltipString": 3738,
              "EncounterTooltipString": 3758
            },
            {
              "LevelDifference": 2,
              "NumIcons": 1,
              "IconName": "icon_levelScale_2",
              "IconNameHighRes": "questDifficulty_2",
              "QuestTooltipString": 3739,
              "EncounterTooltipString": 3759
            },
            {
              "LevelDifference": 3,
              "NumIcons": 1,
              "IconName": "icon_levelScale_3",
              "IconNameHighRes": "questDifficulty_3",
              "QuestTooltipString": 3740,
              "EncounterTooltipString": 3760
            },
            {
              "LevelDifference": 4,
              "NumIcons": 1,
              "IconName": "icon_levelScale_4",
              "IconNameHighRes": "questDifficulty_4",
              "QuestTooltipString": 3740,
              "EncounterTooltipString": 3760
            }
          ],
          "CombatMoveSpeedMultiplier": 0.85,
          "StealthLingerDuration": 2
        },
        {
          "$type": "Game.GameData.TownieScheduleSettingsComponent, Assembly-CSharp",
          "MinWaypointsToVisit": 2,
          "MaxWaypointsToVisit": 4,
          "MinHoursPassedRequiredToReset": 1,
          "MinTimeBetweenDespawnAndSpawn": 2,
          "MaxTimeBetweenDespawnAndSpawn": 4,
          "MinTimeBetweenSpawns": 2,
          "MaxTimeBetweenSpawns": 4,
          "OccupiedMeetingPointBias": 1,
          "ShortDistanceBias": 0,
          "MediumDistanceThreshold": 20,
          "MediumDistanceBias": 0.5,
          "LongDistanceThreshold": 40,
          "LongDistanceBias": 0.7,
          "VeryLongDistanceThreshold": 60,
          "VeryLongDistanceBias": 0.9
        },
        {
          "$type": "Game.GameData.NewGameSettingsComponent, Assembly-CSharp",
          "NewGameStartMapID": "d3484f20-80e1-46bf-a8ec-5d7b8ac71892",
          "NewGamePartyID": "e2b099ce-0977-43b6-8e37-2937b5eb0acc",
          "LegacyHistoryScriptedInteraction": {
            "GuidString": "5e945933-8f84-4c2a-a420-70601b94cbe9"
          },
          "BackerBetaStartingMapID": "cbff745d-3053-4536-b591-c114b292d046",
          "BackerBetaPartyID": "9569f98b-c4bd-4835-9cac-afd58a67e541"
        },
        {
          "$type": "Game.GameData.WorldMapSettings, Assembly-CSharp",
          "BaseSailSpeed": 5,
          "BaseWalkSpeed": 2,
          "ShallowWaterSpeedMultiplier": 0.75,
          "GameSecondsPerRealSecond": 3000,
          "CoordinatesBottomLeft": {
            "x": 30,
            "y": -40
          },
          "CoordinatesTopRight": {
            "x": 70,
            "y": 10
          },
          "PlayerFogRevealRadius": {
            "Sailing": 20,
            "Walking": 8
          },
          "PlayerAtLocationFogRevealRadius": 6,
          "RevealedLocationFogRevealRadius": 10,
          "DiscoveryRadius": {
            "Sailing": 20,
            "Walking": 8
          },
          "IdentifyRadius": {
            "Sailing": 20,
            "Walking": 8
          }
        },
        {
          "$type": "Game.GameData.ArtSettingsComponent, Assembly-CSharp",
          "InteriorLightProbeScalar": 3,
          "ExteriorLightProbeScalar": 0.5
        },
        {
          "$type": "Game.GameData.TutorialSettingsComponent, Assembly-CSharp",
          "MinimumTimeBetweenTutorials": 15,
          "TutorialNotificationDisplayDelay": 1,
          "TimeUntilAutoHideTutorialNotification": 10
        }
      ]
    },
    {
      "$type": "Game.GameData.GlobalReferencesGameData, Assembly-CSharp",
      "DebugName": "GlobalReferences",
      "ID": "c8505c60-5bd5-4530-a888-8dac0e998870",
      "Components": [
        {
          "$type": "Game.GameData.GlobalDataReferencesComponent, Assembly-CSharp",
          "SecondWindAbilityID": "b62658d6-ebf3-4bb7-b347-4cd6875bdfa8",
          "EmptyChantAbilityID": "3b70a869-0eab-4e7a-be7d-59b5938a87b6",
          "UnarmedProficiencyAbilityID": "b863a8c3-ed3b-4787-8b3f-2bdea96c47a1",
          "NormalUnarmedAttackID": "c2dc2269-d720-4e95-955b-da4f995f4b25",
          "MonkUnarmedAttackID": "3482a3d8-4258-4d2c-a52b-c57a984e2b6f",
          "FlankedAfflictionID": "fe62d3e8-663b-4a56-afa6-ad39f3807880",
          "BondedGriefAfflictionID": "5a0f8543-0d26-4c4c-97bf-c47715e607b7",
          "ProneAfflictionID": "00000000-0000-0000-0000-000000000000",
          "FatigueAfflictionID": "86ddf8a0-178c-47b4-a800-50a50ed36cb6",
          "MaimedAfflictionID": "995cad4d-33e9-41c0-a4a1-9497bf70864f",
          "SlogZoneAfflictionID": "06ae3f34-ebfb-4cc9-bca0-c488dbe197b4",
          "DamageTypeInjuries": [
            {
              "DamageType": "None",
              "InjuriesIDs": [
                "f5b7bd4f-05e9-42b8-ac75-a01dd7502d11"
              ]
            },
            {
              "DamageType": "Slash",
              "InjuriesIDs": [
                "f5b7bd4f-05e9-42b8-ac75-a01dd7502d11"
              ]
            },
            {
              "DamageType": "Crush",
              "InjuriesIDs": [
                "33e6c366-91d0-40d4-91ae-0c48a2bc2d31",
                "e2e80fcc-a6f8-4aa1-8c86-b07dc7f519fc",
                "747bed56-348d-46d3-a609-98b9cc3c8425"
              ]
            },
            {
              "DamageType": "Pierce",
              "InjuriesIDs": [
                "f5b7bd4f-05e9-42b8-ac75-a01dd7502d11"
              ]
            },
            {
              "DamageType": "Burn",
              "InjuriesIDs": [
                "b4e1b003-f4eb-4aff-ae48-dcd090683abc"
              ]
            },
            {
              "DamageType": "Freeze",
              "InjuriesIDs": [
                "2aaf61a6-b592-4102-bf2f-012a7bb99f21"
              ]
            },
            {
              "DamageType": "Shock",
              "InjuriesIDs": [
                "500ea338-b8ea-40ba-8a6c-d7afdf9c71dc"
              ]
            },
            {
              "DamageType": "Corrode",
              "InjuriesIDs": [
                "89664f93-5899-4d3c-ad4d-f8fd34819c7a",
                "b17f0139-8545-44d2-8201-386c07bcfd1e"
              ]
            },
            {
              "DamageType": "Count",
              "InjuriesIDs": []
            },
            {
              "DamageType": "All",
              "InjuriesIDs": [
                "747bed56-348d-46d3-a609-98b9cc3c8425"
              ]
            },
            {
              "DamageType": "Raw",
              "InjuriesIDs": [
                "f5b7bd4f-05e9-42b8-ac75-a01dd7502d11"
              ]
            }
          ],
          "InjuryKeywordsIDs": [
            "ada7e454-f0de-4465-a352-37b033c9ac9b",
            "a0bc6782-1803-4b9b-9d86-f65dc5f56b36",
            "2c7d2bf7-d470-4d63-912d-1b5cb72ef352",
            "944b94b5-1d69-4374-9bbe-988609ae9ce6"
          ],
          "SpiritshiftKeywordID": "f58f91d7-58af-4dee-855f-6fa8ae8579b7",
          "InvisibleKeywordID": "fe80a668-dcb3-4cea-ae2e-70d47dc91348",
          "ImmunityKeywordID": "43c3b54c-34d8-4625-b6a9-c2312daae5f2",
          "KithGibsID": "39fe91da-247f-411b-ac7f-39660c843302",
          "KithBloodID": "34b79bc2-836a-413b-90a0-6a94f25a3504",
          "DefaultBlood": "",
          "DefaultKeywordEffects": [
            {
              "KeywordID": "f0dbb330-1b06-4175-8ce4-efe790b99213",
              "Effect": "prefabs/effects/hit/elements/fx_hit_fire_01.prefab"
            },
            {
              "KeywordID": "f8a513bb-3e92-4d10-b59f-abbdf71d8af4",
              "Effect": "prefabs/effects/hit/elements/fx_hit_frost_01.prefab"
            },
            {
              "KeywordID": "9c7d6f36-50ff-4b08-a528-74789abe0599",
              "Effect": "prefabs/effects/hit/elements/fx_hit_electricity_01.prefab"
            },
            {
              "KeywordID": "56895011-1aab-4413-827b-51a126c04555",
              "Effect": "prefabs/effects/hit/elements/fx_hit_acid_01.prefab"
            }
          ],
          "NoneSubclassID": "1fe85e8d-d541-44dd-9dbc-fb056810e10e",
          "EmpowerGlossaryEntryID": "0d4f583e-6de6-4917-868c-809481f520be",
          "MoneyGlossaryEntryID": "ea407a0a-14ab-4b1e-9083-efa84bbc5056",
          "SevereInjuryKeywordID": "944b94b5-1d69-4374-9bbe-988609ae9ce6",
          "PlayShipDeepWaterUpgradeID": "97b4f67b-ee56-4301-808b-38ef6ca1c50a",
          "DefaultGhostMaterialReplacementID": "f0e55a4c-b18a-4d1d-9d52-78ca0de0831c",
          "LockpickReferenceID": "a5200562-c6ae-45b6-8d21-a3d331ae6f11",
          "SummonDurationEffectID": "8d57aa71-7db3-4b1b-9187-d38b350e047b",
          "ProfiencyEquippableDisplays": [
            {
              "ProfiencyAbilityID": "a89cc4e8-62aa-4835-b70a-89abe4516810",
              "ItemReferenceID": "f4b5d8f4-8b66-4243-b9f6-1a626f9848fc"
            },
            {
              "ProfiencyAbilityID": "9239d1dc-6521-4a41-974d-c9a065d07986",
              "ItemReferenceID": "fda980ec-4d4c-4a59-b1d9-fc931b1c7a12"
            },
            {
              "ProfiencyAbilityID": "0c606452-4630-4acb-bff2-df6bd8270bf4",
              "ItemReferenceID": "46546968-6fdd-47e7-bdcb-d93a5f4e920d"
            },
            {
              "ProfiencyAbilityID": "d374e404-1dd1-47ae-8934-7995b88a2dc5",
              "ItemReferenceID": "0bc6b44a-df87-4995-af66-196fbfd1d821"
            },
            {
              "ProfiencyAbilityID": "21b6ff8a-21e5-49af-813f-be586fc3f7cf",
              "ItemReferenceID": "2e523de0-b85c-42e2-acd3-644364d2efe9"
            },
            {
              "ProfiencyAbilityID": "d214667e-0eb3-43a1-a972-feeafdb6a0fe",
              "ItemReferenceID": "4a42e444-2b48-43ee-8655-76fe36e89927"
            },
            {
              "ProfiencyAbilityID": "276065a3-ad81-47f0-b6e9-41a326f8f6a3",
              "ItemReferenceID": "982e3d9d-ce45-4796-8c12-b094879dc770"
            },
            {
              "ProfiencyAbilityID": "2e992e9e-7bc5-454b-9e96-78e558b22e34",
              "ItemReferenceID": "9f15c1ec-aa81-4846-82de-165d582bfd57"
            },
            {
              "ProfiencyAbilityID": "b2ac11dc-55b3-4ce5-bf31-77d74b779aae",
              "ItemReferenceID": "b5924a1b-2194-49a2-ac78-300d80dfa179"
            },
            {
              "ProfiencyAbilityID": "7bc9e9ca-4984-4c42-a2ad-9fc28d3f0fed",
              "ItemReferenceID": "58a78533-9026-49c8-9e28-f2668a45c416"
            },
            {
              "ProfiencyAbilityID": "da440737-d8d8-4613-b9d8-ff969898c2d7",
              "ItemReferenceID": "fffe4faa-8aae-4894-add5-2c9672aeadb4"
            },
            {
              "ProfiencyAbilityID": "d75bd92f-b77d-4b48-903e-22ce0fde1d1d",
              "ItemReferenceID": "431a8af0-0137-4605-b1a6-a67d501cc7a3"
            },
            {
              "ProfiencyAbilityID": "0cf0974b-2f76-4924-bb23-bd58fd34df03",
              "ItemReferenceID": "28c3801b-4b79-411c-8d0c-2329d43309fb"
            },
            {
              "ProfiencyAbilityID": "39a4d04a-bc30-4d88-bae9-5d7b026f6112",
              "ItemReferenceID": "c07155e3-cb06-4251-a030-66240b845fdc"
            },
            {
              "ProfiencyAbilityID": "e573cd0c-3999-44a4-8b23-c68ca32c32a6",
              "ItemReferenceID": "c81e92d4-8cdb-4a77-ba1e-f48aaac2faef"
            },
            {
              "ProfiencyAbilityID": "defaf578-53b7-48bf-9292-16c62fa74c59",
              "ItemReferenceID": "471b717a-402c-4005-b664-8873dd348023"
            },
            {
              "ProfiencyAbilityID": "61039541-f6bb-408c-ae9c-9ae39a958878",
              "ItemReferenceID": "f5b35ad9-9d8e-4a1c-b032-22b566cb9da7"
            },
            {
              "ProfiencyAbilityID": "8feb7dae-7725-41dc-8984-fcdc4ec5d71c",
              "ItemReferenceID": "1a3485bc-ec1a-4089-8086-28da996c2aad"
            },
            {
              "ProfiencyAbilityID": "fdb7384e-48a0-4903-8746-5482d876a6d5",
              "ItemReferenceID": "82a6a6cc-3834-493e-99c9-07ad4bdf6210"
            },
            {
              "ProfiencyAbilityID": "ddadfd01-83e7-4716-8061-ae9bdc892993",
              "ItemReferenceID": "2b19eb13-bb2a-4c29-bba7-4dc3258c753d"
            },
            {
              "ProfiencyAbilityID": "8a7403c8-a1a9-48c9-9482-207b74100a71",
              "ItemReferenceID": "a97a376c-8ad8-4fa5-8dbb-12840414b7d1"
            },
            {
              "ProfiencyAbilityID": "109daa74-3db5-4a95-b924-4fc004ab2127",
              "ItemReferenceID": "af824950-db20-421c-a60c-dd1f7c7a6f08"
            },
            {
              "ProfiencyAbilityID": "05b32957-2146-49bf-9963-62867184fddf",
              "ItemReferenceID": "f90e05cf-5d27-4def-8fe3-eb2b6b162658"
            },
            {
              "ProfiencyAbilityID": "977ffd2e-63c6-497d-b817-b3b3d06b19fd",
              "ItemReferenceID": "a7cabebc-a444-45b7-b60b-4927f29ee542"
            },
            {
              "ProfiencyAbilityID": "9e08e06a-a152-4779-af9b-b848f12dd118",
              "ItemReferenceID": "59d27cb7-b780-447f-a803-ea3be9d5b9c8"
            },
            {
              "ProfiencyAbilityID": "7e1da260-1d5d-425c-9a10-71f1f7482b88",
              "ItemReferenceID": "5282f80a-2527-4382-8144-2b80765519bb"
            },
            {
              "ProfiencyAbilityID": "ad86a4e6-d0ec-4486-8443-fbf869dde9f1",
              "ItemReferenceID": "3530e7a2-ec4f-4d5f-a436-53e26ed3e075"
            },
            {
              "ProfiencyAbilityID": "95fe0d61-105f-4d88-9592-14b51517cf0a",
              "ItemReferenceID": "f1c45417-0b08-4d82-8b71-01b5b2293fec"
            },
            {
              "ProfiencyAbilityID": "c2fb8b3b-8dd4-4f57-bf71-63b285bc8d12",
              "ItemReferenceID": "605ef110-e7ec-4095-a2ff-28e4e7ebce9d"
            },
            {
              "ProfiencyAbilityID": "0b63cb38-ef14-4504-b3dc-0e589c3db2f5",
              "ItemReferenceID": "3213c60a-3fa4-48a0-8742-815ed9427b5f"
            },
            {
              "ProfiencyAbilityID": "0ed6e0ae-afea-4e25-8fb1-b28c7c35c602",
              "ItemReferenceID": "ad031f74-7dab-44c3-ad7b-78382463a07f"
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ProgressionTableManagerGameData, Assembly-CSharp",
      "DebugName": "ProgressionTableManager",
      "ID": "2be32291-5324-4410-9e26-a67d2d20b716",
      "Components": [
        {
          "$type": "Game.GameData.ProgressionTableManagerComponent, Assembly-CSharp",
          "RacialProgressionTableID": "b2afaada-c140-42c3-9f02-2f919e45b49d",
          "ProficienciesProgressionTableID": "a01f1324-4484-4548-a2b3-602a28dca631",
          "ClassTables": [
            {
              "CharacterClass": "None",
              "TableID": "00000000-0000-0000-0000-000000000000"
            },
            {
              "CharacterClass": "Fighter",
              "TableID": "dcc7b7db-c31b-4075-869c-bd660bfe4ee2"
            },
            {
              "CharacterClass": "Rogue",
              "TableID": "958174ed-bef9-4d9b-9bc0-25bef1864623"
            },
            {
              "CharacterClass": "Priest",
              "TableID": "a52e8b61-9343-4716-8a55-3168be143cc4"
            },
            {
              "CharacterClass": "Wizard",
              "TableID": "8b5635bf-28b1-4836-9712-66c99553ccf2"
            },
            {
              "CharacterClass": "Barbarian",
              "TableID": "67621a56-397e-4fae-8e04-59890b25c7fc"
            },
            {
              "CharacterClass": "Ranger",
              "TableID": "d574de6c-f4cb-4948-9a71-fb372700e6a5"
            },
            {
              "CharacterClass": "Druid",
              "TableID": "879d3662-bdf7-4585-99e4-0af2df117c50"
            },
            {
              "CharacterClass": "Paladin",
              "TableID": "7af80bf4-f8fc-48ff-b02e-884b2e7b12f9"
            },
            {
              "CharacterClass": "Monk",
              "TableID": "dc561d91-7bc9-4314-acff-837511566964"
            },
            {
              "CharacterClass": "Cipher",
              "TableID": "12ba861c-8f31-45af-869f-e02a9a677484"
            },
            {
              "CharacterClass": "Chanter",
              "TableID": "6b722b30-d930-43ae-b936-248a1616f48e"
            }
          ],
          "CharacterCreationAttributePoints": 15,
          "CharacterCreationMaxAttributeGulf": 14,
          "CharacterCreationAttributeMinimum": 3,
          "CharacterCreationAttributeMaximum": 18,
          "CharacterCreationBaseStat": 10
        }
      ]
    },
    {
      "$type": "Game.GameData.QuestExperienceTableGameData, Assembly-CSharp",
      "DebugName": "QuestExperienceTable",
      "ID": "3e7b25d6-b7ea-476a-8efd-31dc9f9d9465",
      "Components": [
        {
          "$type": "Game.GameData.QuestExperienceTableComponent, Assembly-CSharp",
          "QuestData": [
            {
              "Level": 1,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Stranded - CP",
                  "QuestReference": {
                    "GuidString": "b34631e8-90f0-49f7-8733-0dc4728894b3"
                  },
                  "ExperienceAmount": 750,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 3,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "To Hunt a God - CP",
                  "QuestReference": {
                    "GuidString": "7fa7fc66-e861-4b33-9e64-6d159ea7d0fe"
                  },
                  "ExperienceAmount": 1500,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 4,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Veins of Eora - CP",
                  "QuestReference": {
                    "GuidString": "7cca2e3f-aab4-4164-8df9-0dd2bfcac3e5"
                  },
                  "ExperienceAmount": 2500,
                  "OriginRegion": "Serpents_Crown"
                }
              ]
            },
            {
              "Level": 0,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "From the Wreckage - CP",
                  "QuestReference": {
                    "GuidString": "66d2446d-db36-49df-b817-1de92351f013"
                  },
                  "ExperienceAmount": 3000,
                  "OriginRegion": "Serpents_Crown"
                }
              ]
            },
            {
              "Level": 8,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Distant Light - CP",
                  "QuestReference": {
                    "GuidString": "8ce05a89-5396-4132-a147-7a5aa4df55c2"
                  },
                  "ExperienceAmount": 4000,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "He Waits in Fire - CP",
                  "QuestReference": {
                    "GuidString": "501e4c0d-590b-421e-bae2-e8e281ab32d8"
                  },
                  "ExperienceAmount": 5000,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 15,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Coming Storm - CP",
                  "QuestReference": {
                    "GuidString": "980a5414-ce0e-43c9-a5e7-b2f209f035a3"
                  },
                  "ExperienceAmount": 8000,
                  "OriginRegion": "Serpents_Crown"
                }
              ]
            },
            {
              "Level": 16,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The City Lost to Time",
                  "QuestReference": {
                    "GuidString": "72910fdc-e97b-4cc2-b76c-ea3311d68d4f"
                  },
                  "ExperienceAmount": 9000,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Blow the Man Down - Principi",
                  "QuestReference": {
                    "GuidString": "b62bd415-5f2d-49e8-9261-6ca74b0b440a"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 7,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Goods and Services - Principi",
                  "QuestReference": {
                    "GuidString": "c4d6ac5e-ef99-441f-8e2c-e2c99d3fcf30"
                  },
                  "ExperienceAmount": 1500,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 11,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Symbols of Death - Principi",
                  "QuestReference": {
                    "GuidString": "25f42850-b2f2-49fc-8d8b-2ea35b4bfe7c"
                  },
                  "ExperienceAmount": 2000,
                  "OriginRegion": "The_Sacred_Stair"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Shrewd Proposition - Principi",
                  "QuestReference": {
                    "GuidString": "ca999a50-e900-4968-a695-f30c8df8599b"
                  },
                  "ExperienceAmount": 2500,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 14,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Honor Among Thieves - Principi",
                  "QuestReference": {
                    "GuidString": "d540ebd1-abbe-4314-a0e3-5ffacb24af42"
                  },
                  "ExperienceAmount": 3000,
                  "OriginRegion": "Dunnage"
                }
              ]
            },
            {
              "Level": 15,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Dance with Death - Principi",
                  "QuestReference": {
                    "GuidString": "70525acb-34f6-4a38-97f6-792591118dfe"
                  },
                  "ExperienceAmount": 4500,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 2,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Meet the Royal Deadfire Company - RDC",
                  "QuestReference": {
                    "GuidString": "1f4138c7-51a6-406e-81cc-2b6725ae72be"
                  },
                  "ExperienceAmount": 500,
                  "OriginRegion": "The_Brass_Citadel"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Dim Prospects",
                  "QuestReference": {
                    "GuidString": "f1f35f1f-3373-4fe8-b064-3c079a8ba859"
                  },
                  "ExperienceAmount": 200,
                  "OriginRegion": "Poko_Kohara"
                }
              ]
            },
            {
              "Level": 8,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Matter of Import - RDC",
                  "QuestReference": {
                    "GuidString": "8a1c2a0e-d242-4b6d-89be-6267e0d1c9d3"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "Poko_Kohara"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Overgrowth - RDC",
                  "QuestReference": {
                    "GuidString": "11dd2414-37c0-4ef2-82ac-ed48b550f3ef"
                  },
                  "ExperienceAmount": 1500,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Clearing Out Crookspur - RDC",
                  "QuestReference": {
                    "GuidString": "d94e8496-aed3-4122-be23-0dd52dfe17f8"
                  },
                  "ExperienceAmount": 2000,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 14,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Final Maneuver - RDC",
                  "QuestReference": {
                    "GuidString": "65e6943f-c35c-4895-b8e4-e2bf1c9a2600"
                  },
                  "ExperienceAmount": 6500,
                  "OriginRegion": "Serpents_Crown"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Trade Secrets - Huana",
                  "QuestReference": {
                    "GuidString": "a1f8f22d-5468-4753-8877-e627ac0d1023"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "The_Gullet"
                }
              ]
            },
            {
              "Level": 8,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Last Sanctuary - Huana",
                  "QuestReference": {
                    "GuidString": "ba656a38-983f-4f8b-a4e2-f54f765933fb"
                  },
                  "ExperienceAmount": 1500,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Shadow Under Neketaka - Huana",
                  "QuestReference": {
                    "GuidString": "c99e709a-aa07-44de-8844-4c3617dad7f2"
                  },
                  "ExperienceAmount": 1800,
                  "OriginRegion": "Perikis_Overlook"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Fruitful Alliance - Huana",
                  "QuestReference": {
                    "GuidString": "3a3fc055-f424-4e13-9486-8984d5db4553"
                  },
                  "ExperienceAmount": 2000,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 14,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Taking out the Traders - Huana",
                  "QuestReference": {
                    "GuidString": "189a5426-1670-42ca-aff2-dd7a0d795f1e"
                  },
                  "ExperienceAmount": 6000,
                  "OriginRegion": "The_Brass_Citadel"
                }
              ]
            },
            {
              "Level": 2,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Meet the Valian Trading Company - VTC",
                  "QuestReference": {
                    "GuidString": "0f78eb02-9931-4418-9c70-c34b7f113fba"
                  },
                  "ExperienceAmount": 500,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Terms of Trade",
                  "QuestReference": {
                    "GuidString": "c0f05e98-ced9-4245-ad8f-ba4552eb81bc"
                  },
                  "ExperienceAmount": 200,
                  "OriginRegion": "Poko_Kohara"
                }
              ]
            },
            {
              "Level": 8,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Glimpse Beyond - VTC",
                  "QuestReference": {
                    "GuidString": "a1d760f7-59d9-493d-806c-45b8a5dbbb1b"
                  },
                  "ExperienceAmount": 1500,
                  "OriginRegion": "The_Sacred_Stair"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Dirty Laundry - VTC",
                  "QuestReference": {
                    "GuidString": "b948483e-6126-42fb-95f2-aab72bdfa45c"
                  },
                  "ExperienceAmount": 1500,
                  "OriginRegion": "Perikis_Overlook"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Of Like Minds - VTC",
                  "QuestReference": {
                    "GuidString": "de1c9fd3-e72a-476b-b913-f61d1c45c519"
                  },
                  "ExperienceAmount": 2000,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 13,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Skipping Ahead - VTC",
                  "QuestReference": {
                    "GuidString": "d242649f-0b1d-4e43-ba91-9381975ac1f6"
                  },
                  "ExperienceAmount": 2000,
                  "OriginRegion": "The_Sacred_Stair"
                }
              ]
            },
            {
              "Level": 13,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Vote of No Confidence - VTC",
                  "QuestReference": {
                    "GuidString": "9551e62b-fd51-4e1b-bdfa-bc54806a6cad"
                  },
                  "ExperienceAmount": 2500,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 14,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Sabotage at the Brass Citadel - VTC",
                  "QuestReference": {
                    "GuidString": "b26cb21d-4567-4610-a02b-7005d9b5c18b"
                  },
                  "ExperienceAmount": 3000,
                  "OriginRegion": "The_Brass_Citadel"
                }
              ]
            },
            {
              "Level": 1,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Helping Hands - Q",
                  "QuestReference": {
                    "GuidString": "a4ee8842-51bd-45a1-9b31-ec85a6377a85"
                  },
                  "ExperienceAmount": 550,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 1,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Drunk Sailor - T",
                  "QuestReference": {
                    "GuidString": "71227793-3fea-47ad-b8ad-648c6bf3f15f"
                  },
                  "ExperienceAmount": 185,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 1,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Basic Provisions - T",
                  "QuestReference": {
                    "GuidString": "a4c54a13-31d7-4286-a7bd-beb9d8029ce5"
                  },
                  "ExperienceAmount": 150,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 2,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Burning Bridges - Q",
                  "QuestReference": {
                    "GuidString": "754423a2-3a1d-42ec-af30-2aac66c4c42e"
                  },
                  "ExperienceAmount": 800,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 2,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Better Man - T",
                  "QuestReference": {
                    "GuidString": "fcc9b50b-ce87-4606-9179-0be878e8fe9e"
                  },
                  "ExperienceAmount": 260,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 2,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Restoring Order - T",
                  "QuestReference": {
                    "GuidString": "4a138edb-7601-4e40-a4b1-66acdd117ec0"
                  },
                  "ExperienceAmount": 350,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 3,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Skeleton Crew - T",
                  "QuestReference": {
                    "GuidString": "e97ceb7b-764d-462c-a5a6-25e51def2cd4"
                  },
                  "ExperienceAmount": 300,
                  "OriginRegion": "Port_Maje"
                }
              ]
            },
            {
              "Level": 4,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Lost Dues in Good Faith - Q",
                  "QuestReference": {
                    "GuidString": "22b8fc97-d68b-4fd6-b93e-65d7f0c9a621"
                  },
                  "ExperienceAmount": 800,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 4,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Shipwright's Plight - T",
                  "QuestReference": {
                    "GuidString": "1a1b2bed-2d5e-4fa5-922b-9b6dbdb3ff76"
                  },
                  "ExperienceAmount": 350,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 4,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Sealed Fate - T",
                  "QuestReference": {
                    "GuidString": "319a713b-b40d-4a59-a3b8-7e7c6c826317"
                  },
                  "ExperienceAmount": 350,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Delver's Row - Q",
                  "QuestReference": {
                    "GuidString": "b8fe43fe-5223-4ddc-a950-70efa0372793"
                  },
                  "ExperienceAmount": 750,
                  "OriginRegion": "The_Gullet"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Salt in the Wound - T",
                  "QuestReference": {
                    "GuidString": "cc1af44d-f832-440c-a469-09a1690e6eb0"
                  },
                  "ExperienceAmount": 400,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Coming to Terms - T",
                  "QuestReference": {
                    "GuidString": "96fc7932-d113-4f61-95ed-3a1dab196fc5"
                  },
                  "ExperienceAmount": 400,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Her Last Request - T",
                  "QuestReference": {
                    "GuidString": "6fb2c9fb-694d-40f9-b51b-fba5b8bf0c20"
                  },
                  "ExperienceAmount": 400,
                  "OriginRegion": "The_Sacred_Stair"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "All Aboard",
                  "QuestReference": {
                    "GuidString": "8b7cd3db-f9d7-4504-99e5-11c5df411af5"
                  },
                  "ExperienceAmount": 400,
                  "OriginRegion": "The_Gullet"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Storms of Poko Kohara",
                  "QuestReference": {
                    "GuidString": "9ac3a288-fcca-48b8-9fbd-6008136a4659"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "Deadfire_Southeast"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Harsh Medicine - Q",
                  "QuestReference": {
                    "GuidString": "0c97f630-fe71-4c02-8810-5ec5351ab608"
                  },
                  "ExperienceAmount": 650,
                  "OriginRegion": "The_Gullet"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Sinking Feeling - T",
                  "QuestReference": {
                    "GuidString": "2e2aa3a5-dbd1-4075-b13c-4c2c66d188e3"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Bigger Fish - T",
                  "QuestReference": {
                    "GuidString": "1589572e-87ff-49e9-891e-b29e8f12ef03"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Plucked Fruit - T",
                  "QuestReference": {
                    "GuidString": "75309417-75a6-49d3-814d-d669b1cc0c49"
                  },
                  "ExperienceAmount": 475,
                  "OriginRegion": "Tikawara"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Cordial Invitation",
                  "QuestReference": {
                    "GuidString": "7918b328-18e4-46df-8071-284ad4bda54d"
                  },
                  "ExperienceAmount": 250,
                  "OriginRegion": "Perikis_Overlook"
                }
              ]
            },
            {
              "Level": 7,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Food for Thought - Q",
                  "QuestReference": {
                    "GuidString": "3c6f825e-04e7-4491-8639-9d4616141224"
                  },
                  "ExperienceAmount": 800,
                  "OriginRegion": "The_Gullet"
                }
              ]
            },
            {
              "Level": 7,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Archmage's Vault - Q",
                  "QuestReference": {
                    "GuidString": "553369bf-7bb6-4702-ad78-3c7ac8423772"
                  },
                  "ExperienceAmount": 1800,
                  "OriginRegion": "Perikis_Overlook"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Tidy Performance - T",
                  "QuestReference": {
                    "GuidString": "7ee8ee96-5d5b-44b3-87b5-9de28482f234"
                  },
                  "ExperienceAmount": 500,
                  "OriginRegion": "Dunnage"
                }
              ]
            },
            {
              "Level": 7,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Wizard's Apprentice - T",
                  "QuestReference": {
                    "GuidString": "c1a28442-3966-4a05-a807-d5683dce58de"
                  },
                  "ExperienceAmount": 500,
                  "OriginRegion": "The_Brass_Citadel"
                }
              ]
            },
            {
              "Level": 8,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Family Pride - Q",
                  "QuestReference": {
                    "GuidString": "8ba2af7c-1aa4-4af7-a2a1-2654a17bbcec"
                  },
                  "ExperienceAmount": 900,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 8,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Coronet's Call",
                  "QuestReference": {
                    "GuidString": "5827ef09-f91e-432d-a8c0-f55e3c981417"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "The_Gullet"
                }
              ]
            },
            {
              "Level": 9,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Broodmother's Fury - Q",
                  "QuestReference": {
                    "GuidString": "f48f89ea-2c86-4779-ada2-26d05ac352d2"
                  },
                  "ExperienceAmount": 900,
                  "OriginRegion": "Tikawara"
                }
              ]
            },
            {
              "Level": 1,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Iron Gut - Task",
                  "QuestReference": {
                    "GuidString": "2070c606-2be2-4408-975d-8047864ebfd1"
                  },
                  "ExperienceAmount": 700,
                  "OriginRegion": "Dunnage"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Hunting Season - Q",
                  "QuestReference": {
                    "GuidString": "22f7cfd1-6756-4e7d-973d-6c772a7d778c"
                  },
                  "ExperienceAmount": 1100,
                  "OriginRegion": "Sayuka"
                }
              ]
            },
            {
              "Level": 11,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Eulogy of the Dead",
                  "QuestReference": {
                    "GuidString": "e9464382-9172-4039-9231-d4d6c6f64b98"
                  },
                  "ExperienceAmount": 1500,
                  "OriginRegion": "The_Sacred_Stair"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Wahaki Must Die - Q",
                  "QuestReference": {
                    "GuidString": "aebe4b63-70bc-4189-8f46-736b24c7243a"
                  },
                  "ExperienceAmount": 1800,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Cruel Cargo - Q",
                  "QuestReference": {
                    "GuidString": "e284b80c-96d8-4286-9fca-a1f300ebd956"
                  },
                  "ExperienceAmount": 1800,
                  "OriginRegion": "Crookspur_Island"
                }
              ]
            },
            {
              "Level": 15,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Berkano's Folly - Q",
                  "QuestReference": {
                    "GuidString": "5663e9a5-ce8c-49ec-bef9-f826133ddda8"
                  },
                  "ExperienceAmount": 2500,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 15,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Merry Dead - Q",
                  "QuestReference": {
                    "GuidString": "6d0c7bf7-fe19-4a04-ba51-a6948572e2f8"
                  },
                  "ExperienceAmount": 2500,
                  "OriginRegion": "Deadfire_Southeast"
                }
              ]
            },
            {
              "Level": 18,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Nemnok the Devourer - Q",
                  "QuestReference": {
                    "GuidString": "e2de3f1d-7545-4180-864d-fbbacba91767"
                  },
                  "ExperienceAmount": 5000,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 18,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Lost Grimoires - Q",
                  "QuestReference": {
                    "GuidString": "8ebd52f0-f012-4b36-bf4a-58429f55b22d"
                  },
                  "ExperienceAmount": 5000,
                  "OriginRegion": "Unknown"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Lantern of Gaun - CQ",
                  "QuestReference": {
                    "GuidString": "d1dfb1c7-ceb7-4db2-afb6-35d111aaed6b"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "Unknown"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Rautaian Candidate - CQ",
                  "QuestReference": {
                    "GuidString": "2dae0308-483d-4ac5-afe7-03b421651d8e"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "The_Brass_Citadel"
                }
              ]
            },
            {
              "Level": 9,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "A Sorcerer and a Gentleman - CQ",
                  "QuestReference": {
                    "GuidString": "e012a9bc-a820-4731-8dbe-46e5a51b9d0d"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "Unknown"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "The Painted Masks - CQ",
                  "QuestReference": {
                    "GuidString": "ee657718-31ec-4140-945f-2f2488af7503"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "Unknown"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Diccila",
                  "QuestReference": {
                    "GuidString": "0aee204d-52bb-44fa-8712-5bf0522e25dd"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "None"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Meryel the Mad",
                  "QuestReference": {
                    "GuidString": "500f28c6-f741-4d2e-80ff-08bd1c8f3e3b"
                  },
                  "ExperienceAmount": 400,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 4,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Chornu",
                  "QuestReference": {
                    "GuidString": "c0738a35-5756-43e2-b8ad-04f2465c0693"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Purakau",
                  "QuestReference": {
                    "GuidString": "381cb6a2-b086-4178-ab55-a5857a9949b8"
                  },
                  "ExperienceAmount": 425,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Assila Wave Skipper",
                  "QuestReference": {
                    "GuidString": "a4933927-8f49-4631-9eaa-2b7496d2381e"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 6,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Nomu the Marauder",
                  "QuestReference": {
                    "GuidString": "b60bafbd-35fc-4008-b731-8ad9125d714a"
                  },
                  "ExperienceAmount": 425,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 7,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Biakara",
                  "QuestReference": {
                    "GuidString": "e9f88ae7-1f1b-497a-8130-7695c0ae06b0"
                  },
                  "ExperienceAmount": 650,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 7,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Tahae the Troll",
                  "QuestReference": {
                    "GuidString": "81312f76-ad17-47f3-9fb2-3344b4347820"
                  },
                  "ExperienceAmount": 425,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 8,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Lord Admiral Imp",
                  "QuestReference": {
                    "GuidString": "54864f82-a489-4a1e-a5f5-8ab28a28caca"
                  },
                  "ExperienceAmount": 450,
                  "OriginRegion": "The_Queens_Berth"
                }
              ]
            },
            {
              "Level": 5,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Veen",
                  "QuestReference": {
                    "GuidString": "3ba1dd35-4057-4d23-9a2f-378221c66255"
                  },
                  "ExperienceAmount": 750,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 9,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Radulf",
                  "QuestReference": {
                    "GuidString": "4a7c2c83-536f-49df-b213-c6e30220d5d9"
                  },
                  "ExperienceAmount": 750,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 9,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Oheiro",
                  "QuestReference": {
                    "GuidString": "ba9b0712-f606-4577-ac98-0fdcdc37d266"
                  },
                  "ExperienceAmount": 750,
                  "OriginRegion": "Deadfire_Southeast"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Dhwrgas the Ascetic",
                  "QuestReference": {
                    "GuidString": "c51f348d-73f0-4032-85c9-a84f5e86ae0e"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Kuaro",
                  "QuestReference": {
                    "GuidString": "98a7e516-26de-4f16-af14-d3427ff82a54"
                  },
                  "ExperienceAmount": 900,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Lady Epero",
                  "QuestReference": {
                    "GuidString": "e1a2b8dc-5bc7-41fd-bcbd-4d08ad65b514"
                  },
                  "ExperienceAmount": 600,
                  "OriginRegion": "Serpents_Crown"
                }
              ]
            },
            {
              "Level": 11,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Rafiq the Redbeard",
                  "QuestReference": {
                    "GuidString": "807afb0e-37ca-45f9-b9bc-6cf1d6a50c23"
                  },
                  "ExperienceAmount": 1200,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 11,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Onadere the Siren",
                  "QuestReference": {
                    "GuidString": "bceb9474-21e0-47f6-bd93-8ba378baee96"
                  },
                  "ExperienceAmount": 800,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Ikorno the Battlemage",
                  "QuestReference": {
                    "GuidString": "d3062c66-c3c5-4e87-93a0-a9bd7b81029d"
                  },
                  "ExperienceAmount": 900,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 12,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Katrenn the Wizard",
                  "QuestReference": {
                    "GuidString": "e0d37395-52ef-491b-928c-b29241dd07bb"
                  },
                  "ExperienceAmount": 900,
                  "OriginRegion": "The_Sacred_Stair"
                }
              ]
            },
            {
              "Level": 13,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Captain Burunga",
                  "QuestReference": {
                    "GuidString": "20bef0ed-96dd-4171-9512-023eca61bc66"
                  },
                  "ExperienceAmount": 2250,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 13,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Jarlseggr the Rathun",
                  "QuestReference": {
                    "GuidString": "334b5ad4-1774-4588-85cc-9913a28b7d44"
                  },
                  "ExperienceAmount": 2250,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 13,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Beina the Mystic",
                  "QuestReference": {
                    "GuidString": "50fca16f-db33-404a-a6ee-e9e30212b798"
                  },
                  "ExperienceAmount": 2250,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            },
            {
              "Level": 14,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Rock Stomper Rodul",
                  "QuestReference": {
                    "GuidString": "afe91b67-54f0-4c77-b607-1ab9504ff9de"
                  },
                  "ExperienceAmount": 1800,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 14,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Torkar the Tactician",
                  "QuestReference": {
                    "GuidString": "b4813169-fd74-4073-9da2-7a8aff257d9a"
                  },
                  "ExperienceAmount": 1200,
                  "OriginRegion": "Dunnage"
                }
              ]
            },
            {
              "Level": 15,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Vessali the Snake",
                  "QuestReference": {
                    "GuidString": "07ee3f5c-fa44-4eaa-bc51-86cbb305d37d"
                  },
                  "ExperienceAmount": 1400,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 16,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - The Steel Preacher",
                  "QuestReference": {
                    "GuidString": "8a8d09a7-5759-49a1-97e4-14f438f3cda3"
                  },
                  "ExperienceAmount": 2500,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 0,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": []
            },
            {
              "Level": 16,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Bounty - Uamoru the Pretender",
                  "QuestReference": {
                    "GuidString": "beff3450-41da-4c2a-9301-5e1b25b5804a"
                  },
                  "ExperienceAmount": 2500,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 7,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Mapping the Archipelago: Port Maje",
                  "QuestReference": {
                    "GuidString": "548e0b25-4f52-4398-b1d2-ac3b2831c9ae"
                  },
                  "ExperienceAmount": 1000,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 10,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Mapping the Archipelago: Tikawara",
                  "QuestReference": {
                    "GuidString": "f19fb82a-567a-4de6-acb4-3695850ed4e0"
                  },
                  "ExperienceAmount": 2000,
                  "OriginRegion": "Deadfire_Southeast"
                }
              ]
            },
            {
              "Level": 13,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Mapping the Archipelago: West Wakara Reef",
                  "QuestReference": {
                    "GuidString": "1ddcdacb-f0a3-4db6-b642-dad98061b752"
                  },
                  "ExperienceAmount": 3000,
                  "OriginRegion": "Deadfire_Southwest"
                }
              ]
            },
            {
              "Level": 15,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Mapping the Archipelago: The Burning Shoals",
                  "QuestReference": {
                    "GuidString": "6a28517a-af5c-4347-a3b8-461da68966bd"
                  },
                  "ExperienceAmount": 4000,
                  "OriginRegion": "Deadfire_Northeast"
                }
              ]
            },
            {
              "Level": 18,
              "ExperienceMultiplier": 1,
              "QuestsAndExperience": [
                {
                  "Tag": "Mapping the Archipelago: Razai Passage",
                  "QuestReference": {
                    "GuidString": "f730f90f-7733-4c21-8a17-121387773081"
                  },
                  "ExperienceAmount": 5000,
                  "OriginRegion": "Deadfire_Northwest"
                }
              ]
            }
          ],
          "Strings": [
            {
              "QuestRegion": "Port_Maje",
              "DisplayNameString": 4585
            },
            {
              "QuestRegion": "Serpents_Crown",
              "DisplayNameString": 4586
            },
            {
              "QuestRegion": "The_Gullet",
              "DisplayNameString": 4587
            },
            {
              "QuestRegion": "The_Sacred_Stair",
              "DisplayNameString": 4590
            },
            {
              "QuestRegion": "Perikis_Overlook",
              "DisplayNameString": 4591
            },
            {
              "QuestRegion": "The_Queens_Berth",
              "DisplayNameString": 4592
            },
            {
              "QuestRegion": "The_Brass_Citadel",
              "DisplayNameString": 4593
            },
            {
              "QuestRegion": "Dunnage",
              "DisplayNameString": 4594
            },
            {
              "QuestRegion": "Sayuka",
              "DisplayNameString": 4595
            },
            {
              "QuestRegion": "Tikawara",
              "DisplayNameString": 4596
            },
            {
              "QuestRegion": "Deadfire_Southwest",
              "DisplayNameString": 4597
            },
            {
              "QuestRegion": "Deadfire_Southeast",
              "DisplayNameString": 4598
            },
            {
              "QuestRegion": "Deadfire_Northwest",
              "DisplayNameString": 4599
            },
            {
              "QuestRegion": "Deadfire_Northeast",
              "DisplayNameString": 4600
            },
            {
              "QuestRegion": "Poko_Kohara",
              "DisplayNameString": 4610
            }
          ]
        }
      ]
    },
    {
      "$type": "Game.GameData.ShipDuelSettingsGameData, Assembly-CSharp",
      "DebugName": "ShipDuelSettings",
      "ID": "3e3d9dd2-be09-4fcb-b96e-75e851797208",
      "Components": [
        {
          "$type": "Game.GameData.ShipDuelSettings, Assembly-CSharp",
          "RakingFireDefenseBonus": 10,
          "TurnDefenseBonus": 5,
          "MovementDefenseBonusMultiplier": 0.2,
          "CannoneerHitChancePerLevel": 10,
          "IncompententHelmsmanAccuracyBonus": -10,
          "JibAccuracyBonus": -30,
          "CannonTooClosePenalty": -30,
          "CannonTooFarPenalty": -30,
          "CannonMaxRangeBonus": 30,
          "CannonHitThreshold": 100,
          "AdvantageMovementBonus": 20,
          "IncompententDeckhandsMovementMultiplier": 0.5,
          "GlobalAIWeights": {
            "SelfHullDamageWeight": 1,
            "SelfSailDamageWeight": 0.5,
            "SelfDeathWeight": 4,
            "SelfEventWeight": 1,
            "OpponentHullDamageWeight": 1,
            "OpponentSailDamageWeight": 0.5,
            "OpponentDeathWeight": 4,
            "BoardingWeight": 1,
            "RetreatWeight": 1,
            "RetreatSkew": -0.2
          },
          "RetreatDistance": 600,
          "HoldAccuracyBonus": 20,
          "RammeeDamage": {
            "DamageMin": 8,
            "DamageMax": 10,
            "CrewDamageMin": 1,
            "CrewDamageMax": 1,
            "DamageTypes": [
              {
                "DamageType": "Hull",
                "Weight": 1
              },
              {
                "DamageType": "Sail",
                "Weight": 0
              },
              {
                "DamageType": "AnyCrew",
                "Weight": 0
              },
              {
                "DamageType": "AboveDeckCrew",
                "Weight": 0
              },
              {
                "DamageType": "BelowDeckCrew",
                "Weight": 0
              }
            ]
          },
          "RammerDamage": {
            "DamageMin": 4,
            "DamageMax": 6,
            "CrewDamageMin": 1,
            "CrewDamageMax": 1,
            "DamageTypes": [
              {
                "DamageType": "Hull",
                "Weight": 1
              },
              {
                "DamageType": "Sail",
                "Weight": 0
              },
              {
                "DamageType": "AnyCrew",
                "Weight": 0
              },
              {
                "DamageType": "AboveDeckCrew",
                "Weight": 0
              },
              {
                "DamageType": "BelowDeckCrew",
                "Weight": 0
              }
            ]
          },
          "CloseToBoardDamage": [
            {
              "DamageMin": 25,
              "DamageMax": 35,
              "CrewDamageMin": 1,
              "CrewDamageMax": 1,
              "DamageTypes": [
                {
                  "DamageType": "Hull",
                  "Weight": 1
                },
                {
                  "DamageType": "Sail",
                  "Weight": 0
                },
                {
                  "DamageType": "AnyCrew",
                  "Weight": 0
                },
                {
                  "DamageType": "AboveDeckCrew",
                  "Weight": 0
                },
                {
                  "DamageType": "BelowDeckCrew",
                  "Weight": 0
                }
              ]
            },
            {
              "DamageMin": 10,
              "DamageMax": 20,
              "CrewDamageMin": 1,
              "CrewDamageMax": 1,
              "DamageTypes": [
                {
                  "DamageType": "Hull",
                  "Weight": 0
                },
                {
                  "DamageType": "Sail",
                  "Weight": 1
                },
                {
                  "DamageType": "AnyCrew",
                  "Weight": 0
                },
                {
                  "DamageType": "AboveDeckCrew",
                  "Weight": 0
                },
                {
                  "DamageType": "BelowDeckCrew",
                  "Weight": 0
                }
              ]
            },
            {
              "DamageMin": 1,
              "DamageMax": 1,
              "CrewDamageMin": 2,
              "CrewDamageMax": 3,
              "DamageTypes": [
                {
                  "DamageType": "Hull",
                  "Weight": 0
                },
                {
                  "DamageType": "Sail",
                  "Weight": 0
                },
                {
                  "DamageType": "AnyCrew",
                  "Weight": 1
                },
                {
                  "DamageType": "AboveDeckCrew",
                  "Weight": 0
                },
                {
                  "DamageType": "BelowDeckCrew",
                  "Weight": 0
                }
              ]
            }
          ],
          "MaxEvents": 4,
          "InjuriesIDs": [
            "17fc9c75-c0ea-4823-b5f6-f3b29f53d546",
            "c97a3d47-ea0b-48e3-93f7-4fa94fcadefc",
            "1e0fbf95-28b3-4a18-baef-ff5ff21e7986",
            "b7c6acd2-ca6d-4bf6-ac71-6dcf82b96d65",
            "163e8713-5a13-4a64-a68d-4a302c9c4596",
            "08142ffd-7629-44e7-898b-bfeb66c78952"
          ],
          "HitPowderStoreChance": 0.01,
          "BraceForImpactInjuryPreventionChance": 0.5,
          "RakingFireSailDamageMultiplier": 1.5,
          "RakingFireHullDamageMultiplier": 1.5,
          "RakingFireCrewDamageMultiplier": 2,
          "AdvantageCannonMaxRangeMultiplier": 1.2,
          "CannonResultDelay": 3,
          "CannonInstanceDelay": 0.9,
          "OnFireCannons": [
            {
              "Data": {
                "FullName": "Void SetInteractionImage(String)",
                "Parameters": [
                  "gui\\interactionportraits\\re_world_map\\re_si_cannons_firing.png"
                ],
                "UnrealCall": "",
                "FunctionHash": 2042494187,
                "ParameterHash": 2130143173
              },
              "Conditional": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean IsShipType(ShipDuelParticipant, ShipType)",
                      "Parameters": [
                        "Actor",
                        "Canoe"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 1807620614,
                      "ParameterHash": -1861780293
                    },
                    "Not": true,
                    "Operator": 0
                  },
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean IsShipType(ShipDuelParticipant, ShipType)",
                      "Parameters": [
                        "Actor",
                        "Longship"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 1807620614,
                      "ParameterHash": 2070572829
                    },
                    "Not": true,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Data": {
                "FullName": "Void SetInteractionImage(String)",
                "Parameters": [
                  "gui\\interactionportraits\\ship_duels\\sd_voyager_attack.png"
                ],
                "UnrealCall": "",
                "FunctionHash": 2042494187,
                "ParameterHash": 767418745
              },
              "Conditional": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean IsShipType(ShipDuelParticipant, ShipType)",
                      "Parameters": [
                        "Actor",
                        "Canoe"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 1807620614,
                      "ParameterHash": -1861780293
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            },
            {
              "Data": {
                "FullName": "Void SetInteractionImage(String)",
                "Parameters": [
                  "gui\\interactionportraits\\ship_duels\\sd_longship_attack.png"
                ],
                "UnrealCall": "",
                "FunctionHash": 2042494187,
                "ParameterHash": 750819960
              },
              "Conditional": {
                "Operator": 0,
                "Components": [
                  {
                    "$type": "OEIFormats.FlowCharts.ConditionalCall, OEIFormats",
                    "Data": {
                      "FullName": "Boolean IsShipType(ShipDuelParticipant, ShipType)",
                      "Parameters": [
                        "Actor",
                        "Longship"
                      ],
                      "UnrealCall": "",
                      "FunctionHash": 1807620614,
                      "ParameterHash": 2070572829
                    },
                    "Not": false,
                    "Operator": 0
                  }
                ]
              }
            }
          ],
          "DifficultySettings": [
            {
              "Difficulty": "Easy",
              "EnemyHealthMultiplier": 0.8,
              "EnemyDamageMultiplier": 0.8,
              "EnemyShipAccuracyBonus": 0,
              "PlayerEventChanceMultiplier": 1,
              "EnemyEventChanceMultiplier": 1,
              "PlayerCannotSink": "false",
              "CloseToBoardDamageMultiplier": 0.8
            },
            {
              "Difficulty": "Normal",
              "EnemyHealthMultiplier": 1,
              "EnemyDamageMultiplier": 1,
              "EnemyShipAccuracyBonus": 0,
              "PlayerEventChanceMultiplier": 1,
              "EnemyEventChanceMultiplier": 1,
              "PlayerCannotSink": "false",
              "CloseToBoardDamageMultiplier": 1
            },
            {
              "Difficulty": "Hard",
              "EnemyHealthMultiplier": 1,
              "EnemyDamageMultiplier": 1,
              "EnemyShipAccuracyBonus": 0,
              "PlayerEventChanceMultiplier": 1,
              "EnemyEventChanceMultiplier": 1,
              "PlayerCannotSink": "false",
              "CloseToBoardDamageMultiplier": 1.1
            },
            {
              "Difficulty": "PathOfTheDamned",
              "EnemyHealthMultiplier": 1,
              "EnemyDamageMultiplier": 1,
              "EnemyShipAccuracyBonus": 0,
              "PlayerEventChanceMultiplier": 1,
              "EnemyEventChanceMultiplier": 1,
              "PlayerCannotSink": "false",
              "CloseToBoardDamageMultiplier": 1.2
            },
            {
              "Difficulty": "StoryTime",
              "EnemyHealthMultiplier": 0.66,
              "EnemyDamageMultiplier": 0.66,
              "EnemyShipAccuracyBonus": -15,
              "PlayerEventChanceMultiplier": 2,
              "EnemyEventChanceMultiplier": 0.5,
              "PlayerCannotSink": "true",
              "CloseToBoardDamageMultiplier": 0.8
            }
          ],
          "RepairHullEventID": "8d178f39-90af-4399-8dae-38104466247e",
          "RepairSailsEventID": "efcba635-cdf0-4d06-b3c2-2dcc9cd8bc4f",
          "ShipSunkExperienceMultiplier": 2,
          "ShipSunkTalesMultiplier": 2,
          "PlayerSurrenderSupplyRatio": 0.3,
          "PlayerSurrenderMinimumFoodDrink": 20,
          "PlayerSurrenderMoneyRatio": 0.15,
          "PlayerSurrenderMinimumMoney": 500,
          "PlayerSurrenderSailorTales": 1,
          "CloseToBoardDamageMultPerDefenderRank": 1.2,
          "CloseToBoardDamageMultPerAttackerRank": 0.9
        },
        {
          "$type": "Game.GameData.ShipDuelEventSettingsComponent, Assembly-CSharp",
          "EventTypeData": [
            {
              "EventType": "HullDamage",
              "FormatString": 5
            },
            {
              "EventType": "SailDamage",
              "FormatString": 6
            },
            {
              "EventType": "CrewDamage",
              "FormatString": 7
            },
            {
              "EventType": "AboveDeckCrewDamage",
              "FormatString": 8
            },
            {
              "EventType": "BelowDeckCrewDamage",
              "FormatString": 9
            },
            {
              "EventType": "SinkClock",
              "FormatString": 10
            },
            {
              "EventType": "LoseResource",
              "FormatString": 11
            },
            {
              "EventType": "MultiplyCombatSpeed",
              "FormatString": 12
            },
            {
              "EventType": "LoseRepairSupplies",
              "FormatString": 22
            }
          ]
        },
        {
          "$type": "Game.GameData.ShipDuelAudioSettingsComponent, Assembly-CSharp",
          "HullDamageRange": {
            "Min": 5,
            "Max": 30
          },
          "SailDamageRange": {
            "Min": 5,
            "Max": 30
          },
          "CrewDamageRange": {
            "Min": 1,
            "Max": 4
          },
          "FullSailEvent": "ship_full_sail",
          "HalfSailEvent": "ship_half_sail",
          "BoardEvent": "",
          "HoldEvent": "ship_hold_position",
          "TurnStarboardEvent": "ship_turn_starboard",
          "TurnPortEvent": "ship_turn_port",
          "TurnReverseEvent": "ship_turn_reverse",
          "FireCannonsEvents": "",
          "ReportEvent": "ship_report_to",
          "BraceForImpactEvent": "",
          "RamEvent": "ship_ram",
          "FireCannonIndividualEvent": "ship_cannon_fire",
          "CannonMissEvent": "ship_cannon_miss",
          "CannonDamageEvent": "ship_cannon_damage",
          "RamDamageEvent": "ship_ram_damage",
          "OtherDamageEvent": "",
          "DamageEventJumpstart": [
            {
              "Threshold": 0,
              "Value": 1.5
            },
            {
              "Threshold": 0.3,
              "Value": 2
            },
            {
              "Threshold": 0.7,
              "Value": 2.5
            }
          ]
        }
      ]
    }
  ]
}`;