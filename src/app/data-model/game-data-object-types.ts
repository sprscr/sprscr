import { Injectable } from '@angular/core';

@Injectable()
export class GameDataObjectTypes {
    // Characters.gamedatabundle types
    CHARACTERSTATS = 'Game.GameData.CharacterStatsGameData, Assembly-CSharp';
    CREATURETYPES = 'Game.GameData.CreatureTypeGameData, Assembly-CSharp';
    BASESTATS = 'Game.GameData.BaseStatsGameData, Assembly-CSharp';

    // Progressiontables.gamedatabundle types
    PROGRESSIONTABLE = 'Game.GameData.CharacterProgressionTableGameData, Assembly-CSharp';
    
    // Abilities.gamedatabundle types
    ABILITY = 'Game.GameData.GenericAbilityGameData, Assembly-CSharp'; // there are more types
    
    // Attacks.gamedatabundle types
    ATTACK_AOE = 'Game.GameData.AttackAOEGameData, Assembly-CSharp'; 
    ATTACK_AURA = 'Game.GameData.AttackAuraGameData, Assembly-CSharp'; 
    ATTACK_BEAM = 'Game.GameData.AttackBeamGameData, Assembly-CSharp';
    ATTACK_FIREARM = 'Game.GameData.AttackFirearmGameData, Assembly-CSharp';
    ATTACK_GRAPPLE = 'Game.GameData.AttackGrappleGameData, Assembly-CSharp';
    ATTACK_MELEE = 'Game.GameData.AttackMeleeGameData, Assembly-CSharp';
    ATTACK_PULSEDAOE = 'Game.GameData.AttackPulsedAOEGameData, Assembly-CSharp';
    ATTACK_RANDOMAOE = 'Game.GameData.AttackRandomAOEGameData, Assembly-CSharp';
    ATTACK_RANGED = 'Game.GameData.AttackRangedGameData, Assembly-CSharp';
    ATTACK_SUMMON = 'Game.GameData.AttackSummonGameData, Assembly-CSharp';
    ATTACK_SUMMONRANDOM = 'Game.GameData.AttackSummonRandomGameData, Assembly-CSharp';
    ATTACK_WALL = 'Game.GameData.AttackWallGameData, Assembly-CSharp';
    ATTACK_BEAMTELEPORT = 'Game.GameData.BeamTeleportAttackGameData, Assembly-CSharp';
    ATTACK_HAZARD = 'Game.GameData.HazardAttackGameData, Assembly-CSharp';
    ATTACK_MASTERSCALL = 'Game.GameData.MastersCallAttackGameData, Assembly-CSharp';
    ATTACK_TELEPORT = 'Game.GameData.TeleportAttackGameData, Assembly-CSharp';

    // Statuseffects.gamedatabundle types
    AFFLICTION = 'Game.GameData.AfflictionGameData, Assembly-CSharp';
    STATUSEFFECT = 'Game.GameData.StatusEffectGameData, Assembly-CSharp';
}