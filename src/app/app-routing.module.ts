import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CharactersComponent } from './components/characters/characters.component';
import { ProgressionTablesComponent } from './components/progression-tables/progression-tables.component';
import { AbilitiesComponent } from './components/abilities/abilities.component';
import { AttacksComponent } from './components/attacks/attacks.component';
import { StatusEffectsComponent } from './components/status-effects/status-effects.component';
import { LoadDataComponent } from './components/load-data/load-data.component';
import { ComingSoonComponent } from './components/coming-soon/coming-soon.component';
import { ExportDataComponent } from './components/export-data/export-data.component';

const routes: Routes = [
  { path: '', redirectTo: '/characters', pathMatch: 'full' },
  { path: 'progression-tables', component: ProgressionTablesComponent },
  { path: 'load-data', component: LoadDataComponent },
  { path: 'characters', component: CharactersComponent },
  { path: 'abilities', component: AbilitiesComponent },
  { path: 'attacks', component: AttacksComponent },
  { path: 'status-effects', component: StatusEffectsComponent },
  //{ path: 'coming-soon', component: ComingSoonComponent },
  { path: 'export-data', component: ExportDataComponent }
];

@NgModule({
  imports: [
	RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
